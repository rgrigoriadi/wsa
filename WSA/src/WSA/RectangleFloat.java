package WSA;

public class RectangleFloat {
	public float x1;
	public float x2;
	public float y1;
	public float y2;
	
	public RectangleFloat() {
		x1 = 0;
		x2 = 0;
		y1 = 0;
		y2 = 0;
	}
	
	public RectangleFloat(float x1, float x2, float y1, float y2) {
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
	}
	
	public void print () {
		Game.print(" X1 X2 " + x1 + " " + x2 + " || Y1 Y2 " + y1 + " " + y2);
	}
}
