package WSA;


import java.util.Arrays;

import org.lwjgl.util.vector.Vector3f;

public class Util {
	static Vector3f addV(Vector3f a, Vector3f b){
		Vector3f ret = new Vector3f(a.x + b.x, a.y + b.y, a.z + b.z);
		return ret;
	}
	
	static Vector3f mulV(Vector3f a, double d){
		Vector3f ret = new Vector3f(a.x * (float) d, a.y * (float) d, a.z * (float) d);
		return ret;
	}
	
	static Vector3f subV(Vector3f a, Vector3f b){
		Vector3f ret = new Vector3f(a.x - b.x, a.y - b.y, a.z - b.z);
		return ret;
	}

	static Vector3f divV(Vector3f a, float b) {
		Vector3f ret = new Vector3f(a.x / b, a.y / b, a.z / b);
		return null;
	}
	
	public static float getDistanceXZ (Vector3f p1, Vector3f p2) {
		return (float) Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.z - p2.z) * (p1.z - p2.z));
	}

	public static float getDistance (Vector3f p1, Vector3f p2) {
		return (float) Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y) + (p1.z - p2.z) * (p1.z - p2.z));
	}
	
	public static float arcTangent(double oppositeCathetus, double nextCathetus) {

		float res = (float) (Math.atan(oppositeCathetus / nextCathetus) / (2.0f * Math.PI) * 360.0f);
		if (nextCathetus < 0.0f)
			res += 180.0f;
		else if (oppositeCathetus < 0.0f)
			res = 360.0f + res;
		return res;
	}

	
	
	public static float approximate (float arg, float[] points) {
		Arrays.sort(points);
		
		if (arg < points[0]) {
			return points[0];
		} else if (arg > points[points.length - 1]) {
			return points[points.length - 1];
		}
		
		for (int i = 0; i < points.length - 1; i++) {
			if (arg > points[i] && arg < points[i + 1]) {
				if (arg < (points[i] + points[i + 1]) / 2.0f) {
					return points[i];
				} else {
					return points[i + 1];
				}
			}
			
		}
		return 0.0f;
	}
}
