package WSA;

import java.util.ArrayList;

import WSA.unit.AliveObject;
import WSA.unit.MapObject;

public class ObjectSelection {

	private ArrayList<MapObject> selectedObjects;

	public ObjectSelection() {
		selectedObjects = new ArrayList<MapObject>();
	}

	public ObjectSelection(MapObject obj) {
		selectedObjects = new ArrayList<MapObject>();
		if (obj != null) {
			selectedObjects.add(obj);
		}

	}

	public MapObject getObject(int index) {
		return selectedObjects.get(index);
	}

	public void add(MapObject obj) {
		if (!selectedObjects.contains(obj)) {
			selectedObjects.add(obj);
		}
	}

	public void add(ObjectSelection newSelection) {
		selectedObjects.addAll(newSelection.getAll());
	}

	public void remove(int index) {
		selectedObjects.remove(index);
	}

	public void clearSelection() {
		selectedObjects.clear();
	}

	public int getSize() {
		return selectedObjects.size();
	}

	public ArrayList<MapObject> getAll() {
		return selectedObjects;
	}

	public int getSelectionPlayerId() {
		if (!isAliveObjectSelection()) {
			return -1;
		}

		int id = -1;

		for (MapObject obj : selectedObjects) {
			if (id == -1) {
				id = obj.getPlayerId();
			} else {
				if (id != obj.getPlayerId()) {
					return -1;
				}
			}
		}

		return id;
	}

	public boolean isAliveObjectSelection() {
		boolean result = true;

		for (MapObject obj : selectedObjects) {
			if (!AliveObject.class.isInstance(obj)) {
				result = false;
			}
		}

		return result;
	}

	public boolean isOneObjectSelection() {
		if (getSize() == 1) {
			return true;
		} else {
			return false;
		}
	}
}
