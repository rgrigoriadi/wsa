package WSA;

import org.lwjgl.input.Mouse;

import de.matthiasmann.twl.Dimension;

public class Cursor {
	private int x;
	private int y;
	private int maxX;
	private int maxY;

	private int minX;
	private int minY;

	private int cursorSize;

	//private float speed;

	private boolean isActive;

	public Cursor(int maxX, int maxY, int minX, int minY) {
		//speed = 0.08f;

		cursorSize = 32;

		this.maxX = maxX;
		this.maxY = maxY;
		
		//Game.print("MaxY = " + maxY);
		
		this.setMinX(minX);
		this.setMinY(minY);
		setActive(true);
		resetCursor();
	}

	public void resetCursor() {
		x = maxX / 2;
		y = maxY / 2;
		//System.out.println("Resettting Cursor (" + x + ", " + y + ")" );

	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean arg) {
		isActive = arg;
	}

	public void refreshCursor() {
		if (Mouse.isCreated() == true) {
			setPosition(Mouse.getX(), Mouse.getY());
		}
	}

	public void setPosition(int X, int Y) {
		x = (X > maxX ? maxX : (X < getMinX() ? getMinX() : X));
		y = (Y > maxY ? maxY : (Y < getMinY() ? getMinY() : Y));

		/*//System.out.println("Cursor moving (" + dX + "; " + dY + ") ");
		 * 
		 * int tX = (int) (x + dX * speed * Game.getDelta()); int tY = (int) (y + dY * speed * Game.getDelta());
		 * 
		 * //System.out.println("Cursor moving TTTT (" + tX + "; " + tY + ") ");
		 * 
		 * System.out.println("Cursor position (" + Mouse.getX() + "; " + Mouse.getX() + ") ");
		 * 
		 * 
		 * if (tX > maxX) { x = maxX; } else if (tX < 0) { x = 0; } else { x = tX; }
		 * 
		 * if (tY > maxY) { y = maxY; } else if (tY < 0) { y = 0; } else { y = tY; } */
		//System.out.println();
	}

	public int getCursorSize() {
		return cursorSize;
	}

	public int getMinX() {
		return minX;
	}

	public void setMinX(int minX) {
		this.minX = minX;
	}

	public int getMinY() {
		return minY;
	}

	public void setMinY(int minY) {
		this.minY = minY;
	}
	
	public Dimension getCursorWindowPosition () {
		return new Dimension(getX(), getMaxY() - getY() + 1);
	}
	
}
