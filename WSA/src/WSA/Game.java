package WSA;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.ai.UnitAI;
import WSA.enviroment.Enviroment;
import WSA.graphics.render.Render;
import WSA.map.MapController;
import WSA.map.MapGenerator;
import WSA.player.Player;
import WSA.player.ViewController;
import WSA.ui.gui.DebugWindow;
import WSA.unit.MapObject;
import WSA.unit.MovingActiveObject;
import WSA.unit.ObjectController;
import WSA.unit.objects.Corpse;
import WSA.unit.objects.Unit;

public class Game {

	public static final String WINDOW_TITLE = "WSA GAME";
	public static Runtime runtime;

	private static long gameLastRefreshTime;
	private static long gameLastRenderTime;
	private static long gameStartedTime;
	private static long playTimeMillis;
	public static boolean gameRunning;

	public static String resourceLocation = "./resources";

	// ПРИЯТНЕНЬКОЕ ВРРЕМЯ ВЫХОДИТ
	// - 12 минут на день
	// - 3 дня на месяц          ||  36 минут на месяц       || полчаса
	// - 9 дней на сезон		 ||  108 минут на сезон      || 2 часа
	// - 36 дней на год          ||  432 минут на год        || 7 часов, один игровой день задрота, полдня среднячка, еще на WoW хватитЪ

	// Единица измерения времени - реальная секунда

	public static final int DAY_PARTS_PER_DAY = 4;
	public static final int DAYS_PER_MONTH = 3;
	public static final int MONTHS_PER_YEAR = 12;
	// здесь можно экспериментировать с длительностью суток

	public static final boolean isShortDay = false;//true;
	public static final float shortDaySizeCoef = 6.0f;

	public static final float DAY_DURATION = (isShortDay ? shortDaySizeCoef * 10.0f : 24.0f * 60.0f); // длительность суток в секундах
	public static final float DAY_PART_DURATION = DAY_DURATION / 4.0f; // длительность части суток в секундах, день состоит из 4 частей суток 
	public static final float MONTH_DURATION = DAY_DURATION * DAYS_PER_MONTH; // длительность месяца в секундах 
	public static final float YEAR_DURATION = MONTH_DURATION * MONTHS_PER_YEAR; // длительность года в секундах 

	public static float GAME_BEGIN_TIME = 0.0f; // время в реальных секундах, в рамках игрового года от 0 до 25920 невключительно

	private int fps = -1;
	private int fpsOld = fps;
	private long cfps = 0;
	private static long delta; // в секундах, время, между обновлениями

	MapController map;
	ViewController viewController;
	int maxFps;
	Render render;
	Enviroment enviroment;
	CollisionsController collisionsController;
	ObjectController objectController;
	WindowController window;
	Player player;

	public Game() {

		runtime = Runtime.getRuntime();

		Game.gameRunning = true;
		initGame();

		gameStartedTime = System.currentTimeMillis();
		playTimeMillis = 0;
		gameLastRefreshTime = System.currentTimeMillis();

		GAME_BEGIN_TIME = (float) ((int) ((int) (Math.random() * MONTHS_PER_YEAR) * MONTH_DURATION + (int) (Math.random() * DAYS_PER_MONTH) * DAY_DURATION + DAY_DURATION / 3.0f));

		gameLoop();

	}

	public void initGame() {

		render = new Render();

		enviroment = new Enviroment(this);
		map = new MapController(); // создаем карту

		objectController = new ObjectController();
		collisionsController = new CollisionsController(map, objectController);

		map.setMapGenerator(new MapGenerator(map, objectController, collisionsController));

		initFirstUnits();

		viewController = new ViewController(0, 80.0f, 0, collisionsController);

		player = new Player(0, viewController);

		window = new WindowController(player, viewController, collisionsController, render);

	}

	private void initFirstUnits() {
		Vector2f minPositions = new Vector2f(-30.0f, -30.0f);
		Vector2f maxPositions = new Vector2f(30.0f, 30.0f);
		int numberOfUnits = 30;

		for (int i = 0; i < numberOfUnits; i++) {
			float x = minPositions.x + (float) Math.random() * (maxPositions.x - minPositions.x);
			float y = minPositions.y + (float) Math.random() * (maxPositions.y - minPositions.y);
			Unit obj;
			obj = new Unit(i, new Vector3f(x, map.getHeight(x, y), y), new Vector2f(), false, map, objectController, (int) (Math.random() * 2));
			obj.setAI(new UnitAI(obj, map, objectController, collisionsController));
			objectController.createObject(obj);
		}
	}

	public void gameLoop() {

		while (Game.gameRunning == true) {

			// FPS						
			playTimeMillis += (System.currentTimeMillis() - gameLastRefreshTime);
			delta = System.currentTimeMillis() - gameLastRefreshTime;
			gameLastRefreshTime = System.currentTimeMillis();

			gameLastRenderTime = System.currentTimeMillis();
			if (fps == -1) {
				fps = 0;
				cfps = System.currentTimeMillis();
			}
			if (System.currentTimeMillis() - cfps < 1000)
				fps++;
			else {
				onSecondEnd();
			}
			Game.print("FPS = " + fpsOld);

			// обработаем действия пользователя
			window.getInput().checkInput();

			// обновляем мир
			enviroment.refresh(Game.getGameTime());
			objectController.refresh(collisionsController);

			// let subsystem paint
			render.render(viewController, player, enviroment, map, objectController, window);

			// update window contents
			if (window != null) {
				window.refresh(getPlayTimeInSeconds() + GAME_BEGIN_TIME);
			}

			Display.update();

		}

		// clean up
		window.onStop();
		map.onStop();
		render.onStop(map);

	}

	public void onSecondEnd() {
		fpsOld = fps;
		fps = 0;
		cfps = System.currentTimeMillis();
	}

	public static float getDeltaInSeconds() {
		return delta * 0.001f;
	}

	private static long getPlayTimeInMillis() {
		return playTimeMillis;
	}

	private static float getPlayTimeInSeconds() {
		return playTimeMillis / 1000.0f;
	}

	public static float getGameTime() {
		return GAME_BEGIN_TIME + getPlayTimeInSeconds();
	}

	public static Date getGameDate() {
		Date ret = new Date();
		ret.setDate(getGameTime());
		return ret;
	}

	public static void print(String str) {
		if (DebugWindow.isInited()) {
			DebugWindow.addImmediate(str);
		} else {
			printConsole(str);
		}
	}

	public static void printTimed(String str, float durationSeconds) {
		if (DebugWindow.isInited()) {
			DebugWindow.addTimed(str, durationSeconds);
		} else {
			printConsole(str);
		}
	}

	public static void printShortTimed(String str) {
		if (DebugWindow.isInited()) {
			DebugWindow.addTimed(str, DebugWindow.TimedString.SHORT_MESSAGE_TIME);
		} else {
			printConsole(str);
		}
	}

	public static void printLongTimed(String str) {
		if (DebugWindow.isInited()) {
			DebugWindow.addTimed(str, DebugWindow.TimedString.LONG_MESSAGE_TIME);
		} else {
			printConsole(str);
		}
	}

	public static void printConsole(String str) {
		System.out.println(str);
	}

	// TODO Уходящие в невидимость блоки

	/////////////////TODO DODODODODO unitController + хранение карты в мапКонтроллере а не в мапгруп 

	// TODO нормали блять

	// потом модели и деревья и анимация итд итп

	// TODO направление зума в зависимоти от положения курсора

	// TODO Угол камеры вверх 90 вниз 90

	// Сижу, не знаю, что делать, не могу разные материалы применить к вбо, читаю, надо текстур атлас делать. 
	// Ну я такой думаю, что это много работы, новые объекты, мороки, 3 дня искал варианты, не нашел. 
	// И вот на четвертый день идея пришла! охуительная, но решил для честности загуглить атлас. Так вот эта идея мне и пришла пиздец
}