package WSA.graphics.render;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import WSA.Game;
import WSA.Util;
import WSA.enviroment.Enviroment;
import WSA.graphics.texture.TextureAtlas;

public class SkyRenderer {
	TextureAtlas skyAtlas;
	float skyDistance = 10.0f;

	float sunCenterRadius = 0.25f;
	float sunRadius = sunCenterRadius * 1.5f;
	float sunLightRadius = sunCenterRadius * 4;
	
	float sunDistance = 10.0f;
	
	float fogHeight = 2.0f;
	
	public SkyRenderer() {

		skyAtlas = new TextureAtlas(Game.resourceLocation + "/texture_skybox.png", 1, 1);

	}

	public void renderSkyDomeBottom(Vector3f pos, Enviroment env) {

		boolean isLightingEnabled = GL11.glIsEnabled(GL11.GL_LIGHTING);

		if (isLightingEnabled) {
			GL11.glDisable(GL11.GL_LIGHTING);
		}

		GL11.glDepthMask(false);
		GL11.glPushMatrix();

		GL11.glTranslatef(pos.x, pos.y, pos.z);

		int circleDetalisation = 16;
		
		Vector3f fogColor = env.getFogColor(Game.getGameTime());
		GL11.glColor4f(fogColor.x, fogColor.y, fogColor.z, 1.0f);
		
		for (int i = 0; i < circleDetalisation; i++) {
			GL11.glBegin(GL11.GL_TRIANGLES);
		
			GL11.glColor4f(fogColor.x, fogColor.y, fogColor.z, 1.0f);
			
			GL11.glVertex3f(0.0f, -1.0f, 0.0f);
			GL11.glVertex3f(skyDistance * (float) Math.cos(2 * Math.PI * i / circleDetalisation), 0.0f, skyDistance * (float) Math.sin(2 * Math.PI * i / circleDetalisation));
			GL11.glVertex3f(skyDistance * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), 0.0f, skyDistance * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation));
			
			GL11.glEnd();
		}
					
		GL11.glPopMatrix();
		GL11.glDepthMask(true);

		if (isLightingEnabled) {
			GL11.glEnable(GL11.GL_LIGHTING);
		}

		
	}
	
	public void renderSkyDomeSideFog (Vector3f pos, Enviroment env) {
		
		boolean isLightingEnabled = GL11.glIsEnabled(GL11.GL_LIGHTING);

		if (isLightingEnabled) {
			GL11.glDisable(GL11.GL_LIGHTING);
		}

		GL11.glDepthMask(false);
		GL11.glPushMatrix();

		GL11.glTranslatef(pos.x, pos.y, pos.z);

		int circleDetalisation = 16;
		
		Vector3f fogColor = env.getFogColor(Game.getGameTime());
		GL11.glColor4f(fogColor.x, fogColor.y, fogColor.z, 1.0f);
		
		for (int i = 0; i < circleDetalisation; i++) {
			GL11.glBegin(GL11.GL_QUADS);
		
			GL11.glColor4f(fogColor.x, fogColor.y, fogColor.z, 1.0f);
			GL11.glVertex3f(skyDistance * (float) Math.cos(2 * Math.PI * i / circleDetalisation), 0.0f, skyDistance * (float) Math.sin(2 * Math.PI * i / circleDetalisation));
			GL11.glVertex3f(skyDistance * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), 0.0f, skyDistance * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation));
			
			GL11.glColor4f(fogColor.x, fogColor.y, fogColor.z, 0.0f);
			GL11.glVertex3f(skyDistance * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), fogHeight, skyDistance * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation));
			GL11.glVertex3f(skyDistance * (float) Math.cos(2 * Math.PI * i / circleDetalisation), fogHeight, skyDistance * (float) Math.sin(2 * Math.PI * i / circleDetalisation));
		
			GL11.glEnd();
			
			
		}

		GL11.glPopMatrix();
		GL11.glDepthMask(true);

		if (isLightingEnabled) {
			GL11.glEnable(GL11.GL_LIGHTING);
		}

		
	}
	
	public void renderSkybox(Vector3f pos) {

		boolean isLightingEnabled = GL11.glIsEnabled(GL11.GL_LIGHTING);

		if (isLightingEnabled) {
			GL11.glDisable(GL11.GL_LIGHTING);
		}

		float bottomOfTexture = 0.0f;

		GL11.glDepthMask(false);
		GL11.glPushMatrix();

		GL11.glTranslatef(pos.x, pos.y, pos.z);
		GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

		GL11.glBindTexture(skyAtlas.getTexDim(), skyAtlas.getTexId());

		GL11.glBegin(GL11.GL_QUADS);

		// FRONT tex bottom NORTH X+
		GL11.glTexCoord2f(0.25f, 1.0f);
		GL11.glVertex3f(skyDistance, bottomOfTexture, -skyDistance);
		GL11.glTexCoord2f(0.25f, 0.75f);
		GL11.glVertex3f(skyDistance, skyDistance, -skyDistance);
		GL11.glTexCoord2f(0.75f, 0.75f);
		GL11.glVertex3f(skyDistance, skyDistance, skyDistance);
		GL11.glTexCoord2f(0.75f, 1.0f);
		GL11.glVertex3f(skyDistance, bottomOfTexture, skyDistance);

		// BACK tex top SOUTH X-
		GL11.glTexCoord2f(0.75f, 0.0f);
		GL11.glVertex3f(-skyDistance, bottomOfTexture, skyDistance);
		GL11.glTexCoord2f(0.75f, 0.25f);
		GL11.glVertex3f(-skyDistance, skyDistance, skyDistance);
		GL11.glTexCoord2f(0.25f, 0.25f);
		GL11.glVertex3f(-skyDistance, skyDistance, -skyDistance);
		GL11.glTexCoord2f(0.25f, 0.0f);
		GL11.glVertex3f(-skyDistance, bottomOfTexture, -skyDistance);

		// LEFT tex left WEST Z-
		GL11.glTexCoord2f(0.0f, 0.75f);
		GL11.glVertex3f(skyDistance, bottomOfTexture, -skyDistance);
		GL11.glTexCoord2f(0.25f, 0.75f);
		GL11.glVertex3f(skyDistance, skyDistance, -skyDistance);
		GL11.glTexCoord2f(0.25f, 0.25f);
		GL11.glVertex3f(-skyDistance, skyDistance, -skyDistance);
		GL11.glTexCoord2f(0.0f, 0.25f);
		GL11.glVertex3f(-skyDistance, bottomOfTexture, -skyDistance);

		// RIGHT tex right EAST Z+
		GL11.glTexCoord2f(1.0f, 0.25f);
		GL11.glVertex3f(-skyDistance, bottomOfTexture, skyDistance);
		GL11.glTexCoord2f(0.75f, 0.25f);
		GL11.glVertex3f(-skyDistance, skyDistance, skyDistance);
		GL11.glTexCoord2f(0.75f, 0.75f);
		GL11.glVertex3f(skyDistance, skyDistance, skyDistance);
		GL11.glTexCoord2f(1.0f, 0.75f);
		GL11.glVertex3f(skyDistance, bottomOfTexture, skyDistance);

		//TOP tex center
		GL11.glTexCoord2f(0.25f, 0.25f);
		GL11.glVertex3f(-skyDistance, skyDistance, -skyDistance);
		GL11.glTexCoord2f(0.25f, 0.75f);
		GL11.glVertex3f(skyDistance, skyDistance, -skyDistance);
		GL11.glTexCoord2f(0.75f, 0.75f);
		GL11.glVertex3f(skyDistance, skyDistance, skyDistance);
		GL11.glTexCoord2f(0.75f, 0.25f);
		GL11.glVertex3f(-skyDistance, skyDistance, skyDistance);

		GL11.glEnd();

		GL11.glBindTexture(skyAtlas.getTexDim(), 0);

		GL11.glPopMatrix();

		GL11.glDepthMask(true);

		if (isLightingEnabled) {
			GL11.glEnable(GL11.GL_LIGHTING);
		}
	}

	public void renderSun(Vector3f posOfSkybox, Enviroment env) {

		boolean isLightingEnabled = GL11.glIsEnabled(GL11.GL_LIGHTING);

		if (isLightingEnabled) {
			GL11.glDisable(GL11.GL_LIGHTING);
		}

		GL11.glDepthMask(false);
		GL11.glPushMatrix();
		
		GL11.glTranslatef(posOfSkybox.x + sunDistance * env.getSunPositionV3f().x, posOfSkybox.y + sunDistance * env.getSunPositionV3f().y, posOfSkybox.z + sunDistance * env.getSunPositionV3f().z);

		float xAng = Util.arcTangent(env.getSunPositionV3f().x, env.getSunPositionV3f().z);

		float topRat = env.getSunPositionV3f().y;
		float botRat = -((float) Math.sqrt(env.getSunPositionV3f().z * env.getSunPositionV3f().z + env.getSunPositionV3f().x * env.getSunPositionV3f().x));
		
		float zAng = (float) (Util.arcTangent(topRat, botRat));
		GL11.glRotatef(xAng, 0.0f, 1.0f, 0.0f);
		GL11.glRotatef(zAng, 1.0f, 0.0f, 0.0f);
		
		int circleDetalisation = 32;
		
		for (int i = 0; i < circleDetalisation; i++) {
			GL11.glBegin(GL11.GL_TRIANGLES);
			
			Vector3f sunColor = env.getSunCenterColor();
			
			// center
			GL11.glColor4f(sunColor.x, sunColor.y, sunColor.z, 1.0f);
			GL11.glVertex3f(0.0f, 0.0f, 0.0f);
			GL11.glVertex3f(sunCenterRadius * (float) Math.cos(2 * Math.PI * i / circleDetalisation), sunCenterRadius * (float) Math.sin(2 * Math.PI * i / circleDetalisation), 0.0f);
			GL11.glVertex3f(sunCenterRadius * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), sunCenterRadius * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation), 0.0f);
			
			
			// edge
			GL11.glColor4f(sunColor.x, sunColor.y, sunColor.z, 1.0f);
			GL11.glVertex3f(sunCenterRadius * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), sunCenterRadius * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation), 0.0f);
			GL11.glVertex3f(sunCenterRadius * (float) Math.cos(2 * Math.PI * i / circleDetalisation), sunCenterRadius * (float) Math.sin(2 * Math.PI * i / circleDetalisation), 0.0f);
			GL11.glColor4f(env.getSunLightColor().x, env.getSunLightColor().y, env.getSunLightColor().z, 1.0f);
			GL11.glVertex3f(sunRadius * (float) Math.cos(2 * Math.PI * i / circleDetalisation), sunRadius * (float) Math.sin(2 * Math.PI * i / circleDetalisation), 0.0f);
			
			GL11.glVertex3f(sunRadius * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), sunRadius * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation), 0.0f);
			GL11.glVertex3f(sunRadius * (float) Math.cos(2 * Math.PI * i / circleDetalisation), sunRadius * (float) Math.sin(2 * Math.PI * i / circleDetalisation), 0.0f);
			GL11.glColor4f(sunColor.x, sunColor.y, sunColor.z, 1.0f);
			GL11.glVertex3f(sunCenterRadius * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), sunCenterRadius * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation), 0.0f);
			
			
			//shine
			GL11.glColor4f(env.getSunLightColor().x, env.getSunLightColor().y, env.getSunLightColor().z, 1.0f);
			GL11.glVertex3f(sunRadius * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), sunRadius * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation), 0.0f);
			GL11.glVertex3f(sunRadius * (float) Math.cos(2 * Math.PI * i / circleDetalisation), sunRadius * (float) Math.sin(2 * Math.PI * i / circleDetalisation), 0.0f);
			GL11.glColor4f(env.getSunLightColor().x, env.getSunLightColor().y, env.getSunLightColor().z, 0.0f);
			GL11.glVertex3f(sunLightRadius * (float) Math.cos(2 * Math.PI * i / circleDetalisation), sunLightRadius * (float) Math.sin(2 * Math.PI * i / circleDetalisation), 0.0f);
			
			GL11.glVertex3f(sunLightRadius * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), sunLightRadius * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation), 0.0f);
			GL11.glVertex3f(sunLightRadius * (float) Math.cos(2 * Math.PI * i / circleDetalisation), sunLightRadius * (float) Math.sin(2 * Math.PI * i / circleDetalisation), 0.0f);
			GL11.glColor4f(env.getSunLightColor().x, env.getSunLightColor().y, env.getSunLightColor().z, 1.0f);
			GL11.glVertex3f(sunRadius * (float) Math.cos(2 * Math.PI * (i + 1) / circleDetalisation), sunRadius * (float) Math.sin(2 * Math.PI * (i + 1) / circleDetalisation), 0.0f);
			
			
			GL11.glEnd();
		}
		
		GL11.glPopMatrix();

		GL11.glDepthMask(true);

		if (isLightingEnabled) {
			GL11.glEnable(GL11.GL_LIGHTING);
		}
	}

}
