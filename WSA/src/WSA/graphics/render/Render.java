/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package WSA.graphics.render;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;

import WSA.Game;
import WSA.ObjectSelection;
import WSA.WindowController;
import WSA.enviroment.Enviroment;
import WSA.graphics.ModelController;
import WSA.graphics.VBO;
import WSA.map.MapController;
import WSA.map.MapGroup;
import WSA.map.MapMaterial;
import WSA.map.MapRenderProcessor;
import WSA.player.Player;
import WSA.player.ViewController;
import WSA.unit.AliveObject;
import WSA.unit.MapObject;
import WSA.unit.MovingActiveObject;
import WSA.unit.ObjectController;
import WSA.unit.action.ActionStack;
import WSA.unit.objects.Corpse;

public class Render {

	public static int WIDTH;
	public static int HEIGHT;
	public static float perspectiveAngle;

	public static boolean isFullScreen = false;

	public static final int RANGE_TO_INVISIBLE_GROUPS = 3000;

	public static String FILEPATH_TEXTUREPACK_MAP = "./resources/texturepack_land.png";// "/cursor2.png";

	public MapRenderProcessor mapRenderer;

	public SkyRenderer skyRenderer;
	int texturePackId;

	public ModelController modelController;

	public Render() {
		init();
	}

	public void init() {
		WIDTH = Display.getDisplayMode().getWidth();
		HEIGHT = Display.getDisplayMode().getHeight();

		isFullScreen = false;

		perspectiveAngle = 45.0f;

		initDisplay(WIDTH, HEIGHT, isFullScreen);

		skyRenderer = new SkyRenderer();

		toggleFullScreen();

		GL11.glViewport(0, 0, WIDTH, HEIGHT); // Reset The Current Viewport

		GL11.glShadeModel(GL11.GL_SMOOTH); // Enables Smooth Shading
		GL11.glClearDepth(100.0f); // Depth Buffer Setup
		GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
		GL11.glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Test To Do

		GL11.glEnable(GL12.GL_RESCALE_NORMAL);

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_TEXTURE_2D);

		// GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);

		GL11.glColorMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT_AND_DIFFUSE);

		// GL11.glEnable(GL11.GL_LINE_SMOOTH);
		// GL11.glEnable(GL11.GL_POINT_SMOOTH);
		// GL11.glEnable(GL11.GL_POLYGON_SMOOTH);

		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
		// GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);
		// GL11.glHint(GL11.GL_POINT_SMOOTH_HINT, GL11.GL_NICEST);
		// GL11.glHint(GL11.GL_POLYGON_SMOOTH_HINT, GL11.GL_NICEST);

		mapRenderer = new MapRenderProcessor();
		mapRenderer.init();

		// Загружаем текстуры (1) Карты
		texturePackId = MapMaterial.loadTexture();

		modelController = new ModelController();
	}

	public void render(ViewController view, Player player, Enviroment enviroment, MapController map, ObjectController objController, WindowController window) {

		Vector3f skyColor = enviroment.getSkyColor(Game.getGameTime());

		GL11.glClearColor(skyColor.x, skyColor.y, skyColor.z, 1.0f);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

		// ///////////

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();

		GLU.gluPerspective(perspectiveAngle, ((float) WIDTH / (float) HEIGHT), 0.1f, 10000.0f);
		GLU.gluLookAt(view.getPosition().x, view.getPosition().y, view.getPosition().z, view.getCamAbsoluteDirection().x, view.getCamAbsoluteDirection().y, view.getCamAbsoluteDirection().z,
				view.getCamAbsoluteYVector().x, view.getCamAbsoluteYVector().y, view.getCamAbsoluteYVector().z); // TODOпеределать1нато,чтобысмотретьсверху

		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_AMBIENT, enviroment.getLightAmbient());
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_DIFFUSE, enviroment.getLightDiffuse());

		GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT_AND_DIFFUSE, enviroment.getLightAmbient());

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();

		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, enviroment.getSunPositionFB());

		//				skyRenderer.renderSkybox(view.getPosition());
		skyRenderer.renderSkyDomeSideFog(view.getPosition(), enviroment);
		skyRenderer.renderSun(view.getPosition(), enviroment);
		skyRenderer.renderSkyDomeBottom(view.getPosition(), enviroment);//renderSkybox(view.position);

		GL11.glEnable(GL11.GL_LIGHT0);

		// GL11.glPushMatrix();

		drawAxis(2);

		org.lwjgl.opengl.Util.checkGLError();

		if (GL11.glGetError() != 0) {
			Game.print(GLU.gluErrorString(GL11.glGetError()) + " it's code=" + GL11.glGetError());
		}

		render(map, view);

		/* THIS IS NOT A TRUE OOP ZEN */
		/* DO YOU WANT SOME TRUE OOP ZEN ? */
		/* LEARN SMALLTALK, MOTHERFUCTKER ! */

		render(objController);
		renderPostMap(objController, player, map, view, window);

		// TODO сделать красиую иконку пересечения с картой
	}

	public void drawAxis(float size) {
		// /////////////////// Оси начало ///////////////////////

		float axisAmpl = 75.0f;

		float dy = 30.0f;

		GL11.glLineWidth(size);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
		GL11.glVertex3f(-axisAmpl, dy, 0.0f);
		GL11.glVertex3f(axisAmpl, dy, 0.0f);

		GL11.glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		GL11.glVertex3f(0.0f, dy + -axisAmpl, 0.0f);
		GL11.glVertex3f(0.0f, dy + axisAmpl, 0.0f);

		GL11.glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		GL11.glVertex3f(0.0f, dy + 0.0f, -axisAmpl);
		GL11.glVertex3f(0.0f, dy + 0.0f, axisAmpl);

		GL11.glEnd();

		GL11.glBegin(GL11.GL_TRIANGLES);

		GL11.glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

		GL11.glVertex3f(axisAmpl + 1.0f, dy + 0.0f, 0.0f);
		GL11.glVertex3f(axisAmpl, dy + 0.5f, 0.0f);
		GL11.glVertex3f(axisAmpl, dy + -0.5f, 0.0f);

		GL11.glVertex3f(axisAmpl + 1.0f, dy + 0.0f, 0.0f);
		GL11.glVertex3f(axisAmpl, dy + 0.0f, 0.5f);
		GL11.glVertex3f(axisAmpl, dy + 0.0f, -0.5f);

		GL11.glColor4f(0.0f, 1.0f, 0.0f, 1.0f);

		GL11.glVertex3f(0.0f, axisAmpl + dy + 1.0f, 0.0f);
		GL11.glVertex3f(0.5f, axisAmpl + dy, 0.0f);
		GL11.glVertex3f(-0.5f, axisAmpl + dy, 0.0f);

		GL11.glVertex3f(0.0f, axisAmpl + dy + 1.0f, 0.0f);
		GL11.glVertex3f(0.0f, axisAmpl + dy, 0.5f);
		GL11.glVertex3f(0.0f, axisAmpl + dy, -0.5f);

		GL11.glColor4f(0.0f, 0.0f, 1.0f, 1.0f);

		GL11.glVertex3f(0.0f, dy + 0.0f, axisAmpl + 1.0f);
		GL11.glVertex3f(0.0f, dy + 0.5f, axisAmpl);
		GL11.glVertex3f(0.0f, dy + -0.5f, axisAmpl);

		GL11.glVertex3f(0.0f, dy + 0.0f, axisAmpl + 1.0f);
		GL11.glVertex3f(0.5f, dy + 0.0f, axisAmpl);
		GL11.glVertex3f(-0.5f, dy + 0.0f, axisAmpl);

		GL11.glEnd();

	}

	private void render(ObjectController objects) {

		//		int n = 256;
		//		int side = (int) Math.sqrt(n);
		//		
		//		int distance = 2;
		//		
		//		for (int i = -side / 2 * distance; i <= side / 2 * distance; i += distance) {
		//			for (int j = -side / 2 * distance; j <= side / 2 * distance; j += distance) {
		//				GL11.glTranslatef(i, 50.0f, j);
		//				modelController.getTestVBO().drawVBO();
		//				GL11.glTranslatef(-i, -50.0f, -j);
		//			}
		//		}

		for (int i = 0; i < objects.getNumberOfObjects(); i++) {
			MapObject obj = objects.getObject(i);

			GL11.glPushMatrix();

			GL11.glTranslatef(obj.getPosition().x, obj.getPosition().y, obj.getPosition().z);

			boolean mustDrawDilinders = true;

			if (mustDrawDilinders) {
				if (obj.getPlayerId() == -1) {
					GL11.glColor3f(0.5f, 0.5f, 0.5f);
				} else if (obj.getPlayerId() == 0) {
					GL11.glColor3f(1.0f, 0.0f, 0.0f);
				} else if (obj.getPlayerId() == 1) {
					GL11.glColor3f(0.0f, 0.0f, 1.0f);
				} else {
					GL11.glColor3f(0.0f, 0.0f, 0.0f);
				}
				GL11.glLineWidth(2);
				drawCilinder(obj.getObjectRadius(), obj.getObjectHeight());
			}

			GL11.glRotatef(-obj.getRotation().x, 0, 1, 0);
//			GL11.glRotatef(90.0f, 0, 1, 0);

			//			GL11.glScalef(obj.getObjectRadius(), obj.getObjectHeight(), obj.getObjectRadius());

			if (Corpse.class.isInstance(obj)) {
				GL11.glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
			}

			modelController.getModelForObject(obj).getVBO().drawVBO();
			
//			if (obj.getPlayerId() == 0) {
//				modelController.treeModel.getVBO().drawVBO();
//			} else {
//				modelController.bigStoneModel.getVBO().drawVBO();
//			}
			GL11.glPopMatrix();
		}

	}

	public void drawCilinder(float radius, float height) {
		int n = 8;

		for (int i = 0; i < n; i++) {
			GL11.glBegin(GL11.GL_LINES);

			GL11.glVertex3f(radius * (float) Math.cos(i * 2 * Math.PI / n), 0.0f, radius * (float) Math.sin(i * 2 * Math.PI / (n)));
			GL11.glVertex3f(radius * (float) Math.cos((i + 1) * 2 * Math.PI / (n)), 0.0f, radius * (float) Math.sin((i + 1) * 2 * Math.PI / (n)));

			GL11.glVertex3f(radius * (float) Math.cos(i * 2 * Math.PI / n), height, radius * (float) Math.sin(i * 2 * Math.PI / (n)));
			GL11.glVertex3f(radius * (float) Math.cos((i + 1) * 2 * Math.PI / (n)), height, radius * (float) Math.sin((i + 1) * 2 * Math.PI / (n)));

			GL11.glVertex3f(radius * (float) Math.cos(i * 2 * Math.PI / n), 0.0f, radius * (float) Math.sin(i * 2 * Math.PI / (n)));
			GL11.glVertex3f(radius * (float) Math.cos((i) * 2 * Math.PI / (n)), height, radius * (float) Math.sin((i) * 2 * Math.PI / (n)));

			GL11.glEnd();
		}
	}

	public void render(MapController map, ViewController view) {

		//		Game.print("rendering map");

		ArrayList<VBO> vbos = mapRenderer.getVBOs(map, view);

		long nTriag = 0;

		for (int i = 0; i < vbos.size(); i++) {
			nTriag += ((MapGroup.GROUP_STEP / vbos.get(i).density) * (MapGroup.GROUP_STEP / vbos.get(i).density) * 2);
		}

		Game.print("VBOs HD size = " + (vbos.size() - 1) + " triangles rendered = " + nTriag);

		for (int i = 0; i < vbos.size(); i++) {

			VBO vbo = vbos.get(i);

			VBO.drawVBO(vbo.vboId, vbo.iboId, vbo.iboSize, texturePackId);
		}

	}

	private void renderPostMap(ObjectController objects, Player player, MapController map, ViewController view, WindowController window) {
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();

		boolean isTextureTestMode = false;

		if (isTextureTestMode) {
			GL11.glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
			GL11.glBegin(GL11.GL_QUADS);
			//			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			//			GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePackId);
			GL11.glNormal3f(0.0f, 1.0f, 0.0f);

			//			GL11.glTexCoord2f(0.0f, 0.0f);
			GL11.glVertex3f(0.0f, 50.0f, 0.0f);

			//			GL11.glTexCoord2f(1.0f, 0.0f);
			GL11.glVertex3f(50.0f, 50.0f, 0.0f);

			//			GL11.glTexCoord2f(1.0f, 1.0f);
			GL11.glVertex3f(50.0f, 50.0f, 50.0f);

			//			GL11.glTexCoord2f(0.0f, 1.0f);
			GL11.glVertex3f(0.0f, 50.0f, 50.0f);
			GL11.glEnd();
		}

		// REndering SELECTION

		if (player.hasSelection()) { // Рисуем юниты
			ObjectSelection sel = player.getSelection();

			if (sel.getSize() == 1 && sel.getObject(0).isAlive()) {
				Game.print(((AliveObject) sel.getObject(0)).getCurrentActionString());
			}

			for (int i = 0; i < sel.getSize(); i++) { // TODO

				MapObject obj = sel.getObject(i);

				float radius = obj.getObjectRadius() * 4.0f;

				int q = 16;

				float dang = 360.0f / (float) q;

				GL11.glColor3f(1.0f, 1.0f, 1.0f);
				GL11.glLineWidth(5);
				GL11.glBegin(GL11.GL_LINE_LOOP);

				for (int j = 0; j < q; j++) {
					float x = obj.getPosition().x + radius * (float) Math.sin(j * dang / 180.0f * Math.PI);
					float z = obj.getPosition().z + radius * (float) Math.cos(j * dang / 180.0f * Math.PI);

					GL11.glVertex3f(x, obj.getPosition().y + obj.getObjectHeight() / 2.0f, z);
				}

				GL11.glEnd();

				if (MovingActiveObject.class.isInstance(obj)) {
					// рисуем стек действий

					GL11.glColor3f(0.0f, 1.0f, 0.0f);
					GL11.glLineWidth(5);
					GL11.glBegin(GL11.GL_LINES);

					MovingActiveObject mao = (MovingActiveObject) obj;

					ArrayList<ActionStack.actionChainElement> chain = mao.getActionChain();
					if (chain != null) {
						for (int j = 0; j < chain.size() - 1; j++) {
							GL11.glVertex3f(chain.get(j).position.x, chain.get(j).position.y, chain.get(j).position.z);
							GL11.glVertex3f(chain.get(j + 1).position.x, chain.get(j + 1).position.y, chain.get(j + 1).position.z);
						}
					}
					GL11.glEnd();
				}

			}
		}

		// Рисуем Мульт-Выбор юнитов (прямоугольничком :-D)
	}

	private boolean setDisplayMode() {
		try {
			// get modes
			DisplayMode[] dm = org.lwjgl.util.Display.getAvailableDisplayModes(WIDTH, HEIGHT, -1, -1, -1, -1, 60, 60);
			org.lwjgl.util.Display.setDisplayMode(dm, new String[] { "width=" + WIDTH, "height=" + HEIGHT, "freq=" + 60, "bpp=" + org.lwjgl.opengl.Display.getDisplayMode().getBitsPerPixel() });
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Unable to enter fullscreen, continuing in windowed mode");
		}
		return false;
	}

	private void initDisplay(int width, int height, boolean isFullScreen) {
		DisplayMode dis[];
		try {

			if (width == 0 || height == 0) { // Выберем максимальное разрешение
												// экрана при параметрах 0 0
				dis = Display.getAvailableDisplayModes();
				// TODO
				for (int i = 0; i < dis.length; i++) {
					Game.printConsole("Dispay Mode " + i + " w h = " + dis[i].getWidth() + " " + dis[i].getHeight());
				}

				width = 1024;
				height = 768;
			}

			Render.isFullScreen = isFullScreen;

			Display.setVSyncEnabled(false); // TRUE
			// Display.sync(200);
			Display.setTitle(Game.WINDOW_TITLE);

			Display.setFullscreen(isFullScreen);
			Display.setDisplayMode(new DisplayMode(width, height));

			Display.create();

		} catch (LWJGLException e) {
			e.printStackTrace();
			System.out.println("Game exiting - exception in Render display initialization:");
			Game.gameRunning = false;
			return;
		}

	}

	public void toggleFullScreen(boolean fs) {
		if (fs != isFullScreen) {
			toggleFullScreen();
		}
	}

	public void toggleFullScreen() {
		isFullScreen = !isFullScreen;

		try {

			DisplayMode displayMode = null;
			DisplayMode[] modes = Display.getAvailableDisplayModes();

			int targetWidth = this.WIDTH;
			int targetHeight = this.HEIGHT;

			int width = 0;
			int height = 0;

			for (int i = 0; i < modes.length; i++) {

				//				Game.printConsole(modes[i].toString() + " fullscr "+  modes[i].isFullscreenCapable());

				if (modes[i].isFullscreenCapable() || isFullScreen == false)
					if (((targetWidth - modes[i].getWidth()) * (targetWidth - modes[i].getWidth()) + (targetHeight - modes[i].getHeight()) * (targetHeight - modes[i].getHeight())) < ((targetWidth - width)
							* (targetWidth - width) + (targetHeight - height) * (targetHeight - height))) {
						width = modes[i].getWidth();
						height = modes[i].getHeight();
						displayMode = modes[i];
					}
			}

			if (displayMode != null) {
				Display.setFullscreen(isFullScreen);
				Display.setDisplayMode(displayMode);
			}

			Display.setFullscreen(isFullScreen);
			// Display.
		} catch (LWJGLException e) {
			e.printStackTrace();
		}

	}

	public static boolean getFullScreen() {
		return isFullScreen;
	}

	//	public void drawCilinderElement (float x, float z, loat height1, float radius1, float height2, float radius2)

	public static int createVBOID() {
		if (GLContext.getCapabilities().GL_ARB_vertex_buffer_object) {
			IntBuffer buffer = BufferUtils.createIntBuffer(1);
			ARBVertexBufferObject.glGenBuffersARB(buffer);
			return buffer.get(0);
		}
		return 0;
	}

	public static void bufferData(int id, FloatBuffer buffer) {
		if (GLContext.getCapabilities().GL_ARB_vertex_buffer_object) {
			ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, id);
			ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, buffer, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
		}
	}

	public static void bufferElementData(int id, IntBuffer buffer) {
		if (GLContext.getCapabilities().GL_ARB_vertex_buffer_object) {
			ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, id);
			ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, buffer, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
		}
	}

	public void onStop(MapController map) {
		GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
		GL11.glDisableClientState(GL11.GL_NORMAL_ARRAY);
		GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		MapMaterial.deallocTexture();
		Display.destroy();
	}

}
