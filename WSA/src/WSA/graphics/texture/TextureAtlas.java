package WSA.graphics.texture;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;

import WSA.Game;
import WSA.RectangleFloat;
import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;


public class TextureAtlas {

	ByteBuffer textureBuffer = null;

	private int textureId;
	private int textureDim = GL11.GL_TEXTURE_2D;
	private int textureLevel = GL13.GL_TEXTURE0;

	int TEXTURE_BUFFER_WIDTH = 0;
	int TEXTURE_BUFFER_HEIGHT = 0;

	int TEXTURE_ATLAS_WIDTH = 0;
	int TEXTURE_ATLAS_HEIGHT = 0;

	float TEXTURE_IMAGE_WIDTH = 0.0f;
	float TEXTURE_IMAGE_HEIGHT = 0.0f;

	int TEXTURE_IMAGE_NUMBER = 0;

	private Texture[] textures;

	public TextureAtlas(String filePath, int numOfImagesInARow, int numOfAvailableImages) {
		loadTexture(filePath, numOfImagesInARow, numOfAvailableImages);

		if (isLoadedGPU()) {
			TEXTURE_IMAGE_NUMBER = numOfAvailableImages;
		}
	}

	public void loadTexture(String filePath, int numOfImagesInARow, int numOfAvailableImages) {

		try {

			InputStream in = new FileInputStream(filePath);
			PNGDecoder decoder = new PNGDecoder(in);

			TEXTURE_BUFFER_WIDTH = decoder.getWidth();
			TEXTURE_BUFFER_HEIGHT = decoder.getHeight();

			textureBuffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());

			TEXTURE_ATLAS_WIDTH = numOfImagesInARow;
			TEXTURE_ATLAS_HEIGHT = (int) (TEXTURE_ATLAS_WIDTH / (float) decoder.getWidth() * decoder.getHeight());

			decoder.decode(textureBuffer, TEXTURE_BUFFER_WIDTH * TEXTURE_ATLAS_HEIGHT * 4, Format.RGBA);
			textureBuffer.flip();

			// Create a new texture object in memory and bind it
			
			textureDim = GL11.GL_TEXTURE_2D;
			textureLevel = GL13.GL_TEXTURE0;
			textureId = GL11.glGenTextures();
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			GL11.glBindTexture(textureDim, textureId);
			GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

			GL11.glTexImage2D(textureDim, 0, GL11.GL_RGBA, TEXTURE_BUFFER_WIDTH, TEXTURE_BUFFER_HEIGHT, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, textureBuffer);
			GL30.glGenerateMipmap(textureDim);

			GL11.glTexParameteri(textureDim, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
			GL11.glTexParameteri(textureDim, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

			if (isLoadedGPU()) {
				Game.printConsole("TextureAtlas file " + filePath + " loaded sucsessfuly");
			} else {
				Game.printConsole("TextureAtlas file " + filePath + " loaded unsucsessfuly");
			}
			
			deallocTexture();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isLoadedGPU() {
		return GL11.glIsTexture(textureId);
	}

	public void deallocTexture() {
		if (textureBuffer != null)
			textureBuffer = ByteBuffer.allocateDirect(0);
	}
	
	public void unLoadGPU () {
		GL11.glBindTexture(textureLevel, textureId);
		GL11.glDeleteTextures(0);
		GL11.glBindTexture(textureLevel, 0);
	}

	public RectangleFloat getTexCoord(int imageNumber) {
		if (imageNumber < 0 || imageNumber >= TEXTURE_IMAGE_NUMBER) {
			return null;
		} else {

			float inc = 0.5f; // сдвиг границ в пикселах

			RectangleFloat ret = new RectangleFloat(
					(imageNumber % TEXTURE_ATLAS_WIDTH * (TEXTURE_IMAGE_WIDTH) + inc) / (float) TEXTURE_BUFFER_WIDTH, 
					((imageNumber % TEXTURE_ATLAS_WIDTH + 1) * (TEXTURE_IMAGE_WIDTH) - inc) / (float) TEXTURE_BUFFER_WIDTH,
					(imageNumber / TEXTURE_ATLAS_HEIGHT * (TEXTURE_IMAGE_HEIGHT) + inc) / (float) TEXTURE_BUFFER_HEIGHT,
					((imageNumber / TEXTURE_ATLAS_HEIGHT + 1) * (TEXTURE_IMAGE_HEIGHT) - inc) / (float) TEXTURE_BUFFER_HEIGHT
			);
			return ret;
		}

	}

	public int getTexId() {
		return textureId;
	}
	
	public int getTexDim() {
		return textureDim;
	}

	public int getTexLevel() {
		return textureLevel;
	}
}
