package WSA.graphics.texture;

import org.lwjgl.opengl.GL11;

import WSA.RectangleFloat;


public class Texture {

	TextureAtlas atlas;
	
	public final int IMAGE_NUMBER_IN_ATLAS;
	
	public Texture(TextureAtlas parentAtlas, int numberInAtlas) {
		atlas = parentAtlas;
		IMAGE_NUMBER_IN_ATLAS = numberInAtlas;
	}

	public boolean isLoaded() {
		return GL11.glIsTexture(atlas.getTexId());
	}

	public int getId() {
		return atlas.getTexId();
	}

	public RectangleFloat getTexCoord() {
		return atlas.getTexCoord(IMAGE_NUMBER_IN_ATLAS);
	}

}
