package WSA.graphics;

import galileo.collada.Collada;

import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import WSA.Game;

public class Model {
	
	private VBO modelVBO;
	
	public Vector3f[] vertexes;
	public Vector4f[] colors;
	public Vector3f[] normals;
	public Face[] faces;
	
	public static class Face {
		public Face (int [] vertexIndexes) {
			this.vertexIndexes = vertexIndexes;
		}
		
		public int[] vertexIndexes;
	}
	
	public VBO cresateUnloadedVBO () {
		VBO result = new VBO();
		
		result.vbo = VBO.createFloatBuffer(vertexes, colors, normals, null);
		result.ibo = VBO.createIndexBuffer(getIndexList(faces));
		
		result.iboSize = faces.length * faces[0].vertexIndexes.length;
		
		return result;
	}
	
	public VBO getVBO() {
		if (modelVBO == null) {
			modelVBO = cresateUnloadedVBO();
			modelVBO.loadBuffer2GPU();
		}
		return modelVBO;
	}
	
	public void destroyVBO () {
		if (modelVBO != null) {
			if (modelVBO.isLoaded()) {
				modelVBO.unloadVBO();
			}
		}
	}
	
	public static int[][] getIndexList(Face[] faces) {
		
//		if (faces == null) {
//			Game.print("Faces = null");
//		}
//		
//		if (faces[0] == null) {
//			Game.print("Faces[0] = null");
//		}
//		
//		if (faces[0].vertexIndexes == null) {
//			Game.print("Faces[0].vertexIndexes = null");
//		}
		
		int[][] indexes = new int [faces.length][faces[0].vertexIndexes.length];
		
		for (int i = 0; i < faces.length; i++) {
//			if (faces[i] == null) {
//				Game.print("Faces[" + i + "] = null");
//			}
//			
//			if (faces[i].vertexIndexes == null) {
//				Game.print("Faces[0].vertexIndexes = null");
//			}
			
			for (int j = 0; j < faces[i].vertexIndexes.length; j++) {
				indexes[i][j] = faces[i].vertexIndexes[j];
			}
		}
		
		return indexes;
	}
	
	
	public void ololo () {
		Collada collada = new Collada();
		
	}
	
}
