package WSA.graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import WSA.graphics.render.Render;


public class VBO {

	public int vboId;
	public int iboId;
	public int iboSize;

	public FloatBuffer vbo;
	public IntBuffer ibo;

	public int density;

	private boolean isLoaded = false;

	public boolean isLoaded() {
		return isLoaded;
	}

	public void loadBuffer2GPU() {
		
		vboId = Render.createVBOID();
		iboId = Render.createVBOID();
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, vboId);
		ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, vbo, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);

		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, iboId);
		ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, ibo, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);

		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, 0);
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, 0);

		isLoaded = true;
	}

	public static FloatBuffer createFloatBuffer(Vector3f[] vertex, Vector4f[] color, Vector3f[] normals, Vector2f[] texCoords) {
		int size = vertex.length;

		if ((color != null && vertex.length != color.length) || (normals != null && vertex.length != normals.length) || (texCoords != null && vertex.length != texCoords.length)) {
			System.err.println("FloatBuffer creation : color array size not equal vertex array size");
			return null;
		}

		int bytesPerVertex = (3 + 4 + 3) * 4;
		if (texCoords != null) {
			bytesPerVertex += 2 * 4;
		}

		FloatBuffer ret = ByteBuffer.allocateDirect(size * bytesPerVertex).order(ByteOrder.nativeOrder()).asFloatBuffer();

		for (int i = 0; i < size; i++) {
			ret.put(vertex[i].x);
			ret.put(vertex[i].y);
			ret.put(vertex[i].z);

			if (normals != null) {
				ret.put(normals[i].x);
				ret.put(normals[i].y);
				ret.put(normals[i].z);
			}

			if (color != null) {
				ret.put(color[i].x);
				ret.put(color[i].y);
				ret.put(color[i].z);
				ret.put(color[i].w);
			}

			if (texCoords != null) {
				ret.put(texCoords[i].x);
				ret.put(texCoords[i].y);
			}

		}

		ret.rewind();
		return ret;
	}

	public static IntBuffer createIndexBuffer(int[][] faceIndexes) {
		int faceSize = faceIndexes[0].length;
		int facesCount = faceIndexes.length;

		IntBuffer ret = ByteBuffer.allocateDirect(facesCount * faceSize * 4).order(ByteOrder.nativeOrder()).asIntBuffer();

		for (int i = 0; i < facesCount; i++) {
			for (int j = 0; j < faceSize; j++) {
				ret.put(faceIndexes[i][j]);
			}
		}

		ret.rewind();
		return ret;
	}

	public void drawVBO () {
		drawVBO(vboId, iboId, iboSize, 0);
	}
	
	public static void drawVBO(int vboid, int iboid, int ibosize, int textureAtlasId) {

		int stride = (3 + 3 + 4) * 4;

		if (textureAtlasId != 0) stride += 2 * 4;
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, vboid);

		// vertices
		GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		int offset = 0 * 4;
		GL11.glVertexPointer(3, GL11.GL_FLOAT, stride, offset);

		// normals
		GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
		offset = (0 + 3) * 4; 
		GL11.glNormalPointer(GL11.GL_FLOAT, stride, offset);

		// colours
		GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
		offset = (0 + 3 + 3) * 4;
		GL11.glColorPointer(4, GL11.GL_FLOAT, stride, offset);

		if (textureAtlasId != 0) {
			GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureAtlasId);
			offset = (0 + 3 + 3 + 4) * 4;
			GL11.glTexCoordPointer(2, GL11.GL_FLOAT, stride, offset);
		}

		// Bind to the index VBO that has all the information about the
		// order of the vertices
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, iboid);
		// Draw the vertices

		GL11.glDrawElements(GL11.GL_TRIANGLES, ibosize, GL11.GL_UNSIGNED_INT, 0);

		if (textureAtlasId != 0) {
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		}
		
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, 0);
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, 0);

		GL11.glDisableClientState(GL11.GL_NORMAL_ARRAY);
		GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
		GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		if (textureAtlasId != 0) {
			GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		}
	}
	
	public void unloadVBO() {
		ARBVertexBufferObject.glDeleteBuffersARB(vboId);
		ARBVertexBufferObject.glDeleteBuffersARB(iboId);
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, 0);
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
	}
}