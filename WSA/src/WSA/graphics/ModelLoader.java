package WSA.graphics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import WSA.Game;
import WSA.graphics.Model.Face;

public class ModelLoader {

	public Model load(String url) throws IOException {

		File file = new File(url);

		int counters[] = new int[4];

		BufferedReader reader = new BufferedReader(new FileReader(file));
		String currentString;

		ArrayList<Vector3f> vertexes = new ArrayList<Vector3f>();
		ArrayList<Vector3f> normals = new ArrayList<Vector3f>();
		//		ArrayList<Model.Face> faces = new ArrayList<Face>();

		ArrayList<int[]> indexes = new ArrayList<int[]>();

		while ((currentString = reader.readLine()) != null) {
			String prefix = currentString.split(" ")[0];
			if (prefix.equals("#")) {
				continue;
			} else if (prefix.equals("o")) {
				counters[0]++;

			} else if (prefix.equals("v")) {
				counters[1]++;
				String splited[] = currentString.split(" ");
				vertexes.add(new Vector3f(Float.parseFloat(splited[1]), Float.parseFloat(splited[2]), Float.parseFloat(splited[3])));
			} else if (prefix.equals("vn")) {
				counters[2]++;
				String splited[] = currentString.split(" ");
				Vector3f normal = new Vector3f(Float.parseFloat(splited[1]), Float.parseFloat(splited[2]), Float.parseFloat(splited[3]));
				normals.add(normal);

			} else if (prefix.equals("f")) {
				counters[3]++;
				String splited[] = currentString.split(" ");

				for (int i = 0; i < 3; i++) {
					String doubleSplited[] = splited[i + 1].split("/");

					int[] ind = new int[2];

					ind[0] = Integer.parseInt(doubleSplited[0]) - 1;
					ind[1] = Integer.parseInt(doubleSplited[2]) - 1;

					indexes.add(ind);

				}

			} else {
				Game.printConsole("OBJ Loader: \"" + url + "\" contains line which cannot be parsed correctly: " + currentString);
				continue;
			}
		}

		Game.printConsole("Objects found = " + counters[0] + " vertexes = " + counters[1] + "/" + vertexes.size() + " normals = " + counters[2] + "/" + normals.size() + " faces = " + counters[3]
				+ "/" + (indexes.size() / 3.0f));

		reader.close();

		Model result = new Model();

		ArrayList<Vector3f[]> vertexNormalPair = new ArrayList<Vector3f[]>();
		ArrayList<Integer> newIndexes = new ArrayList<Integer>();

		for (int i = 0; i < indexes.size(); i++) {

			Vector3f[] pair = new Vector3f[2];

			pair[0] = vertexes.get(indexes.get(i)[0]);
			pair[1] = normals.get(indexes.get(i)[1]);

			vertexNormalPair.add(pair);
			newIndexes.add(new Integer(i));//new Integer(vertexNormalPair.indexOf(pair)));

		}

		result.vertexes = new Vector3f[vertexNormalPair.size()];
		result.colors = new Vector4f[vertexNormalPair.size()];
		result.normals = new Vector3f[vertexNormalPair.size()];

		for (int i = 0; i < vertexNormalPair.size(); i++) {
			result.vertexes[i] = vertexNormalPair.get(i)[0];
			result.colors[i] = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
			result.normals[i] = vertexNormalPair.get(i)[1];
		}

		result.faces = new Face[vertexNormalPair.size() / 3];

		for (int i = 0; i < vertexNormalPair.size() / 3; i++) {
			int[] arr3 = new int[3];
			for (int j = 0; j < 3; j++) {
				arr3[j] = i * 3 + j;
			}
			result.faces[i] = new Face(arr3);
		}

		Game.printConsole("Vertex normal pair size = " + vertexNormalPair.size());

		return result;
	}
}
