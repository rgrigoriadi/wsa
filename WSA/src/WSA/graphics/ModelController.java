package WSA.graphics;

import java.io.IOException;

import WSA.Game;
import WSA.unit.MapObject;
import WSA.unit.objects.BigStone;
import WSA.unit.objects.Corpse;
import WSA.unit.objects.TreeObject;
import WSA.unit.objects.Unit;


public class ModelController {
	
	ModelLoader modelLoader;
	
	public static final String MODELFOLDER = "/Blender/";
	
	public static final String TEST_MODEL = "test.obj";
	public static final String TREE_MODEL = "tree.obj";
	public static final String BIG_STONE_MODEL = "big_stone.obj";
	public static final String UNIT_MODEL = "unit.obj";
	
	VBO testVBO;
	
	public Model testModel;
	public Model treeModel;
	public Model bigStoneModel;
	public Model unitModel;
	
	public ModelController() {
		modelLoader = new ModelLoader();
		
		loadModels();
	}
	
	public void loadModels() {
		try {
			testModel = modelLoader.load(Game.resourceLocation + MODELFOLDER + TEST_MODEL);
			
			treeModel = modelLoader.load(Game.resourceLocation + MODELFOLDER + TREE_MODEL);
			bigStoneModel = modelLoader.load(Game.resourceLocation + MODELFOLDER + BIG_STONE_MODEL);
			
			unitModel = modelLoader.load(Game.resourceLocation + MODELFOLDER + UNIT_MODEL);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		testModel.getVBO().loadBuffer2GPU();
		treeModel.getVBO().loadBuffer2GPU();
		bigStoneModel.getVBO().loadBuffer2GPU();
		unitModel.getVBO().loadBuffer2GPU();
	}
	
	public Model getUnitModel () {
		return testModel;
	}
	
	public VBO getTestVBO() {
		return testVBO;
	}
	
	
	
	public Model getModelForObject (MapObject obj) {
		
//		Game.print("Getting model for object named " + obj.getNameString());
		
		if (TreeObject.class.isInstance(obj)) {
			return treeModel;
		} else if (BigStone.class.isInstance(obj)) {
			return bigStoneModel;
		} else if (Unit.class.isInstance(obj)) {
			return unitModel;
		} else if (Corpse.class.isInstance(obj)) {
			return getModelForObject(((Corpse) obj).getAliveObject());
		}
		
		return null;//testModel;
	}
}
