package WSA.ui.input;

import org.lwjgl.util.vector.Vector2f;

import WSA.CollisionsController;
import WSA.ObjectSelection;
import WSA.Vector2i;
import WSA.player.ViewController;

public class SelectionController {

	private Vector2i cursorStart;
	private Vector2i cursorEnd;

	CollisionsController collisionsController;
	ViewController viewController;

	public SelectionController(ViewController cam, CollisionsController col) {
		viewController = cam;
		collisionsController = col;
	}

	public void onPressStart(int screenX, int screenY) {
		cursorEnd = null;
		cursorStart = null;

		cursorStart = new Vector2i(screenX, screenY);
	}

	public void onPressRefresh(int screenX, int screenY) {
		cursorEnd = new Vector2i(screenX, screenY);
	}
	
	public void onSelectionDestroy () {
		cursorStart = null;
		cursorEnd = null;
	}

	public ObjectSelection getSelectedObjects (int playerId) {
		if (isSingleSelectionEnabled()) {
			return new ObjectSelection(collisionsController.getObjectByCursor(viewController));
		} else if (isMultiSelectionEnabled()) {
			return collisionsController.getUnitsByRect(getMapPolygonByScreenRectangle(cursorStart, cursorEnd), playerId);
		} else {
			return null;
		}
	}

	public boolean isMultiSelectionEnabled() {
		if (cursorStart != null && cursorEnd != null && (cursorStart.x != cursorEnd.x || cursorStart.y != cursorEnd.y)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isSingleSelectionEnabled() {
		if (cursorStart != null && (cursorEnd == null || (cursorStart.x == cursorEnd.x || cursorStart.y == cursorEnd.y))) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isSelectionEnabled () {
		if (isSingleSelectionEnabled() || isMultiSelectionEnabled()) {
			return true;
		} else {
			return false;
		}
	}

	private Vector2f[] getMapPolygonByScreenRectangle (Vector2i p1, Vector2i p2) {
		Vector2f[] coords = new Vector2f[4];

		coords[0] = collisionsController.getVectorIntersectionWithMap(viewController.getPosition(), viewController.getCursorDirection(cursorStart.x, cursorStart.y));
		coords[1] = collisionsController.getVectorIntersectionWithMap(viewController.getPosition(), viewController.getCursorDirection(cursorStart.x, cursorEnd.y));
		coords[2] = collisionsController.getVectorIntersectionWithMap(viewController.getPosition(), viewController.getCursorDirection(cursorEnd.x, cursorEnd.y));
		coords[3] = collisionsController.getVectorIntersectionWithMap(viewController.getPosition(), viewController.getCursorDirection(cursorEnd.x, cursorStart.y));

		return coords;
	}
}
