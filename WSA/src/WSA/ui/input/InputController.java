package WSA.ui.input;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import WSA.CollisionsController;
import WSA.Game;
import WSA.WindowController;
import WSA.player.Player;

public class InputController {
	private int wheelAccum;
	private static boolean shift;
	private float lmbPushTime = 0;
	private float rmbPushTime = 0;
	private float mmbPushTime = 0;

	public static final int BUTTON_LMB = 0;
	public static final int BUTTON_RMB = 1;
	public static final int BUTTON_MMB = 2;

	WindowController window;
	Player playerHandler;
	SelectionController selectionController;

	public InputController(Player player, WindowController window) {

		this.playerHandler = player;
		this.window = window;

		wheelAccum = 0;
		try {
			Keyboard.create();
			Mouse.create();
			Mouse.setGrabbed(true);
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.out.println("MOUSE problem");
		}

	}

	public void checkInput() {

		if (Keyboard.isKeyDown(Keyboard.KEY_F11)) {
			window.toggleFullScreen();
		}

		int dirX = 0;
		int dirZ = 0;
		int dirWheel = 0;

		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			dirX = 1;
		} else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			dirX = -1;
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
			dirZ = -1;
		} else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
			dirZ = 1;
		}

		window.refreshCursor();

		window.processSpaceButtonState(Keyboard.isKeyDown(Keyboard.KEY_SPACE));

		float mDX = Mouse.getDX();
		float mDY = Mouse.getDY();
		float mDWheel = Mouse.getDWheel();

		if (window.getCam().isGlobalFree == true) {

			//			Game.print("MDX MDY = " + mDX + " " + mDY);

			window.getCam().rotateCamera(mDX, mDY);
			window.getCursor().resetCursor();

		} else if (window.getCursor().isActive() == true) {
			if (dirX == 0)
				if (window.getCursor().getX() >= window.getCursor().getMaxX()) {
					dirZ = 1;
				} else if (window.getCursor().getX() <= window.getCursor().getMinX()) {
					dirZ = -1;
				}

			if (dirZ == 0)
				if (window.getCursor().getY() >= window.getCursor().getMaxY()) {
					dirX = 1;
				} else if (window.getCursor().getY() <= window.getCursor().getMinY()) {
					dirX = -1;
				}
		}

		wheelAccum += mDWheel;
		int dWheel = 40;

		boolean isGlobalMoved = false;

		if (wheelAccum != 0) {
			if (wheelAccum > 0) {
				if (mDWheel < 0)
					wheelAccum = 0;
				wheelAccum -= dWheel;
			} else if (wheelAccum < 0) {
				if (mDWheel > 0)
					wheelAccum = 0;
				wheelAccum += dWheel;
			} else {
				
			}
			dirWheel = ((wheelAccum > 0) ? dWheel : (wheelAccum < 0) ? -dWheel : 0);
			isGlobalMoved = true;
		} else {
			dirWheel = 0;
		}

		window.getCam().moveCamera(dirX, dirZ, dirWheel, isShift());

		if (dirX != 0 || dirZ != 0) {
			isGlobalMoved = true;
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_R)) {
			window.getCam().resetCameraRotation();
			isGlobalMoved = true;
		}

		if (isGlobalMoved || window.getCam().isGlobalFree == true) {
			window.disableMultiSelection();
		}

		refreshMultiSelection();

		if (Display.isCloseRequested())
			Game.gameRunning = false;

	}

	public void refreshMultiSelection() {
		if (selectionController != null) {
			selectionController.onPressRefresh(window.getCursor().getX(), window.getCursor().getY());
			if (selectionController.isMultiSelectionEnabled()) {//TODOwindow.isMultiSelectionEnabled() == true) {
				if (!window.isMultiSelectionEnabled()) {
					window.enableMultiSelection(window.getCursor().getX(), window.getCursor().getY(), window.getCursor().getMaxY());
				}
				window.setMultiSelectionOnScreenPos(window.getCursor().getX(), window.getCursor().getY());
			}
		}
	}

	public void pollMouseWorldClick(int buttonNumber, boolean isPushed, CollisionsController collisionController) { // 0 - lmb, 1 - rmb, 2 -mmb, true - pushed, false button releaseds

		if (isPushed == true) {
			switch (buttonNumber) {
			case BUTTON_LMB:
				lmbPushTime = Game.getGameTime();
				selectionController = new SelectionController(window.getCam(), collisionController);
				selectionController.onPressStart(window.getCursor().getX(), window.getCursor().getY());
				
//				Game.printLongTimed("LBM click");
				
				break;
			case BUTTON_RMB:
				rmbPushTime = Game.getGameTime();
				break;
			case BUTTON_MMB:
				mmbPushTime = Game.getGameTime();
				break;
			default:
				break;
			}
		} else {
			switch (buttonNumber) {
			case BUTTON_LMB:

				playerHandler.getActionController().onLMBClick(collisionController, window.getCam(), selectionController, isShift());
				
				if (selectionController.isMultiSelectionEnabled()) {
					window.disableMultiSelection();
				}
				selectionController.onSelectionDestroy();
				lmbPushTime = -1;

				break;
			case BUTTON_RMB:

				playerHandler.getActionController().onRMBClick(collisionController, window.getCam(), isShift());
				break;

			case BUTTON_MMB:
				mmbPushTime = -1;
				break;
			default:
				break;
			}
		}
	}

	public static void pollShift() {
		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			shift = true;
		} else {
			shift = false;
		}
	}

	public void processMouseWorldClick() {

	}

	public static boolean isShift() {
		pollShift();
		return shift;
	}

}
