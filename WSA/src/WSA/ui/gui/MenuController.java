package WSA.ui.gui;

import WSA.WindowController;
import WSA.ui.gui.menu.InventoryMenu;
import WSA.ui.gui.menu.StatusBar;
import WSA.ui.gui.menu.StatusWindow;
import WSA.unit.MapObject;
import WSA.unit.inventory.Inventory;
import de.matthiasmann.twl.Widget;

public class MenuController {

	WindowController windowController;
	Widget parent;
	
	InventoryMenu inventoryMenu;
	StatusBar statusBar;
	StatusWindow statusWindow;
	
	public MenuController(WindowController windowController, Widget parent) {
		this.windowController = windowController;
		this.parent = parent;
		
		initMenu();
	}
	
	private void initMenu () {
		inventoryMenu = new InventoryMenu(windowController, parent);
		statusBar = new StatusBar(windowController, parent, WidgetSizeCounter.getStatusBarSize());
		statusWindow = new StatusWindow(windowController, parent, WidgetSizeCounter.getStatsWindowSize());
	}
	
	public void openInventory(Inventory inventory) {
		inventoryMenu.openInventory(inventory);
	}
	public void closeInventory() {
		inventoryMenu.destroyMenu();
	}
	
	public void refresh(float time) {
		statusBar.refresh(time);
		
		statusWindow.refreshStats();
	}
	
	public void showStats(MapObject obj) {
		statusWindow.openStats(obj);
	}
	
	public void refreshStats() {
		statusWindow.refreshStats();
	}
	
	public void closeStats() {
		statusWindow.destroyMenu();
	}
}
