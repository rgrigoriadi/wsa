package WSA.ui.gui;

import de.matthiasmann.twl.Dimension;

public class WidgetSizeCounter {
	private static Dimension STATUS_BAR_SIZE = new Dimension(300, 40);
	private static int MARGIN_LABEL = 15;
	private static Dimension STATS_WINDOW_SIZE = new Dimension(200, 200);
	
	
	public static Dimension getStatusBarSize () {
		return STATUS_BAR_SIZE;
	}
	
	public static Dimension getStatsWindowSize () {
		return STATS_WINDOW_SIZE;
	}
	
	public static int getMarginLabel () {
		return MARGIN_LABEL;
	}
}
