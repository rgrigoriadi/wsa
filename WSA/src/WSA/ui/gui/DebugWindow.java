package WSA.ui.gui;

import java.util.ArrayList;

import WSA.Game;
import WSA.WindowController;
import de.matthiasmann.twl.Alignment;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Widget;

public class DebugWindow {

	static ArrayList<String> immediateStrings;

	static ArrayList<TimedString> timedStrings;

	static Widget parent;
	static WindowController wind;
	static Label immediateLabel;
	static Label timedLabel;

	static boolean isInited = false;

	static int maxSizeX = 0;

	public static void init(Widget par, WindowController wind) {
		DebugWindow.wind = wind;
		parent = par;
		immediateLabel = new Label();
		immediateLabel.setPosition(WidgetSizeCounter.getMarginLabel(), WidgetSizeCounter.getMarginLabel());
		immediateLabel.setTheme("label0");
		par.add(immediateLabel);

		immediateStrings = new ArrayList<String>();
		timedStrings = new ArrayList<DebugWindow.TimedString>();

		isInited = true;

	}

	public static void addImmediate(String str) {
		immediateStrings.add(str);
		updateLabel();
	}

	public static void addTimed(String str, float durantionIsSeconds) {
		timedStrings.add(TimedString.getTimeString(durantionIsSeconds, str));
	}

	public static void updateLabel() {
		String bigString = new String();

		for (int i = 0; i < immediateStrings.size(); i++) {
			bigString += ("#" + i + " Message : " + immediateStrings.get(i) + "\n");
		}

		immediateLabel.setText(bigString);
		immediateLabel.setAlignment(Alignment.CENTER);
		if (immediateLabel.computeTextWidth() + wind.buttonMargin * 2 > maxSizeX) {
			maxSizeX = immediateLabel.computeTextWidth() + wind.buttonMargin * 2;
		}
		immediateLabel.setSize(maxSizeX, immediateLabel.computeTextHeight() + wind.buttonMargin * 2);
		//		Game.printConsole("str size = " + strings.size() + " Contents \n" + bigString);

		if (timedStrings.size() == 0) {
			if (timedLabel != null) {
				parent.removeChild(timedLabel);
				timedLabel.destroy();
				timedLabel = null;
			}
		} else {
			if (timedLabel == null) {
				initTimedLabel();
			}
			
			clearTimedStrings();
			
			bigString = "";
		
			for (int i = 0; i < timedStrings.size(); i++) {
				bigString += (timedStrings.get(i).getString() + "\n");
			}
			
			timedLabel.setPosition(20, immediateLabel.getBottom() + 20);
			timedLabel.setText(bigString);
			timedLabel.setSize(timedLabel.computeTextWidth() + wind.buttonMargin * 2, timedLabel.computeTextHeight() + wind.buttonMargin * 2);
			timedLabel.setAlignment(Alignment.CENTER);
			
		}
	}

	public static void clearTimedStrings () {
		for (int i = 0; i < timedStrings.size(); i++) {
			if (!timedStrings.get(i).isActual()) {
				timedStrings.remove(i);
				i--;
			}
		}
	}
	
	public static void initTimedLabel() {
		timedLabel = new Label();
		timedLabel.setPosition(20, immediateLabel.getBottom() + 20);
		timedLabel.setTheme("label0");
		parent.add(timedLabel);

		isInited = true;

	}

	public static void clear() {
		immediateStrings.clear();
		immediateLabel.setText("");
	}

	public static boolean isInited() {
		return isInited;
	}

	public static class TimedString {
		private float creationTimeSeconds;
		private float durationSeconds;
		private String messageString;

		public static final float SHORT_MESSAGE_TIME = 1.0f;
		public static final float LONG_MESSAGE_TIME = 3.0f;

		public static TimedString getTimeString(float duration, String message) {
			TimedString ret = new TimedString();
			ret.creationTimeSeconds = Game.getGameTime();
			ret.durationSeconds = duration;
			ret.messageString = message;
			return ret;
		}
		
		public String getString () {
			return messageString;
		}

		public boolean isActual() {
			float leftTime = Game.getGameTime() - creationTimeSeconds;
			if (leftTime < durationSeconds) {
				return true;
			} else {
				return false;
			}
		}
	}

}
