package WSA.ui.gui.menu;

import WSA.WindowController;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Widget;

public abstract class Menu {
	Label backgroundLabel;
	
	WindowController window;
	Widget parent;
	
	public Menu(WindowController window, Widget parent) {
		this.window = window;
		this.parent = parent;
	}
	
	protected abstract void destroyMenu();
	
	public boolean isActive() {
		if (backgroundLabel == null) {
			return false;
		} else {
			return true;
		}
	}
}
