package WSA.ui.gui.menu;

import WSA.WindowController;
import WSA.unit.inventory.Inventory;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Widget;

public class InventoryMenu extends Menu {

	Inventory inventory;
	
	public InventoryMenu(WindowController window, Widget parent) {
		super(window, parent);
	}

	public void openInventory(Inventory inventory) {
		if (!isActive()) {
			this.inventory = inventory;
			
			backgroundLabel = new Label("INVENTORY");
			backgroundLabel.setTheme("label0");

			backgroundLabel.setSize(300, 400);

			backgroundLabel.setPosition(0, (parent.getHeight() - backgroundLabel.getHeight()) / 2);
			parent.add(backgroundLabel);
		}
	}

	@Override
	public void destroyMenu() {
		if (isActive()) {
			inventory = null;
			
			parent.removeChild(backgroundLabel);
			backgroundLabel.destroy();
			backgroundLabel = null;
		}
	}
}
