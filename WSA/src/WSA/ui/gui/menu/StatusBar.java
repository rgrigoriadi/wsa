package WSA.ui.gui.menu;

import WSA.Date;
import WSA.WindowController;
import WSA.ui.gui.WidgetSizeCounter;
import WSA.unit.ActiveObject;
import WSA.unit.MapObject;
import WSA.unit.objects.Unit;
import de.matthiasmann.twl.Alignment;
import de.matthiasmann.twl.Dimension;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Widget;

public class StatusBar extends Menu {

	public StatusBar(WindowController window, Widget parent, Dimension size) {
		super(window, parent);
		initStatusBar(size);
	}

	private void initStatusBar (Dimension size) {
		backgroundLabel = new Label("STATUSBAR");
		backgroundLabel.setTheme("label0");
		backgroundLabel.setSize(size.getX(), size.getY());
		backgroundLabel.setPosition(
				Alignment.TOPRIGHT.computePositionX(window.getWindowSize().getX(), backgroundLabel.getWidth()) - WidgetSizeCounter.getMarginLabel(),
				Alignment.TOPRIGHT.computePositionY(window.getWindowSize().getY(), backgroundLabel.getHeight()) + WidgetSizeCounter.getMarginLabel());
		backgroundLabel.setAlignment(Alignment.LEFT);
		parent.add(backgroundLabel);
	}

	@Override
	protected void destroyMenu() {
		backgroundLabel.destroy();
		backgroundLabel = null;
	}

	public void refresh(float time) {
		backgroundLabel.setText("   " + Date.getDateString(Date.getDate(time)) + "  ||  " + Date.getTimeString(Date.getDate(time)));	
	}

}
