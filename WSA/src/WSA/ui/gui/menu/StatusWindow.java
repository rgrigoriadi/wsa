package WSA.ui.gui.menu;

import WSA.Game;
import WSA.WindowController;
import WSA.ui.gui.WidgetSizeCounter;
import WSA.unit.ActiveObject;
import WSA.unit.AliveObject;
import WSA.unit.AtackingObject;
import WSA.unit.MapObject;
import WSA.unit.objects.Unit;
import de.matthiasmann.twl.Alignment;
import de.matthiasmann.twl.Dimension;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Widget;

public class StatusWindow extends Menu {

	Dimension size;
	
	MapObject obj; 
	
	public StatusWindow(WindowController windowController, Widget parent, Dimension size) {
		super(windowController, parent);
		this.size = size;
	}

	public void initLabel(Dimension size) {
		backgroundLabel = new Label("STATUS");
		backgroundLabel.setSize(size.getX(), size.getY());
		backgroundLabel.setTheme("label0");
		backgroundLabel.setPosition(
				Alignment.BOTTOMLEFT.computePositionX(window.getWindowSize().getX(), backgroundLabel.getWidth()) + WidgetSizeCounter.getMarginLabel(), 
				Alignment.BOTTOMLEFT.computePositionY(window.getWindowSize().getY(), backgroundLabel.getHeight()) - WidgetSizeCounter.getMarginLabel());
		parent.add(backgroundLabel);
	}
	
	public void openStats (MapObject obj) {
		if (!isActive()) {
			initLabel(size);
		}
		
		this.obj = obj;
		refreshStats();
	}

	@Override
	public void destroyMenu() {
		if (isActive()) {
			parent.removeChild(backgroundLabel);
			backgroundLabel.destroy();
			backgroundLabel = null;
		}
	}
	
	public void refreshStats () {
		if (!isActive()) {
			return;
		}
		
		if (!obj.isOnMapAndActual()) {
			destroyMenu();
			return;
		}
		
		String statString = "";
		statString += obj.getNameString() + "\n";
		
//		AliveObject.class.is
		
		if (obj.isAlive()) {
			AliveObject aObj = ((AliveObject) obj);
			statString += aObj.isMale() ? "Male" : "Female";
			statString += aObj.isGod() ? " God" : " Human";
			statString += " " + aObj.getAIName();
			statString += "\n" + aObj.getCurrentActionString() + "\n";
		}
		
		if (AtackingObject.class.isInstance(obj)) {
			AtackingObject aObj = (AtackingObject) obj;
			statString += "hit " + ((int) (aObj.getHitKillProbability() * 100)) + "% " + aObj.getHitDistance() + "m " + aObj.getHitCompleteTime() + "s " + ((int) (aObj.getHitKillProbability() * 100) / aObj.getHitCompleteTime()) + "%/s";
		}

		backgroundLabel.setText(statString);
	}
	
	public void drawStats (int ageInSecs, int strength, int agility, int intel, int faith/*, int proffessionLevel, TODO вывод типа профессии профессия*/) {
	
	}
}
