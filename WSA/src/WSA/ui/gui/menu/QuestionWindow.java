package WSA.ui.gui.menu;

import WSA.Game;
import WSA.WindowController;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.TextArea;
import de.matthiasmann.twl.Widget;
import de.matthiasmann.twl.textarea.SimpleTextAreaModel;

public class QuestionWindow {
	public Label container;
	public Button[] butts;
	public TextArea question;
	public String questionText;
	public Widget parent;
	public Label funcLabel;
	public WindowController gui;
	
	public QuestionWindow (String text, Button[] buttons, Label funcLabel, Widget parent, WindowController gui) {
		this.gui = gui;
		this.parent = parent;
		this.funcLabel = funcLabel;
		butts = buttons;
		questionText = text;
		
		init();
	}
	
	public void init () {
		if (butts.length == 0) {
			Game.print("enableQuestionLabel error : no answerButtons provided");
			return;
		}

		if (butts.length > 3) {
			Game.print("enableQuestionLabel error : too many answerButtons provided (" + butts.length + ")");
			return;
		}

		question = new TextArea();
		SimpleTextAreaModel stma = new SimpleTextAreaModel(questionText);
		question.setModel(stma);
		
		container = new Label();
		
		container.setTheme("label0");
		container.setSize(funcLabel.getWidth(), (gui.buttonSizeD + gui.buttonMargin) * 2 + gui.buttonMargin);
		container.setPosition(funcLabel.getX(), funcLabel.getY() - container.getHeight());

		question.setPosition(container.getX() + 2 * gui.buttonMargin, container.getY() + (gui.buttonMargin * 2 + gui.buttonSizeD - gui.TEXTHEIGHT) / 2);
		question.setSize(container.getWidth() - 4 * gui.buttonMargin, gui.buttonMargin * 2 + gui.buttonSizeD);
		question.setTheme("zeroBackground");
		
		parent.add(container);

		parent.add(question);
		
		for (int i = 0; i < butts.length; i++) {
			butts[i].setSize(gui.buttonSizeD, gui.buttonSizeD);
			butts[i].setPosition(container.getX() + gui.buttonMargin + (gui.buttonSizeD + gui.buttonMargin) * i, container.getY() + gui.buttonMargin * 2 + gui.buttonSizeD);
			parent.add(butts[i]);
		}
	}
	
	public void destroy() {
		for (int i = 0; i < butts.length; i++) {
			if (butts[i] != null) {
				parent.removeChild(butts[i]);
				butts[i].destroy();
			}
		}
		questionText = null;
		parent.removeChild(container);
		parent.removeChild(question);
		parent.destroy();
		question.destroy();
	}
	
}
