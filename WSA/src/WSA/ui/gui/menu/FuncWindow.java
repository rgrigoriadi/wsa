package WSA.ui.gui.menu;

import WSA.WindowController;
import de.matthiasmann.twl.BoxLayout;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Widget;

public class FuncWindow {
	
	Widget parent;
	Button[][] funcMenu;
	Label container;
	WindowController gui;
	
	public FuncWindow (Widget parent, WindowController windowController) {
		this.parent = parent;
		this.gui = windowController;
		container = new Label("FUNCTIONS");
		container.setTheme("label0");
		setFuncLabelCapacity(windowController.buttonNumberXMin, windowController.buttonNumberYDefault);
		
//		container.setPosition((int) windowController.statWindow.getWidth(), parent.getHeight() - container.getHeight());
		parent.add(container);
	
	}
	
	public void destroy() {
		funcMenu = null;
		container.destroy();
	}

	public boolean setFuncLabelCapacity(int x, int y) {
		if (x == 0 && y == 0) {
			destroy();
			return true;
		}

		if (x > gui.buttonMargin) {
			x = gui.buttonMargin;
		} else if (x < gui.buttonNumberXMin) {
			x = gui.buttonNumberXMin;
		}

		if (y > gui.buttonNumberYMax) {
			y = gui.buttonNumberYMax;
		} else if (y <= 0) {
			y = 1;
		}

		y = gui.buttonNumberYDefault;

		//TODO

		gui.buttonNumberX = x;
		gui.buttonNumberY = y;

		int funcWidth = x * gui.buttonSizeD + gui.buttonMargin * (x + 1);
		int funcHeight = gui.mainPannelHeight;

		container.setSize(funcWidth, funcHeight);
		funcMenu = null;
		funcMenu = new Button[x][y];

		return true;
	}

	public boolean addButtonToFuncLabel(int x, int y, Button butt) {

		if (funcMenu.length == 0 || funcMenu[0].length == 0) {
			return false;
		}

		if (x >= funcMenu.length)
			return false;

		if (y >= funcMenu[0].length)
			return false;

		if (funcMenu[x][y] != null) {
			funcMenu[x][y].destroy();
		}

		funcMenu[x][y] = butt;
		butt.setSize(gui.buttonSizeD, gui.buttonSizeD);
		butt.setPosition(container.getX() + gui.buttonMargin + (x * (gui.buttonMargin + gui.buttonSizeD)), container.getY() + gui.buttonMargin + (y * (gui.buttonMargin + gui.buttonSizeD)));
		parent.add(butt);
		return true;
	}
	
	public Label getLabel () {
		return container;
	}
}
