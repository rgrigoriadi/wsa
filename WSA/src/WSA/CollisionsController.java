package WSA;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.map.MapController;
import WSA.map.MapGroup;
import WSA.player.ViewController;
import WSA.unit.AliveObject;
import WSA.unit.MapObject;
import WSA.unit.ObjectController;

public class CollisionsController {
	private MapController map;
	private ObjectController objectController;
	public static final float MIN_CAM_HEIGHT_ON_MAP = 0.5f;
	public static final float MAX_CAM_HEIGHT_ON_MAP = 500.0f;

	public static final float POSITION_ERROR = 0.1f;

	public CollisionsController(MapController map, ObjectController objContoller) {
		this.map = map;
		objectController = objContoller;
	}

	public MapObject getObjectByCursor(ViewController view) {
		Vector2f mapPos = getCursorIntersectionWithMap(view);
		if (mapPos == null) {
			return null;
		}

		MapObject result = null;

		float step = 0.1f;

		float maxHeight = 3.0f;

		float h = getMapHeight(mapPos);
		float x = mapPos.x;
		float z = mapPos.y;

		Vector3f dir = Vector3f.sub(view.getPosition(), view.getCamAbsoluteDirection(), null);
		dir.normalise(null);

		result = null;
		while (h <= getMapHeight(x, z) + maxHeight) {

			h += dir.y * step;

			x += dir.x * step;
			z += dir.z * step;

			MapObject foundObject;

			foundObject = getObjectSimple(x, z, getMapErrorByDistanceToMap(getDistanceCursorToMap(view)));

			if (foundObject != null) {
				if (foundObject.getObjectHeight() > h - getMapHeight(x, z)) {
					//				result = foundObject;
					result = foundObject;
				} else { // не объект ниже, чем луч курсора
					foundObject = null;
				}
			}
		}

		return result;
	}

	public MapObject getObjectSimple(float posX, float posY, float allowedDistanceError) { // TODO выбор не точно, а в зависимости от дальности, можно промахнуться
		MapObject ret = null;

		for (int i = 0; i < objectController.getNumberOfObjects(); i++) {
			MapObject object = objectController.getObject(i);

			float dist = (float) Math.sqrt(((object.getPosition().x - posX) * (object.getPosition().x - posX) + (object.getPosition().z - posY) * (object.getPosition().z - posY)));

			//Game.print(dist + " - dist, " + unit.unitRadius);

			if (dist < object.getObjectRadius() + allowedDistanceError) {
				ret = object;
			}
		}

		return ret;
	}

	public ObjectSelection getUnitsByRect(Vector2f[] coord, int playerId) {
		ObjectSelection ret = new ObjectSelection();

		if (coord.length != 4)
			return ret;

		float maxX = Float.NaN;

		for (int i = 0; i < 4; i++)
			if (coord[i].x >= maxX || Float.isNaN(maxX))
				maxX = coord[i].x;

		boolean isSelectionNotAlive = true;

		for (int i = 0; i < objectController.getNumberOfObjects(); i++) {
			MapObject obj = objectController.getObject(i);

			boolean ok = false;

			Vector2f objectPosition = new Vector2f(obj.getPosition().x, obj.getPosition().z);

			ok = isPointInPolygone2d(objectPosition, maxX, coord);

			if (ok == true) {
				if (obj.isAlive()) {
					if (obj.getPlayerId() == playerId) {
						ret.add(obj);

						if (isSelectionNotAlive) {
							for (int j = 0; j < ret.getSize(); j++) {
								if (!ret.getObject(j).isAlive()) {
									ret.remove(j);
									j--;
								}
							}
						}

						isSelectionNotAlive = false;
					}
				} else if (isSelectionNotAlive) {
					ret.add(obj);
				}

			}

		}

		if (ret.getSize() > 0) {
			return ret;
		} else {
			return null;
		}

	}

	public Vector2f getVectorIntersectionWithMap(Vector3f pos, Vector3f dir) {
		Vector2f ret = new Vector2f(0.0f, 0.0f);
		//MapGroup group = this.getGroup(pos.x, pos.z);

		float md;

		float h;
		float mh;

		float dx = dir.x - pos.x;
		float dy = dir.y - pos.y;
		float dz = dir.z - pos.z;

		float dxz = (float) Math.sqrt(dx * dx + dz * dz);

		float x = pos.x;
		float y = pos.y;
		float z = pos.z;

		float dist = 0.0f;
		while (true) {

			dist = (float) Math.sqrt((x - pos.x) * (x - pos.x) + (y - pos.y) * (y - pos.y) + (z - pos.z) * (z - pos.z));

			mh = getMapHeight(x, z);
			h = y - mh;
			md = dist / 100.0f; // шаг просмотра

			if (md < 0.1f)
				md = 0.1f;

			if (h <= 0) {
				break; // нашли координаты, с ними выходим.
			}

			if (dist > 50000.0f) {
				break;
			}

			float k = md / dxz;

			float dxm = dx * k;
			float dzm = dz * k;
			float dym = dy * k;

			x += dxm;
			y += dym;
			z += dzm;

		}
		Game.print("Intersection step = " + md);
		ret.set(x, z);

		return ret;
	}

	public float getDistanceCursorToMap(ViewController view) {

		Vector3f dir = view.getCamAbsoluteDirection();
		Vector3f pos = view.getPosition();

		float md;

		float h;
		float mh;

		float dx = dir.x - pos.x;
		float dy = dir.y - pos.y;
		float dz = dir.z - pos.z;

		float dxz = (float) Math.sqrt(dx * dx + dz * dz);

		float x = pos.x;
		float y = pos.y;
		float z = pos.z;

		float dist = 0.0f;
		while (true) {

			dist = (float) Math.sqrt((x - pos.x) * (x - pos.x) + (y - pos.y) * (y - pos.y) + (z - pos.z) * (z - pos.z));

			mh = getMapHeight(x, z);
			h = y - mh;
			md = dist / 100.0f; // шаг просмотра

			if (md < 0.1f)
				md = 0.1f;

			if (h <= 0) {
				break; // нашли координаты, с ними выходим.
			}

			if (dist > 50000.0f) {
				break;
			}

			float k = md / dxz;

			float dxm = dx * k;
			float dzm = dz * k;
			float dym = dy * k;

			x += dxm;
			y += dym;
			z += dzm;

		}

		return dist;
	}

	public Vector2f getCursorIntersectionWithMap(ViewController view) {
		return getVectorIntersectionWithMap(view.getPosition(), view.getCursorDirection());
	}

	public static Vector2f getVectorIntersectionWithZeroLevel(Vector3f pos, Vector3f dir) {
		Vector2f ret = new Vector2f(pos.x, pos.z);
		if (dir.y > pos.y) {
			return ret;
		}
		float scale = (float) (pos.y / (Math.abs(dir.y - pos.y))); //pos.y / Math.sqrt((dir.x - pos.x) * (dir.x - pos.x) + (dir.y - pos.y) * (dir.y - pos.y) + (dir.z - pos.z) * (dir.z - pos.z)));

		ret.x += (dir.x - pos.x) * scale;
		ret.y += (dir.z - pos.z) * scale;
		//ret.y *= scale;

		return ret;
	}

	public boolean isPointInPolygone2d(Vector2f point, float maxX, Vector2f[] vertexes) {
		int numberOfIntersections = 0;

		Vector2f rayPoint = new Vector2f(maxX, point.y);

		Vector2f p1 = point;
		Vector2f p2 = rayPoint;

		for (int i = 0; i < vertexes.length; i++) {
			Vector2f p3 = vertexes[i];
			Vector2f p4 = (i + 1 < vertexes.length ? vertexes[i + 1] : vertexes[0]);
			float uaUp = (p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x);
			float ubUp = (p2.x - p1.x) * (p1.y - p3.y) - (p2.y - p1.y) * (p1.x - p3.x);
			float uDown = (p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y);

			if (uDown == 0) {
				;
			} else if ((uaUp == 0 || ubUp == 0) && uDown == 0) {
				numberOfIntersections++;
			} else {
				float ua = uaUp / uDown;
				float ub = ubUp / uDown;
				if (ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f) {
					numberOfIntersections++;
				} else {
					;
				}
			}

		}

		if (numberOfIntersections % 2 == 1) {
			return true;
		} else {
			return false;
		}

	}

	public ArrayList<MapObject> getCollidedObjects(MapObject objectToCheck) {
		ArrayList<MapObject> potentiallyCollided = objectController.getListOfPotentiallyCollidedObjects(objectToCheck);
		ArrayList<MapObject> collidedObjects = new ArrayList<MapObject>();

		for (int i = 0; i < potentiallyCollided.size(); i++) {
			MapObject secondObject = potentiallyCollided.get(i);

			if (!secondObject.isSolid()) {
				continue;
			}

			float x1 = objectToCheck.getPosition().x;
			float x2 = secondObject.getPosition().x;

			float dx = x1 - x2;

			float y1 = objectToCheck.getPosition().y;
			float y2 = secondObject.getPosition().y;

			float dy = y1 - y2;

			float minDistance = objectToCheck.getObjectRadius() + secondObject.getObjectRadius();
			float currentDistance = (float) Math.sqrt(dx * dx + dy * dy);

			if (minDistance > currentDistance) {
				collidedObjects.add(secondObject);
			}
		}

		if (collidedObjects.size() == 0) {
			return null;
		} else {
			return collidedObjects;
		}
	}

	public void processCollisions(MapObject collider, ArrayList<MapObject> collidedObjects) {
		if (collidedObjects == null) {
			return;
		} else {

		}

		for (int i = 0; i < collidedObjects.size(); i++) {
			MapObject secondObject = collidedObjects.get(i);

			if (!secondObject.isSolid()) {
				continue;
			}

			float x1 = collider.getPosition().x;
			float x2 = secondObject.getPosition().x;

			float dx = x1 - x2;

			float y1 = collider.getPosition().z;
			float y2 = secondObject.getPosition().z;

			float dy = y1 - y2;

			float minDistance = collider.getObjectRadius() + secondObject.getObjectRadius();
			float currentDistance = (float) Math.sqrt(dx * dx + dy * dy);

			if (minDistance > currentDistance) {
				float newX = x2 + dx / currentDistance * minDistance;
				float newY = y2 + dy / currentDistance * minDistance;
				collider.setPosition(new Vector3f(newX, getMapHeight(newX, newY), newY));
				//				Game.printLongTimed("HAS Collisions! Obj " + collider.getId() + " and " + secondObject.getId() + " || r1 + r2 = " + minDistance + " currentDistance = " + currentDistance);
				//				Game.printConsole("HAS Collisions! Obj " + collider.getId() + " and " + secondObject.getId() + " || r1 + r2 = " + minDistance + " currentDistance = " + currentDistance);
			}
		}
	}

	public float getDistanceToAnotherObject(Vector3f pos, Class ObjectType) {

		float min = 999;

		for (int i = 0; i < objectController.getNumberOfObjects(); i++) {
			if (ObjectType.isInstance(objectController.getObject(i))) {
				float dist = Util.getDistance(pos, objectController.getObject(i).getPosition());

				if (dist < min) {
					min = dist;
				}
			}
		}

		return min;

	}

	public float getMapHeight(float x, float z) {
		return map.getHeight(x, z);
	}
	
	public Vector3f getPositionOnMap(float x, float z) {
		return new Vector3f(x, getMapHeight(x, z), z);
	}

	public float getMapHeight(Vector2f pos) {
		return map.getHeight(pos.x, pos.y);
	}

	public float getMapErrorByDistanceToMap(float distance) {
		return distance / 200.0f;
	}

	public MapGroup getMapGroup(Vector2f point) {
		return map.getGroup(point);
	}
}
