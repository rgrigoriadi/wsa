package WSA.unit;

import java.util.ArrayList;

import WSA.CollisionsController;
import WSA.Game;


public class ObjectController {
	
	ArrayList<MapObject> objects;
	
	public ObjectController() {
		objects = new ArrayList<MapObject>();
	}
	
	
	public void refresh (CollisionsController collisionsController) {		
		for (int i = 0; i < objects.size(); i++) {
			MapObject object = objects.get(i);
			object.refresh(collisionsController);
			
			if (MovingActiveObject.class.isInstance(object)) {
				collisionsController.processCollisions(object, collisionsController.getCollidedObjects(object));
			}
			
			if (!objects.contains(object)) { // Если мы умудрились удалить (убить, уничтожить) объект в процессе обхода
				i--;
			}
		}
		
		Game.print("ObjectController handles " + getNumberOfObjects() + " objects");
	}
	
	public int getNumberOfObjects(){
		return objects.size();
	}
	
	public MapObject getObject(int index) {
		return objects.get(index);
	}
	
	public void createObject (MapObject object) {
		objects.add(object);
		object.onCreateOnMap();
	}
	
	public void removeObject (MapObject object) {
		objects.remove(object);
	}
	
	public boolean contains (MapObject obj) {
		return objects.contains(obj);
	}
	
	public ArrayList<MapObject> getListOfPotentiallyCollidedObjects (MapObject currentObject) {
		ArrayList<MapObject> result = new ArrayList<MapObject>(objects);
		
		result.remove(currentObject);
		
		return result; // TODO получение списка объектов, находящихся в этом и соседних чанках
	}
	
}
