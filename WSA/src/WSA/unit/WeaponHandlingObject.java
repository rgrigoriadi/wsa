package WSA.unit;

import WSA.unit.inventory.items.AtackItem;
import WSA.unit.inventory.items.AtackItemInterface;
import WSA.unit.objects.DroppedItemObject;

public interface WeaponHandlingObject {
	public void takeWeaponInHands(DroppedItemObject obj);
	public AtackItem getWeaponInHands();
	public void dropWeapon();
}
