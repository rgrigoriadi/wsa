package WSA.unit;

public interface AtackingObject {
	
	public static final float DIDNT_HIT = -1.0f;
	
	public float getHitCompleteTime();
	public float getHitStartTime();
	public void onHitStart();
	public void onHit();
	public void onHitEnd();
	
	public boolean didHitDuringAtack();
	
	
	public float getHitPrepareTime();
	public float getHitPostpareTime();
	
	public float getHitKillProbability();
	public float getHitDistance();
	
	
}
