package WSA.unit.action;

import WSA.unit.MovingActiveObject;

public abstract class MoveActionClass extends Action {
	
	@Override
	public Class getRequiredObjectClass () {
		return MovingActiveObject.class;
	}
}
