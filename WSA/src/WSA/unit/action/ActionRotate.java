package WSA.unit.action;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.Game;
import WSA.unit.ActiveObject;
import WSA.unit.MapObject;
import WSA.unit.MovingActiveObject;

public class ActionRotate extends MoveActionClass {

	private Vector2f targetAngles;
	public ActionRotate (Vector2f targetAngles) {
		this.targetAngles = targetAngles;
	}
	
	
	
	@Override
	public Vector3f getPosition() {
		return null;
	}

	@Override
	public float act(ActiveObject object, float timeProvidedMove) {
		if (timeProvidedMove <= 0)
			return 0;
		
		if (isFinished() == true)
			return 0;
			
		
		targetAngles.x = MovingActiveObject.correctAngle(targetAngles.x, false);
		targetAngles.y = MovingActiveObject.correctAngle(targetAngles.y, false);
		
//		Game.printConsole("Acting current angle = " + object.getRotation().x + " target = " + targetAngles.x);
		
		float time = rotateToAngle((MovingActiveObject) object, targetAngles.x, targetAngles.y, timeProvidedMove);
		
		if (isCompleted(object, targetAngles)) {
			onComplete();
		}
		
		return time;
	}

	public float rotateToAngle(MovingActiveObject objectToRotate, float xAngle, float yAngle, float timeProvided) {

		float dx = xAngle - objectToRotate.getRotation().x;
		float dy = yAngle - objectToRotate.getRotation().y;

		dx = MovingActiveObject.correctAngle(dx, false);
		dy = MovingActiveObject.correctAngle(dy, false);

		float mdx = -(360 - dx);
		float mdy = -(360 - dy);

		//Game.print("DX + MDX " + dx + " " + mdx);

		if (Math.abs(mdx) < dx) {
			dx = mdx;
		}

		if (Math.abs(mdy) < dy) {
			dy = mdy;
		}

		// Game.print("Angles dx, dy " + dx + " " + dy);

		return rotate(objectToRotate, dx, dy, timeProvided);
	}
	
	public float rotate(MovingActiveObject objectToRotate, float xAngle, float yAngle, float timeProvided) { // returns ammount of time in milliseconds, spent to finish the action, 1000 if action is not finished
		 
		float timeSpent = 0.0f;
		
		xAngle = MovingActiveObject.correctAngle(xAngle, true);
		//yAngle = correctAngle(yAngle, true);

		objectToRotate.getRotation().x = MovingActiveObject.correctAngle(objectToRotate.getRotation().x, false);
		//rotation.y = correctAngle(rotation.y, false);

		if (xAngle != 0.0f) {
			if ((timeProvided * objectToRotate.getMaxRotationSpeed().x > Math.abs(xAngle)) || (objectToRotate.getMaxRotationSpeed().x == 0)) {
				objectToRotate.getRotation().x += xAngle;
				timeSpent = xAngle / objectToRotate.getMaxRotationSpeed().x;
			} else { // действие не окончено
				objectToRotate.getRotation().x += objectToRotate.getMaxRotationSpeed().x * timeProvided * (xAngle > 0 ? 1 : -1);
				timeSpent = timeProvided;
			}
		}

		if (yAngle != 0.0f) {
			if (objectToRotate.getRotation().y + yAngle > objectToRotate.getMaxYAngle()) {
				yAngle = objectToRotate.getMaxYAngle() - objectToRotate.getRotation().y;
			} else if (objectToRotate.getRotation().y + yAngle < objectToRotate.getMinYAngle()) {
				yAngle = objectToRotate.getMinYAngle() - objectToRotate.getRotation().y;
			}
			if ((timeProvided * objectToRotate.getMaxRotationSpeed().y > yAngle) || (objectToRotate.getMaxRotationSpeed().y == 0)) {
				objectToRotate.getRotation().y += yAngle;
				float timeSpentY = yAngle / objectToRotate.getMaxRotationSpeed().y;
				if (timeSpentY > timeSpent)
					timeSpent = timeSpentY;
			} else { // действие не окончено
				objectToRotate.getRotation().y += objectToRotate.getMaxRotationSpeed().y * Game.getDeltaInSeconds() * (yAngle > 0 ? 1 : -1);
				timeSpent = timeProvided;
			}
		}

		objectToRotate.getRotation().x = MovingActiveObject.correctAngle(objectToRotate.getRotation().x, false);
		objectToRotate.getRotation().y = MovingActiveObject.correctAngle(objectToRotate.getRotation().y, false);

		//Game.print(" Is rotation completed ? " + (ret == 1 ? "false" : "true"));

		//Game.print("Rotated");
		
		return timeSpent;
	}
	
	public boolean isCompleted (MapObject objectToRotate, Vector2f targetAngles) {
		if ((targetAngles.x - MovingActiveObject.correctAngle(objectToRotate.getRotation().x, false)) < CollisionsController.POSITION_ERROR && (targetAngles.y - MovingActiveObject.correctAngle(objectToRotate.getRotation().y, false)) < CollisionsController.POSITION_ERROR) {
			return true;
		} else {
			return false;
		}
	}



	@Override
	public String getStatusString(MapObject obj) {
		return "Action Rotate " + (targetAngles.x - MovingActiveObject.correctAngle(obj.getRotation().x, false)) + " deg left";
	}
}
