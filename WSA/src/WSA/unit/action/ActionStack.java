package WSA.unit.action;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector3f;

public class ActionStack {

	private ArrayList<Action> stack;

	public ActionStack() {
		stack = new ArrayList<Action>();
	}

	public void addAction(Action action) {
		stack.add(action);
	}
	
	public void addActionFirst(Action action) {
		stack.add(0, action);
	}

	//	public UnitAction getAction (float x, float z) {
	//		
	//		//получаем действие по позиции по аргументам
	//		
	//		//если действие - патруль, что есть группа действий, надо просматривать весь патруль
	//	
	//		//else
	//		return null;
	//	}

	public ArrayList<actionChainElement> getActionChain() {
		ArrayList<actionChainElement> ret = new ArrayList<actionChainElement>();

		for (int i = 0; i <= stack.size() - 1; i++) {
			if (stack.get(i).getPosition() != null) { // нуль возвращается, когда считать положение нерационально, как правило, когда действие не должно отображаться в этом списке
				ret.add(new actionChainElement(stack.get(i).getPosition()));
			}
		}

		if (ret.size() > 0) {
			return ret;	
		} else {
			return null;
		}
	}

	public void removeAction(int index) {
		if (stack.size() > index) {
			stack.remove(index);
		}
	}

	public Action getNextAction() {
		removeFinished();
		if (!stack.isEmpty())
			return stack.get(0); // берем первое действие в стеке
		else
			return null;
	}

//	public void onActionCompleted() {
//		if (stack.size() > 0) {
//			stack.remove(0); 
//		}
//	}

	public void clearStack() {
		stack.clear();
	}

	public boolean isEmpty() {
		removeFinished();
		return stack.isEmpty();
	}
	
	public int size () {
		return stack.size();
	}

	public void removeFinished() {
		if (!stack.isEmpty())
			if (stack.get(0).isFinished() == true)
				stack.remove(0); // удаляем первое действие в стеке, если оно выполнено
	}

	public class actionChainElement { // только для вывода данных на карту через getActionChain
//		public int type;
		public Vector3f position;

		public actionChainElement(Vector3f pos) {
			position = pos;
		}
	}
}
