package WSA.unit.action;

import org.lwjgl.util.vector.Vector3f;

import WSA.unit.ActiveObject;
import WSA.unit.MapObject;

public abstract class Action {

	private boolean isFinished;
	
	public Action() {
		isFinished = false;
	}
		
	public abstract Vector3f getPosition ();
	
	public boolean isFinished () {
		return isFinished;
	}
	
	public void onComplete () {
		isFinished = true;
	}

	public abstract float act(ActiveObject object, float timeProvidedMove);
	
	public abstract Class getRequiredObjectClass ();
	
	public boolean isActionAppliableForObject(MapObject obj) {
		if (getRequiredObjectClass().isInstance(obj)) {
			return true;
		} else {
			return false;
		}
	}
	
	public ActiveObject getAppliableObjectInstance(MapObject obj) {
		if (isActionAppliableForObject(obj)) {
			return (ActiveObject) (getRequiredObjectClass().cast(obj));
		} else {
			return null;
		}
	}
	
	public abstract String getStatusString(MapObject obj);
}
