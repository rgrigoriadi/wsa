package WSA.unit.action;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.Game;
import WSA.Util;
import WSA.unit.ActiveObject;
import WSA.unit.MapObject;
import WSA.unit.MovingActiveObject;

public class ActionMove extends MoveActionClass {

	private Vector3f targetPosition;
	
	private MapObject targetObject;
	
	private float targetdiastance;
	
	
	private CollisionsController collisionsController;
	
	public ActionMove (Vector3f targetPosition, CollisionsController collisionsController) {
		this.targetPosition = targetPosition;
		this.collisionsController = collisionsController;
		this.targetdiastance = CollisionsController.POSITION_ERROR;
	}
	
	public ActionMove (MapObject targetObject, float targetDistance, CollisionsController collisionsController) {
		this.targetObject = targetObject;
		this.collisionsController = collisionsController;
		
		this.targetdiastance = targetDistance + targetObject.getObjectRadius();
	}
	
	public Vector3f getTargetPosition () {
		if (targetObject == null) {
			return targetPosition;
		} else {
			return targetObject.getPosition();
		}
	}
	
	@Override
	public Vector3f getPosition() {
		return getTargetPosition();
	}

	@Override
	public float act(ActiveObject object, float timeProvidedMove) {
		if (timeProvidedMove <= 0)
			return 0;
		
		if (isFinished() == true)
			return 0;
		
		float time = move((MovingActiveObject) object, getTargetPosition(), timeProvidedMove, collisionsController); 
//		Game.printLongTimed("Moved");
		if (isCompleted(object)) {
			onComplete();	
//			Game.printLongTimed("MoveCompleted");
		}
		
		return time;
	}
	
	public float move(MovingActiveObject objectToMove, Vector3f target, float timeProvided, CollisionsController col) { // для движения по карте
		float x = target.x;
		float z = target.z;

		float timeSpent = 0.0f;
		
		// поворачиваем

		float xAngle = (float) (Math.atan((z - objectToMove.getPosition().z) / (x - objectToMove.getPosition().x)) * 180.0f / Math.PI);

		if (x < objectToMove.getPosition().x) {
			xAngle += 180.0f;
		}
		
		MovingActiveObject.correctAngle(xAngle, false);
				
		//Game.print("ATAN -inf -1 0 1 +inf " + Math.atan(-100000) + " " + Math.atan(-1) + " " + Math.atan(0) + " " + Math.atan(1) + " " + Math.atan(1000000)); 
		//Game.print("ASIN -inf -1 0 1 +inf " + Math.asin(-100000) + " " + Math.asin(-1) + " " + Math.asin(0) + " " + Math.asin(1) + " " + Math.asin(1000000));
		
		float rotateTime = 0.0f;
		if (timeProvided - timeSpent > 0.0f) {
			ActionRotate rot = new ActionRotate(new Vector2f(xAngle, 0.0f));
			rotateTime = rot.act(objectToMove, timeProvided - timeSpent);
		}
		
		timeSpent += rotateTime;
		
		// ходим

		float moveTime = 0.0f;
		if (timeProvided - timeSpent > 0.0f) {
			moveTime = moveForward(objectToMove, (float) Math.sqrt((objectToMove.getPosition().x - x) * (objectToMove.getPosition().x - x) + (objectToMove.getPosition().z - z) * (objectToMove.getPosition().z - z)), timeProvided - timeSpent, collisionsController);
		}

		timeSpent += moveTime;
	
		return timeSpent;

	}
	
	public float moveForward(MovingActiveObject objectToMove, float distance, float timeProvided, CollisionsController col) { // для движения по карте
		
		float timeSpent = 0.0f;
		
		if (objectToMove.getAcceleration().x == 0.0f) {
			objectToMove.getSpeed().x = objectToMove.getMaxSpeed().x;
		} else {
			objectToMove.getSpeed().x += objectToMove.getAcceleration().x * timeProvided; // Ускоряемся	
		}
		
		if (objectToMove.getSpeed().x > objectToMove.getMaxSpeed().x) {
			objectToMove.getSpeed().x = objectToMove.getMaxSpeed().x;
		}
		
		float maxDistance = objectToMove.getSpeed().x * Game.getDeltaInSeconds();
		
		if (maxDistance > distance) {
			timeSpent += distance / objectToMove.getSpeed().x;// position.x = targetPosition.x;
			objectToMove.moveChangePosition(timeSpent);//position.z = targetPosition.z;
			objectToMove.getPosition().y = col.getMapHeight(objectToMove.getPosition().x, objectToMove.getPosition().z);
			// значит тормозим
			objectToMove.getSpeed().x = 0.0f;
		} else {
			objectToMove.moveChangePosition(timeSpent);
			objectToMove.getPosition().y = col.getMapHeight(objectToMove.getPosition().x, objectToMove.getPosition().z);
			timeSpent = timeProvided; // потратили все время
		}
		
		objectToMove.onPositionCoordinatesChange();
		//Game.print("Moving");
		return timeSpent;
	}
	
	public boolean isCompleted (MapObject obj) {
		if (targetObject != null) {
			return isCompleted(targetObject, obj);
		} else {
			return isCompleted(getTargetPosition(), obj);
		}
	}
	
	public boolean isCompleted (MapObject targetObject, MapObject movingObject) {
		if (targetObject.isOnMapAndActual() && Util.getDistanceXZ(targetObject.getPosition(), movingObject.getPosition()) < targetdiastance + movingObject.getObjectRadius()) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isCompleted (Vector3f targetPosition, MapObject movingObject) {
		if (Util.getDistanceXZ(targetPosition, movingObject.getPosition()) < targetdiastance) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String getStatusString(MapObject obj) {
		return "Action Move " + (int) Util.getDistanceXZ(getTargetPosition(), obj.getPosition()) + " meters left";
	}

}
