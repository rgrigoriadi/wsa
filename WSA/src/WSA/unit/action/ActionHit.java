package WSA.unit.action;

import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.Game;
import WSA.Util;
import WSA.unit.ActiveObject;
import WSA.unit.AtackingObject;
import WSA.unit.MapObject;

public class ActionHit extends Action {

	private boolean mustHitTillDestroy;

	private MapObject target;
	private CollisionsController collisionsController;

	public ActionHit(boolean mustHitTillDestroy, MapObject target, CollisionsController collisionsController) {
		this.mustHitTillDestroy = mustHitTillDestroy;
		this.target = target;
		this.collisionsController = collisionsController;
	}

	@Override
	public Vector3f getPosition() {
		return null;
	}

	@Override
	public float act(ActiveObject object, float timeProvidedMove) {
		float timeSpent = 0.0f;

		if (!isTargetAlive()) {
			onComplete();
			return timeSpent;
		}
		
		AtackingObject aObj = (AtackingObject) object;

		if (Util.getDistanceXZ(object.getPosition(), target.getPosition()) > object.getObjectRadius() + target.getObjectRadius() + aObj.getHitDistance()) {
			aObj.onHitEnd();
			object.addActionFirst(new ActionMove(target, aObj.getHitDistance(), collisionsController));
			return timeSpent;
		}

		if (aObj.getHitStartTime() == AtackingObject.DIDNT_HIT) {
			aObj.onHitStart();
		}

		float timeFromHitStart = Game.getGameTime() - aObj.getHitStartTime();

		if (timeFromHitStart > aObj.getHitCompleteTime()) {
			aObj.onHitEnd();
			if (mustHitAgain()) {
				aObj.onHitStart();
			} else {
				onComplete();
			}
		} else if (timeFromHitStart > aObj.getHitPrepareTime()) {
			if (!aObj.didHitDuringAtack()) {
				aObj.onHit();
				onHit(aObj, target);
			}
		}
		
		timeSpent = timeProvidedMove;

		return timeSpent;
	}

	public void onHit(AtackingObject atackingObject, MapObject atackedObject) {
		if (Math.random() < atackingObject.getHitKillProbability()) {
			atackedObject.onDestroy();
		}
	}

	public boolean isTargetAlive() {
		return target.isOnMapAndActual();
	}
	
	public boolean mustHitAgain() {
		if (mustHitTillDestroy && isTargetAlive()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Class getRequiredObjectClass() {
		return AtackingObject.class;
	}

	@Override
	public String getStatusString(MapObject obj) {
		int progress = (int) (100.0 * (Game.getGameTime() - ((AtackingObject) obj).getHitStartTime()) / ((AtackingObject) obj).getHitCompleteTime());
		return "Action Hit hitingProgress = " + progress + "%";
	}
}
