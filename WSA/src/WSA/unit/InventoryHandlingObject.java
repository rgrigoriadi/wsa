package WSA.unit;

import WSA.unit.inventory.Inventory;

public interface InventoryHandlingObject {
	public Inventory getInventory();
}
