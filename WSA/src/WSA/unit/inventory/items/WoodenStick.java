package WSA.unit.inventory.items;

public class WoodenStick extends AtackItem {

	public WoodenStick(float weight) {
		super(0.5f);
	}

	@Override
	public float getAtackMultiplier() {
		return 1.1f;
	}

	@Override
	public String getName() {
		return "Little Wooden Stick";
	}

}
