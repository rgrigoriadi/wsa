package WSA.unit.inventory.items;

public abstract class Item {

	float weight;
	
	public Item (float weight) {
		this.weight = weight;
	}
	
	public float getWeight() {
		return weight;
	}
	
	public abstract String getName();
	
}
