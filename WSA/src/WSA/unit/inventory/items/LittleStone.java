package WSA.unit.inventory.items;

public class LittleStone extends AtackItem {

	public LittleStone() {
		super(0.3f);
	}
	
	@Override
	public float getAtackMultiplier() {
		return 1.1f;
	}

	@Override
	public String getName() {
		return "Little Stone";
	}

}
