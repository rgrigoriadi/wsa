package WSA.unit.inventory;

public class Inventory {
	float weightCapacity;
	int itemsCapacity;
	
	public Inventory(float weightCapacity) {
		setWeight(weightCapacity);
	}
	
	public void setWeight (float weight) {
		this.weightCapacity = weightCapacity;
		itemsCapacity = (int) weightCapacity;
	}
}
