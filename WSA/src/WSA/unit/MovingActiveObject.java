package WSA.unit;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.Game;
import WSA.map.MapController;
import WSA.map.MapGroup;

public abstract class MovingActiveObject extends ActiveObject {

	public MovingActiveObject(ObjectController objectController, float height, float radius, Vector3f position) {
		super(objectController, height, radius, position);
	}

	protected Vector2f speed; // скорость в метрах в секунду
	protected Vector2f maxSpeed;
	protected Vector2f acceleration = null;
	protected Vector2f maxRotationSpeed;

	protected boolean isPlaceRotatable = true;
	protected boolean isAbleForSideMove = false;

	protected float maxYAngle;
	protected float minYAngle;

	public void moveChangePosition(float timeProvided) {
		position.x += getHorizontalSpeed().x * Game.getDeltaInSeconds();
		position.z += getHorizontalSpeed().y * Game.getDeltaInSeconds();
	}
	
	public void onPositionCoordinatesChange() {
		
		MapGroup gr;

		if (map.ifGroupExists(position.x, position.x) == false) {
			map.generate(position.x, position.z, viewDistance);
		}

		gr = map.getGroup(position.x, position.z);

		//Game.print("GROUP COMPARATION " + (gr == parentGroup));

		if (gr != parentGroup) {
			if (gr == null) {
				Game.printLongTimed("MovableActiveObject on changePos parent group error");
			}
			onGroupChange(parentGroup, gr);
			parentGroup = gr;
		}

		onPositionChange();
	}
	
	private void onGroupChange(MapGroup Old, MapGroup New) {
		map.makeInvisible(Old.getStartX(), Old.getStartZ(), viewDistance);
		map.makeVisible(New.getStartX(), New.getStartZ(), viewDistance);
	}

	private void onPositionChange() {
		//Game.print("position (x,z) = (" + position.x + ", " + position.z + "); rotation.x = " + rotation.x + "; speed.x = " + speed.x);
	}
	
	
//	for (float ang = -720.0f; ang <= 720.0f; ang++) {
//		Game.printConsole("ang = " + ang + " correctedAng = " + MovingActiveObject.correctAngle(ang, true));
//	}
	
	public static float correctAngle(float angle, boolean isMinus) {
		if (angle >= 360.0f) {
			angle -= ((int) angle) / 360 * 360;
		}
		if (angle < 0.0f) {
			angle = ((int) Math.abs(angle)) / 360 * 360 - ((int) Math.abs(angle)) + (isMinus ? 0 : 360.0f);// / 360 * 360 + 0.0f;
		}
		return angle;
	}

	public Vector2f getHorizontalSpeed() {
		return new Vector2f((float) (speed.x * Math.cos(rotation.x / 180.0f * Math.PI)), (float) (speed.x * Math.sin(rotation.x / 180.0f * Math.PI)));
	}

	public Vector2f getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(Vector2f acceleration) {
		this.acceleration = acceleration;
	}

	public Vector2f getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(Vector2f maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public Vector2f getSpeed() {
		return speed;
	}

	public void setSpeed(Vector2f speed) {
		this.speed = speed;
	}
	
	public Vector2f getMaxRotationSpeed() {
		return maxRotationSpeed;
	}

	public void setMaxRotationSpeed(Vector2f maxRotationSpeed) {
		this.maxRotationSpeed = maxRotationSpeed;
	}

	public float getMaxYAngle() {
		return maxYAngle;
	}

	public void setMaxYAngle(float maxYAngle) {
		this.maxYAngle = maxYAngle;
	}

	public float getMinYAngle() {
		return minYAngle;
	}

	public void setMinYAngle(float minYAngle) {
		this.minYAngle = minYAngle;
	}
	
	
}
