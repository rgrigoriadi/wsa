package WSA.unit;

public interface AtackableObject {
	public void onBeingAtacked(float killProbability);
}