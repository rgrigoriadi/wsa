package WSA.unit;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.Game;
import WSA.graphics.Model;
import WSA.graphics.ModelController;
import WSA.map.MapController;
import WSA.map.MapGroup;

public abstract class MapObject {
	protected Vector3f position;
	protected Vector2f rotation; // х - по часовой стрелке, если смотреть сверху

	protected float objectHeight;
	protected float objectRadius;
	
	protected int in; // идентификационный номер в системе
	
	protected ObjectController objectController;
	
	//Formal var
	protected static MapController map;
	protected MapGroup parentGroup = null;
	
	public MapObject (ObjectController objectController, float height, float radius, Vector3f position) {
		
		this.objectController = objectController;
		this.objectHeight = height;
		this.objectRadius = radius;
		this.position = position;
		this.rotation = new Vector2f(0.0f, 0.0f);
	}
	
	public abstract void refresh(CollisionsController controller);
	
	public abstract void onCreateOnMap();

	public void onDestroy() {
		objectController.removeObject(this);
	}
	
	public boolean isOnMapAndActual () {
		if (objectController == null || !objectController.contains(this)) {
			return false;
		} else {
			return true;
		}
	}

	public abstract String getNameString();
	
	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}
	
	public Vector2f getRotation() {
		return rotation;
	}

	public void setRotation(Vector2f rotation) {
		this.rotation = rotation;
	}

	public float getObjectRadius() {
		return objectRadius;
	}

	public void setObjRadius(float unitRadius) {
		this.objectRadius = unitRadius;
	}

	public float getObjectHeight() {
		return objectHeight;
	}

	public void setObjectHeight(float unitHeight) {
		this.objectHeight = unitHeight;
	}

	public int getId () {
		return in;
	}
	
	public boolean isAlive () {
		if (AliveObject.class.isInstance(this)) {
			return true;
		} else {
			return false;
		}
	}
	
	public abstract boolean isSolid();

	public int getPlayerId() {
		return -1;
	}
	
	public Model getStandartModel (ModelController modelController) {
		return modelController.getModelForObject(this);
	}
}
