package WSA.unit.objects;

import org.lwjgl.util.vector.Vector2f;

import WSA.CollisionsController;
import WSA.Game;
import WSA.unit.AliveObject;
import WSA.unit.MapObject;
import WSA.unit.ObjectController;


public class Corpse extends MapObject {

	AliveObject whoseCorpse;
	
	float deathTime;
	
	public Corpse(AliveObject whoseCorpse, ObjectController objectController) {
		super(objectController, whoseCorpse.getObjectHeight(), whoseCorpse.getObjectRadius(), whoseCorpse.getPosition());
		this.whoseCorpse = whoseCorpse;
		this.deathTime = Game.getGameTime();
		this.rotation = new Vector2f(0.0f, 0.0f);
	}

	@Override
	public void refresh(CollisionsController controller) {
		// TODO Разлагаемся
	}

	@Override
	public void onCreateOnMap() {

	}

	@Override
	public String getNameString() {
		return "Corse of " + whoseCorpse.getNameString();
	}

	@Override
	public boolean isSolid() {
		return false;
	}
	
	public AliveObject getAliveObject() {
		return whoseCorpse;
	}
}
