package WSA.unit.objects;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.Faith;
import WSA.Game;
import WSA.map.MapController;
import WSA.unit.AliveObject;
import WSA.unit.ObjectController;
import WSA.unit.WeaponHandlingObject;
import WSA.unit.action.ActionStack;
import WSA.unit.inventory.items.AtackItem;
import WSA.unit.inventory.items.AtackItemInterface;

public class Unit extends AliveObject implements WeaponHandlingObject {

	// HP, perks etc.

	protected float strength = 0;
	protected float agility = 0;
	protected float intelligence = 0;
	protected short faith = 100;

	protected AtackItem weaponInHands;
	 
	public Unit(int in, Vector3f position, Vector2f rotation, boolean isGod, MapController map, ObjectController objController, int playerId) {
		super(objController, playerId, 1.8f, 0.5f, position);
		Unit.map = map;

		this.acceleration = new Vector2f(10.0f, -9.8f);
		this.maxSpeed = new Vector2f(3.0f, 60.0f);
		this.maxRotationSpeed = new Vector2f(270.0f, 360.0f); //new Vector2f(270.0f, 360.0f);

		maxYAngle = 90.0f;
		minYAngle = -90.0f;

		this.in = in;
		viewDistance = 600;

		this.rotation = rotation;

		this.speed = new Vector2f(0.0f, 0.0f);

		map.makeVisible(position.x, position.z, viewDistance);
		parentGroup = map.getGroup(position.x, position.z);

		this.isGod = isGod;
		birthDay = Game.getGameTime();
		isMale = Math.random() < MALE_COEFFICIENT ? true : false;

		actionStack = new ActionStack();
	}

	@Override
	public void refresh(CollisionsController col) {
		super.refresh(col);
		faith = (short) (((int) Game.getGameTime() / 100) % Faith.MAX);
	};

	public float getStrength() {
		return strength;
	}

	public void setStrength(float strength) {
		this.strength = strength;
	}

	public float getAgility() {
		return agility;
	}

	public void setAgility(float agility) {
		this.agility = agility;
	}

	public float getIntelligence() {
		return intelligence;
	}

	public void setIntelligence(float intelligence) {
		this.intelligence = intelligence;
	}

	public short getFaith() {
		return faith;
	}

	public void setFaith(short faith) {
		this.faith = faith;
	}


	@Override
	public float getHitCompleteTime() {
		return 1.0f;
	}


	@Override
	public float getHitPrepareTime() {
		return 0.7f;
	}


	@Override
	public float getHitKillProbability() {
		return 0.3f;
	}


	@Override
	public float getHitDistance() {
		return 1.0f;
	}


	@Override
	public void takeWeaponInHands(DroppedItemObject weapon) {
		if (!AtackItem.class.isInstance(weapon.item)) {
			return;
		}
		
		if (weaponInHands != null) {
			dropWeapon();
		}
		
		weaponInHands = (AtackItem) weapon.item;
		
		objectController.removeObject(weapon);
		// TODO уничтожить объект
	}

	@Override
	public AtackItem getWeaponInHands() {
		return weaponInHands;
	}


	@Override
	public void dropWeapon() {
		// TODO создать объект
		objectController.createObject(new DroppedItemObject(weaponInHands, objectController, this.position));
		weaponInHands = null;
	}


	@Override
	public String getNameString() {
		return " Unit";
	}


	@Override
	public boolean isSolid() {
		return true;
	}
}
