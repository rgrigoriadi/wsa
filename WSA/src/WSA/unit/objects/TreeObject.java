package WSA.unit.objects;

import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.unit.MapObject;
import WSA.unit.ObjectController;
import WSA.unit.PlantObject;

public class TreeObject extends PlantObject {

	public TreeObject(ObjectController objectController, Vector3f position) {
		super(objectController, 8.0f, 0.5f, position);

	
	}

	@Override
	public void refresh(CollisionsController controller) {
		// растем
	}

	@Override
	public void onCreateOnMap() {
		
	}

	@Override
	public String getNameString() {
		return "tree";
	}

	@Override
	public boolean isSolid() {
		return true;
	}
	
	@Override
	public float getAppearanceProbability(float richness, float distanceToNextPlant) {
		float MIN_DISTANCE_TO_ANOTHER = 8.0f;//getObjectRadius();
		
		if (distanceToNextPlant <= MIN_DISTANCE_TO_ANOTHER || richness <= 0.0f) {
			return 0.0f;
		} else {
			
			float distanceFactor = (distanceToNextPlant - MIN_DISTANCE_TO_ANOTHER) / MIN_DISTANCE_TO_ANOTHER;
			if (distanceFactor > 1.0f) {
				distanceFactor = 1.0f;
			}
			
			return (float) (Math.random() * richness * distanceFactor);
		}
	}
}
