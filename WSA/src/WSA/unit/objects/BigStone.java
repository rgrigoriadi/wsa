package WSA.unit.objects;

import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.unit.MapObject;
import WSA.unit.ObjectController;

public class BigStone extends MapObject{

	public BigStone(ObjectController objectController, Vector3f position) {
		super(objectController, 3.0f, 2.0f, position);
	}

	@Override
	public void refresh(CollisionsController controller) {
		
	}

	@Override
	public void onCreateOnMap() {
		
	}

	@Override
	public String getNameString() {
		return "big stone";
	}

	@Override
	public boolean isSolid() {
		return true;
	}

}
