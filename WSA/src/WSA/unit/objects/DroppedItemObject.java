package WSA.unit.objects;

import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.unit.MapObject;
import WSA.unit.ObjectController;
import WSA.unit.inventory.items.Item;

public class DroppedItemObject extends MapObject {

	Item item;
	
	public DroppedItemObject(Item item, ObjectController objectController, Vector3f position) {
		super(objectController, 0.2f, 1.0f, position);
		this.item = item;
	}

	@Override
	public void refresh(CollisionsController controller) {

	}

	@Override
	public void onCreateOnMap() {

	}

	@Override
	public String getNameString() {
		return item.getName() + " lying on the ground";
	}

	@Override
	public boolean isSolid() {
		return false;
	}

}
