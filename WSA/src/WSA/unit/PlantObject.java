package WSA.unit;

import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;

public abstract class PlantObject extends MapObject implements PlantInterface {

	public PlantObject(ObjectController objectController, float height, float radius, Vector3f position) {
		super(objectController, height, radius, position);
	}
}
