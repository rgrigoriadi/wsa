package WSA.unit;

import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.Game;
import WSA.ai.AI;
import WSA.unit.action.Action;
import WSA.unit.inventory.Inventory;
import WSA.unit.objects.Corpse;

public abstract class AliveObject extends MovingActiveObject implements AtackingObject, AtackableObject, InventoryHandlingObject {

	public static final float MALE_COEFFICIENT = 0.7f;

	protected float hitStartTime = AtackingObject.DIDNT_HIT;
	protected boolean didHitDuringAtack = false;

	protected float birthDay; // в секундах, возраст берется из "getAge()"
	protected boolean isMale;
	protected boolean isGod = false;

	protected int playerLordId;

	protected Inventory inventory;

	AI ai;

	public AliveObject(ObjectController objectController, int playerId, float height, float radius, Vector3f position) {
		super(objectController, height, radius, position);
		playerLordId = playerId;
		inventory = new Inventory(15.0f);
	}

	public void setAI(AI ai) {
		this.ai = ai;
	}

	public String getAIName() {
		if (isAIEnabled()) {
			return ai.getAIName();
		} else {
			return "no ai";
		}

	}

	public boolean isAIEnabled() {
		if (ai != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onCreateOnMap() {

	}

	@Override
	public void refresh(CollisionsController col) {

		if (isAIEnabled()) {
			ai.refresh();
		}

		if (!isOnMapAndActual()) {
			Game.print("----- Refreshing dead Object");
		}

		float timeProvidedMove = Game.getDeltaInSeconds();

		if (col.getMapHeight(position.x, position.z) > position.y || col.getMapHeight(position.x, position.z) < position.y) {
			position.y = col.getMapHeight(position.x, position.z);
			speed.y = 0.0f;
		} else {

		}

		//Game.print("Unit position (" + position.x + ", " + position.y + ", " + position.z + "), rotation (" + rotation.x + ", " + rotation.y + ")");

		while (timeProvidedMove > 0.0f && !actionStack.isEmpty()) {
			Action action = actionStack.getNextAction();

			float timeSpent = action.act(this, timeProvidedMove);
			timeProvidedMove -= timeSpent;
			
//			Game.printConsole("Cycle step action " + action.getStatusString(this) + " provided time = " + timeProvidedMove + " timeSpent = " + timeSpent);
		}
		
//		System.out.println("Cycle end");
	}

	public float getAge() { // возраст юнита в секундах
		return (float) (Game.getGameTime() - birthDay) / 1000.0f;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		objectController.createObject(new Corpse(this, objectController));
	}

	@Override
	public void onBeingAtacked(float killProbability) {
		if (Math.random() < killProbability) {
			onDestroy();
		}
	}

	@Override
	public int getPlayerId() {
		return playerLordId;
	}

	@Override
	public float getHitStartTime() {
		return hitStartTime;
	}

	@Override
	public void onHitStart() {
		hitStartTime = Game.getGameTime();
		didHitDuringAtack = false;
	}

	@Override
	public void onHit() {
		didHitDuringAtack = true;
	}

	@Override
	public void onHitEnd() {
		didHitDuringAtack = false;
		hitStartTime = AtackingObject.DIDNT_HIT;
	}

	@Override
	public boolean didHitDuringAtack() {
		return didHitDuringAtack;
	}

	@Override
	public float getHitPostpareTime() {
		return getHitCompleteTime() - getHitPrepareTime();
	}

	public boolean isGod() {
		return isGod;
	}

	public boolean isMale() {
		return isMale;
	}

	@Override
	public Inventory getInventory() {
		return inventory;
	}

	@Override
	public boolean isSolid() {
		return true;
	}
}
