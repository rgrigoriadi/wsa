package WSA.unit;

public interface PlantInterface {
	public float getAppearanceProbability(float richness, float distanceToNextPlant);
	
}
