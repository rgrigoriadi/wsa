package WSA.unit;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector3f;

import WSA.unit.action.Action;
import WSA.unit.action.ActionStack;

public abstract class ActiveObject extends MapObject {
	
	public ActiveObject(ObjectController objectController, float height, float radius, Vector3f position) {
		super(objectController, height, radius, position);
	}

	protected float viewDistance;
	protected ActionStack actionStack;

	public void clearActionStack() {
		actionStack.clearStack();
	}
	
	public void addActionFirst(Action action) {
		actionStack.addActionFirst(action);
	}
	
	public void addAction(Action action) {
		actionStack.addAction(action);
	}
	
	public String getCurrentActionString() {
		Action action = actionStack.getNextAction();
		if (action != null) {
			return action.getStatusString(this);
		} else {
			return "Does Nothing";
		}
	}
	
	public ArrayList<ActionStack.actionChainElement> getActionChain () {
		if (actionStack.getActionChain() != null && actionStack.getActionChain().size() > 0) {
			return actionStack.getActionChain();
		} else {
			return null;
		}
	}
	
	public boolean isBusy() {
		if (actionStack.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
}
