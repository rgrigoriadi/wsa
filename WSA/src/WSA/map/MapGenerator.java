package WSA.map;

import java.util.Random;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.Game;
import WSA.unit.ObjectController;
import WSA.unit.objects.TreeObject;

public class MapGenerator {

	public static final float CORNER_DEVIATION_PLAIN = 8.0f;
	public static final float CORNER_DEVIATION_HILLS = 16.0f;
	public static final float CORNER_DEVIATION_MOUNT = 32.0f;

	public static final float RICHNESS_NORMAL_HEIGHT = 50.0f;
	public static final float EARTH_NORMAL_ANGLE = 12.0f;

	public static final float STONE_MIN_ANGLE = 45.0f;

	public static final float HEIGHT_NON_GENERATED = MapGroup.HEIGHT_NON_GENERATED;

	public static final int MIN_DENSITY = MapController.MIN_DENSITY;

	MapController map;
	ObjectController objectController;
	CollisionsController collisionsController;

	public MapGenerator(MapController map, ObjectController objectController, CollisionsController collisionsController) {
		this.collisionsController = collisionsController;
		this.objectController = objectController;
		this.map = map;
	}

	public void generate(MapGroup gr, float cornerDeviation) { // на генерацию должны подаваться чистые блоки, без тоннелей

		// TODO возможна функция генерации высот, их средних, а остальное - сглаживание, нужно сюда добавить сглаживание вершин не кратных minDensity

		// TODO возможно, разнести генерацию на разные моменты времени

		//		cornerDeviation = CORNER_DEVIATION_MOUNT;
		float sideDeviation = (float) Math.sqrt(cornerDeviation);
		float centerDeviation = sideDeviation;

		int size = gr.getHeights().length;
		int squareSize = size - 1;

		sideDeviation /= (float) squareSize;
		centerDeviation /= (float) squareSize;

		for (; squareSize >= 2; squareSize /= 2) {
			for (int i = 0; i <= MapGroup.GROUP_STEP - squareSize; i += squareSize) {
				for (int j = 0; j <= MapGroup.GROUP_STEP - squareSize; j += squareSize) {

					int numberOfKnownVertexesOnCorners = 0;
					int sumOfKnowsVertexesOnCorners = 0;

					for (int ii = i; ii <= i + squareSize; ii += squareSize) {
						for (int jj = i; jj <= j + squareSize; jj += squareSize) {
							if (gr.getHeights()[ii][jj] == MapGroup.HEIGHT_NON_GENERATED) {
								sumOfKnowsVertexesOnCorners += 0;
								numberOfKnownVertexesOnCorners += 0;
							} else {
								sumOfKnowsVertexesOnCorners += gr.getHeights()[ii][jj];
								numberOfKnownVertexesOnCorners += 1;
							}
						}
					}

					if (numberOfKnownVertexesOnCorners == 0) {
						numberOfKnownVertexesOnCorners = 1;
					}

					float cornerMeanValue = sumOfKnowsVertexesOnCorners / (float) numberOfKnownVertexesOnCorners;

					for (int ii = i; ii <= i + squareSize; ii += squareSize) {
						for (int jj = i; jj <= j + squareSize; jj += squareSize) {

							if (gr.getHeights()[ii][jj] == MapGroup.HEIGHT_NON_GENERATED) {
								//								if (squareSize == MapGroup.GROUP_STEP) { // вызывается только на самом крупном степе
								gr.getHeights()[ii][jj] = getNormalRandom(cornerMeanValue, cornerDeviation, 0);
								//								} else {
								//									array[ii][jj] = cornerMeanValue;
								//								}
							}
						}
					}

					// строим точки на гранях

					float sumOfKnownSideVertexes = 0;
					float numberOfKnownSideVertexes = 4;

					for (int ii = 0; ii <= squareSize; ii += squareSize) {
						if (gr.getHeights()[i + ii][j + squareSize / 2] == MapGroup.HEIGHT_NON_GENERATED) {
							gr.getHeights()[i + ii][j + squareSize / 2] = getNormalRandom((gr.getHeights()[i + ii][j + 0] + gr.getHeights()[i + ii][j + squareSize]) / 2.0f, sideDeviation, squareSize);
						}

						if (gr.getHeights()[i + squareSize / 2][j + ii] == MapGroup.HEIGHT_NON_GENERATED) {
							gr.getHeights()[i + squareSize / 2][j + ii] = getNormalRandom((gr.getHeights()[i + 0][j + ii] + gr.getHeights()[i + squareSize][j + ii]) / 2.0f, sideDeviation, squareSize);
						}
						sumOfKnownSideVertexes += gr.getHeights()[i + ii][j + squareSize / 2];
						sumOfKnownSideVertexes += gr.getHeights()[i + squareSize / 2][j + ii];
						//Game.print("s[" + i + "][" + step / 2 + "] = " + s2 + " array = " + array[i][step / 2] + " isInf = " + Float.isInfinite(array[i][step / 2])); 
					}

					//Game.print(" s1, s2 " + s1 + ", " + s2);

					float sideMeanValue = sumOfKnownSideVertexes / numberOfKnownSideVertexes;
					// строим центральную

					if (gr.getHeights()[i + squareSize / 2][j + squareSize / 2] == MapGroup.HEIGHT_NON_GENERATED) {
						gr.getHeights()[i + squareSize / 2][j + squareSize / 2] = getNormalRandom(sideMeanValue, centerDeviation, squareSize);
					}
				}
			}
		}

		for (int i = 0; i < MapGroup.GROUP_STEP; i++) {
			for (int j = 0; j < MapGroup.GROUP_STEP; j++) {

				float ang = gr.getAngleOfLandscape(new Vector2f(i, j));

				float h = gr.getHeight(i, j);
				float richnessHeightMultiplier = (h < 1.0f ? 1.0f : (RICHNESS_NORMAL_HEIGHT * h) / RICHNESS_NORMAL_HEIGHT);
				float richnessAngleMultiplier = EARTH_NORMAL_ANGLE / ang;

				gr.getRichness()[i][j] = richnessHeightMultiplier * richnessAngleMultiplier;

				if (gr.getRichness(i, j) < 0.2f) {
					if (ang < STONE_MIN_ANGLE) {
						gr.getMaterials()[i][j] = MapMaterial.SAND;
					} else {
						gr.getMaterials()[i][j] = MapMaterial.STONE;
					}
				} else if (gr.getRichness(i, j) < 0.3f) {
					gr.getMaterials()[i][j] = MapMaterial.DIRT;
				} else {
					gr.getMaterials()[i][j] = MapMaterial.GRASS;
				}

			}
		}

		putSomeObjects(gr);
	}

	public void putSomeObjects(MapGroup gr) {

		for (int x = 0; x < MapGroup.GROUP_STEP; x++) {
			for (int z = 0; z < MapGroup.GROUP_STEP; z++) {

				if (x % 8 == 0 && z % 8 == 0) {

					float xpos = gr.getStartX() + x;
					float zpos = gr.getStartZ() + z;

					if (gr.getRichness(x, z) > 0.8f && Math.random() < 0.1f) {
						objectController.createObject(new TreeObject(objectController, new Vector3f(xpos, gr.getHeight(x, z), zpos)));	
					}
					
				}
			}
		}

		//		Game.print("map elements number = " + gr.map.getMapSize());
		//		Game.print("gr.h = " + gr.getHeight(MapGroup.GROUP_STEP / 2, MapGroup.GROUP_STEP / 2) + " map.gh = " + gr.map.getHeight(xpos, zpos));
		//		Game.print("gr.x = " + xpos + " centerPointx = " + gr.getCenterPoint().x);
		//		Game.print("gr.z = " + zpos + " centerPointz = " + gr.getCenterPoint().z);

	}

	//	public static float filter()

	//	public static float doloniseHeight (float height) {
	//		return
	//	}

	public static void normaliseDolonise(float array[][]) {
		float min = 999999999;
		float max = -999999999;
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				if (array[i][j] > max) {
					max = array[i][j];
				} else if (array[i][j] < min) {
					min = array[i][j];
				}
			}
		}

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = (array[i][j] - min) / (max - min);
			}
		}

		dolonise(array);
		dolonise(array);

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = array[i][j] * (max - min) + min;
			}
		}
	}

	public static void dolonise(float array[][]) {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = (array[i][j] < 0 ? -1 : 1) * (float) Math.sqrt(Math.abs(array[i][j]));

			}
		}
	}

	public static float getNormalRandom(float m, float deviation, int distanceCoefficient) {

		Random random = new Random();

		float number = (float) random.nextGaussian();

		if (distanceCoefficient == 0) {
			distanceCoefficient = 1;
		}

		return (float) number * deviation * distanceCoefficient + m;
	}
}
