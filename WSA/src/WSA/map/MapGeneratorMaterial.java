package WSA.map;

public class MapGeneratorMaterial {
	public static byte getMaterial(float height) {
		byte ret = 0;
		float h = height;
		if (h < 0.0f) {
			ret = MapMaterial.SAND;
		} else if (h < 20.0f) {
			ret = MapMaterial.DIRT;
		} else if (h < 40.f) {
			ret = MapMaterial.GRASS;
		} else {
			ret = MapMaterial.STONE;
		}
		return ret;
	}
}
