/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package WSA.map;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;

import WSA.Game;
import WSA.unit.ObjectController;

/**
 * 
 * @author Roman
 */
public class MapController {

	public ArrayList<MapGroup> map;

	private MapGenerator generator;
	
	public static final int GROUPSIZE = MapGroup.GROUPSIZE;
	public static final int GROUP_STEP = MapGroup.GROUP_STEP;
	public static final int MIN_DENSITY = MapGroup.MIN_DENSITY;

	public MapController() {
		map = new ArrayList<MapGroup>();
	}

	public void setMapGenerator (MapGenerator generator) {
		this.generator = generator;
	}
	
	public MapGenerator getMapGenerator() {
		return generator;
	}
	
	public boolean generate(float x, float z, float radius) {

		int rx = getGroupStartCoordinate(x);
		int rz = getGroupStartCoordinate(z);

		int alt = ((int) radius / GROUP_STEP) * GROUP_STEP;

		for (int i = rx - alt; i <= rx + alt; i += GROUP_STEP) {
			for (int j = rz - alt; j <= rz + alt; j += GROUP_STEP) {
				if (Math.sqrt((i - rx) * (i - rx) + (j - rz) * (j - rz)) < radius) {
					if (!ifGroupExists(i, j)) {
						generateGroup(rx, rz);
					} else {
						continue;
					}
				} else {
					//	continue;
				}
			}
		}
		return true;
	}

	public boolean generateGroup(float x, float z) {

		int rx = getGroupStartCoordinate(x);
		int rz = getGroupStartCoordinate(z);

		if (!ifGroupExists(rx, rz)) {
			map.add(new MapGroup(rx, rz, this));
		}
		
		return true;
	}

	public boolean makeVisible(float x, float z, float radius) {

		int rx = getGroupStartCoordinate(x);
		int rz = getGroupStartCoordinate(z);

		int alt = ((int) radius / GROUP_STEP) * GROUP_STEP;

		for (int i = rx - alt; i <= rx + alt; i += GROUP_STEP) {
			for (int j = rz - alt; j <= rz + alt; j += GROUP_STEP) {
				if (Math.sqrt((i - rx) * (i - rx) + (j - rz) * (j - rz)) < radius) {
					if (!ifGroupExists(i, j)) {
						generateGroup(i, j);
					} else {
						getGroup(i, j).increaseVisibility();
					}

				} else {
					//	continue;
				}
			}
		}
		return true;
	}

	public boolean makeInvisible(float x, float z, float radius) {

		int rx = getGroupStartCoordinate(x);
		int rz = getGroupStartCoordinate(z);

		int alt = ((int) radius / GROUP_STEP) * GROUP_STEP;

		for (int i = rx - alt; i <= rx + alt; i += GROUP_STEP) {
			for (int j = rz - alt; j <= rz + alt; j += GROUP_STEP) {
				if (Math.sqrt((i - rx) * (i - rx) + (j - rz) * (j - rz)) < radius) {
					if (ifGroupExists(i, j)) {
						getGroup(i, j).decreaseVisibility();
					}

				} else {
					//	continue;
				}
			}
		}
		return true;
	}

	public boolean ifGroupExists(float x, float z) {
		if (getGroup(x, z) != null) {
			return true;
		} else {
			return false;
		}
	}

	public MapGroup getGroup(int index) {
		return map.get(index);
	}

	public MapGroup getGroup(Vector2f coordinate) {
		return getGroup(coordinate.x, coordinate.y);
	}

	public MapGroup getGroup(float x, float z) {
		int groupStartX = getGroupStartCoordinate(x);
		int groupStartZ = getGroupStartCoordinate(z);

		//		if (x % GROUP_STEP == 0.0f) {
		//			stx = (int) x;
		//		} else {
		//			if (x >= 0) {
		//				stx = ((((int) x) / (GROUP_STEP)) * (GROUP_STEP));
		//			} else {
		//				stx = (((int) (x / GROUP_STEP)) - 1) * GROUP_STEP;
		//			}
		//		}
		//
		//		if (z % GROUP_STEP == 0.0f) {
		//			stz = (int) z;
		//		} else {
		//			if (z >= 0) {
		//				stz = ((((int) z) / (GROUP_STEP)) * (GROUP_STEP));
		//			} else {
		//				stz = (((int) (z / GROUP_STEP)) - 1/* ((z / GROUP_STEP == 0.0f) ? 0 : 1) */) * GROUP_STEP;
		//			}
		//		}

//		Game.print("GetGroup " + x + ", " + z + " stx stz = " + groupStartX + " " + groupStartZ);

		for (int i = 0; i < map.size(); i++) {
			MapGroup gr = map.get(i);
			// можно попробовать перейти по дереву от первой к нужной
			//Game.print(stx + " " + stz + " gr starts = " + gr.startX + " " + gr.startZ);
			if (groupStartX == gr.getStartX() && groupStartZ == gr.getStartZ()) {
				return gr;
			}
		}

		//Game.print("GetGroupFail, x, z = " + x + " , " + z + " stx, stz : " + stx + " , " + stz);

		return null;
	}

	public float getHeight(float argX, float argZ) {
		int x[] = new int[2];
		int z[] = new int[2];

		MapGroup gr = getGroup(argX, argZ);

		if (gr == null) {
//			Game.printLongTimed("Not found group at (" + argX + ", " + argZ + ")");
			return 0;
		}
			

		//x[0] = (int) (argX % GROUP_STEP) - (x[0] < 0 ? gr.getDensity() : 0);
		//z[0] = (int) (argZ % GROUP_STEP) - (z[0] < 0 ? gr.getDensity() : 0);

		//Game.print("" + argX + " -> " + x[0] + " | " + argZ + " -> " + z[0]);

		x[0] = ((int) (argX - gr.getStartX())) / gr.getDensity() * gr.getDensity();
		z[0] = ((int) (argZ - gr.getStartZ())) / gr.getDensity() * gr.getDensity();

		x[1] = x[0] + gr.getDensity();
		z[1] = z[0] + gr.getDensity();

		float dx = argX % gr.getDensity();
		float dz = argZ % gr.getDensity();

		if (argX < 0) {
			dx = gr.getDensity() + dx;
		}

		if (argZ < 0) {
			dz = gr.getDensity() + dz;
		}

		//Game.print("" + gr.getStartX() + " " + gr.getStartZ() + " || " + argX + " -> " + x[0] + " | " + argZ + " -> " + z[0] + "   ||||||   (dx, dz) = (" + dx + ", " + dz + ")"/* + " GetGroup " + argX + ", " + argZ + " stx stz = " */);

		float h[] = new float[4];

		h[0] = gr.getHeight(x[0], z[0]);
		h[1] = gr.getHeight(x[0], z[1]);
		h[2] = gr.getHeight(x[1], z[0]);
		h[3] = gr.getHeight(x[1], z[1]);

		if (Float.isNaN(h[0]) || Float.isNaN(h[1]) || Float.isNaN(h[2]) || Float.isNaN(h[3])) {
			return Float.NaN;
		}

		int triagN = 0;

		if (dz + dx < gr.getDensity()) {
			triagN = 0;
		} else {
			triagN = 1;
		}

		float dh[] = new float[2];
		dh[0] = 0.0f;
		dh[1] = 0.0f;

		float x0, x1, x2, y0, y1, y2, z0, z1, z2, xx, zz;

		x1 = 0;
		y1 = h[1];
		z1 = gr.getDensity();

		x2 = gr.getDensity();
		y2 = h[2];
		z2 = 0;

		xx = dx;
		zz = dz;

		if (triagN == 0) {
			x0 = 0;
			z0 = 0;
			y0 = h[0];
		} else {
			x0 = gr.getDensity();
			z0 = gr.getDensity();
			y0 = h[3];
		}

		// TODO преобразовать выражения для частных случаев треугольников

		dh[1] = ((xx - x0) * ((y1 - y0) * (z2 - z0) - (y2 - y0) * (z1 - z0)) + (zz - z0) * ((x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0))) / ((x1 - x0) * (z2 - z0) - (x2 - x0) * (z1 - z0)) + y0;
		//Game.print("GetHeight = " + dh[1] + " dh0 = " + dh[0] + " x = " + argX + " z = " + argZ + " dx, dz = " + dx + " " + dz + " h1 - h4 " + h[0] + ", " + h[1] + ", " + h[2] + ", " + h[3] + " density = " + gr.getDensity());
		return dh[1];
	}

	public int getMapSize() {
		return map.size();
	}
	
	public void onStop() {
		for (int i = 0; i < map.size(); i++) {
			MapGroup group = map.get(i);
			// DO SMHT
		}
	}

	public static int getGroupStartCoordinate(float x) { // получить из координат на карте координаты кластера, которому они принадлежат
		int ret = (int) (x - (x % GROUP_STEP) - ((x < 0) && (x % GROUP_STEP != 0.0f) ? GROUP_STEP : 0));
		//Game.print("ggsc x = " + x + " res = " + ret);
		return ret;
	}
}