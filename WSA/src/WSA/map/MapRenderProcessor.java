package WSA.map;

import java.util.ArrayList;

import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import WSA.RectangleFloat;
import WSA.graphics.VBO;
import WSA.player.ViewController;

public class MapRenderProcessor {

	ArrayList<Link> vboMapHD; // TODO перевести это в режим МАП

	VBO vboMapLD;
	ArrayList<MapGroup> lowDensityGroups; // TODO лоуДЕНСИТИ МАП

	//int minHighDensity = 64;

	public void init() {

		vboMapHD = new ArrayList<MapRenderProcessor.Link>();

	}

	public ArrayList<VBO> getVBOs(MapController map, ViewController view) {
		refresh(map, view);
		ArrayList<VBO> ret = new ArrayList<VBO>();
		for (int i = 0; i < vboMapHD.size(); i++) {
			ret.add(vboMapHD.get(i).vbo);
		}
		//ret.add(vboMapLD); // TODO
		return ret;
	}

	public void refresh(MapController map, ViewController view) {
		for (int i = 0; i < map.getMapSize(); i++) {
			
			MapGroup gr = map.getGroup(i);
			
			Vector3f center = gr.getCenterPoint();
			float distance = (float) Math.sqrt((view.getPosition().x - center.x) * (view.getPosition().x - center.x) + (view.getPosition().y - center.y) * (view.getPosition().y - center.y) + (view.getPosition().z - center.z) * (view.getPosition().z - center.z));
			int density = getDensity(distance);
			
			if (isGroupLoaded(gr)) { // TODO нет варианта удаления вбо из буффера, только перезагрузка и догрузка
				if (density != getLink(gr).getDensity()) {
					reload(gr, density);
				} else {
					// OK
				}
			} else {
				load(gr, density);
			}
		}
	}

	public void reload(MapGroup gr, int density) {
		unLoad(gr);
		load(gr, density);
	}

	public void unLoad(MapGroup gr) {
		Link lnk = getLink(gr); 
		unloadVBO(lnk.vbo);
		vboMapHD.remove(lnk);
	}

	public void unloadVBO(VBO vbo) {
		ARBVertexBufferObject.glDeleteBuffersARB(vbo.vboId);
		ARBVertexBufferObject.glDeleteBuffersARB(vbo.iboId);
		vbo = null;		
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, 0);
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
	}

	public void load(MapGroup gr, int density) {
		
		// TODO баг дыр в границах, решаемо		

		// TODO можно реализовать smothDensity!, так как мы все равно обновляем группы и можем рендерить их вне зависимости от их реальной плотности
		
		Link lnk;

		if (isGroupLoaded(gr) == false) {
			lnk = new Link();
			lnk.group = gr;
			lnk.vbo = new VBO();
			vboMapHD.add(lnk);
		} else {
			lnk = getLink(gr);
		}

		gr.setDensity(density);
		if (density != gr.getDensity()) {
//			Game.print("dens != (density = " + density + ") (gr.dens = " + gr.getDensity() + ")");
			// старый исправленнй баг функции сетДенсити
			density = gr.getDensity();
		}
		
		
		
		int vao_stride = 3 + 3 + 4 + 2; // на вершину по 3 координаты, 3 координаты нормали и 4 цвета и 2 текстуры, при том, то у нас массив квадратов, но 2 теруголника на кадыйй, 4 вершины на пару треугольников.
		/////////////////v + n + c + t;

		int numberOfSquares = MapGroup.GROUP_STEP / gr.getDensity();
		int vertexPerSquare = MapGroup.VERTEX_PER_SQUARE;
		int vertexPerSquareString = vertexPerSquare * numberOfSquares;
		int indicesPerSquare = MapGroup.INDEX_PER_SQUARE;
		int indicesPerSquareString = indicesPerSquare * numberOfSquares;

		int[] indices = new int[numberOfSquares * indicesPerSquareString];

		float vao[] = new float[numberOfSquares * vertexPerSquareString * vao_stride]; // № на № квадратов по 4 вершины на каждый

		Vector3f [] vertexes = new Vector3f[numberOfSquares * vertexPerSquareString];
		Vector4f [] colors = new Vector4f[numberOfSquares * vertexPerSquareString];
		Vector3f [] normals = new Vector3f[numberOfSquares * vertexPerSquareString];
		Vector2f [] texCoords = new Vector2f[numberOfSquares * vertexPerSquareString];
		
		
		// проходим по квадратом, пишем их подряд - первую строку, потом вторую итд до строни номер n - 1

		for (int j = 0; j < numberOfSquares; j++) { // по z 
			for (int i = 0; i < numberOfSquares; i++) { // по x

				for (int k = 0; k < 4; k++) { // по 4 вершинам квадрата   

					/////////////////////// ||
					int incLine = k / 2; // \/
					int incCol = k % 2; /// -->

					int xIndex = i + incCol;
					int zIndex = j + incLine;

					float h = gr.getHeight(xIndex * density, zIndex * density);
					Vector3f n = gr.getNormal(xIndex, zIndex);
					
					Vector4f c = null;

					if (gr.isVisible() == false) { // для безфильтрового условия это убираем
						c = MapMaterial.getColor(MapMaterial.GRASS); // we must pass any except UNGENERATED
						c.x -= 0.0f;
						c.y -= 0.0f;
						c.z -= 0.0f;
						c.w -= 0.5f;
					} else {
						c = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f); //MapMaterial.getColor(getMaterial(i * density, k * density));// убрали в силу активных текстур
					}

//					vao[0 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = gr.getStartX() + xIndex * density;
//					vao[1 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = h;
//					vao[2 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = gr.getStartZ() + zIndex * density;
//
//					vao[3 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = n.x;
//					vao[4 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = n.y;
//					vao[5 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = n.z;
//
//					vao[6 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = c.x;
//					vao[7 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = c.y;
//					vao[8 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = c.z;
//					vao[9 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = c.w;
//
	
//
//					vao[10 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = rect.x1 + incCol * (rect.x2 - rect.x1); // 0 layer of textures
//					vao[11 + vao_stride * (k + i * vertexPerSquare + j * vertexPerSquareString)] = rect.y1 + incLine * (rect.y2 - rect.y1);

					RectangleFloat rect = MapMaterial.getTexCoord(gr.getMaterial(i * density, j * density));
					
					vertexes[k + i * vertexPerSquare + j * vertexPerSquareString] = new Vector3f(gr.getStartX() + xIndex * density, h, gr.getStartZ() + zIndex * density);
					normals[k + i * vertexPerSquare + j * vertexPerSquareString] = n;
					colors[k + i * vertexPerSquare + j * vertexPerSquareString] = c;
					texCoords[k + i * vertexPerSquare + j * vertexPerSquareString] = new Vector2f(rect.x1 + incCol * (rect.x2 - rect.x1), rect.y1 + incLine * (rect.y2 - rect.y1));
					
				}
			}
		}

		
		
		// проходим по квадратом, пишем их подряд - первую строку, потом вторую итд до строни номер n - 1
		
		int [][] newIndeces = new int [numberOfSquares * numberOfSquares * 2][3]; // 2 треуг на кв
 		
		for (int j = 0; j < numberOfSquares; j++) {
			for (int i = 0; i < numberOfSquares; i++) {

				newIndeces[0 + 2 * (i + j * numberOfSquares)][0] = 1 + i * vertexPerSquare + j * vertexPerSquareString;
				newIndeces[0 + 2 * (i + j * numberOfSquares)][1] = 0 + i * vertexPerSquare + j * vertexPerSquareString;
				newIndeces[0 + 2 * (i + j * numberOfSquares)][2] = 2 + i * vertexPerSquare + j * vertexPerSquareString;
				
				newIndeces[1 + 2 * (i + j * numberOfSquares)][0] = 1 + i * vertexPerSquare + j * vertexPerSquareString;
				newIndeces[1 + 2 * (i + j * numberOfSquares)][1] = 2 + i * vertexPerSquare + j * vertexPerSquareString;
				newIndeces[1 + 2 * (i + j * numberOfSquares)][2] = 3 + i * vertexPerSquare + j * vertexPerSquareString;
//				indices[0 + i * indicesPerSquare + j * indicesPerSquareString] = 1 + i * vertexPerSquare + j * vertexPerSquareString;//i + j * (numberOfSteps + 1);
//				indices[1 + i * indicesPerSquare + j * indicesPerSquareString] = 0 + i * vertexPerSquare + j * vertexPerSquareString;//i + (numberOfSteps + 1) + j * (numberOfSteps + 1);
//				indices[2 + i * indicesPerSquare + j * indicesPerSquareString] = 2 + i * vertexPerSquare + j * vertexPerSquareString;//i + 1 + j * (numberOfSteps + 1);
//
//				indices[3 + i * indicesPerSquare + j * indicesPerSquareString] = 1 + i * vertexPerSquare + j * vertexPerSquareString;//i + 1 + j * (numberOfSteps + 1);
//				indices[4 + i * indicesPerSquare + j * indicesPerSquareString] = 2 + i * vertexPerSquare + j * vertexPerSquareString;//i + (numberOfSteps + 1) + j * (numberOfSteps + 1);
//				indices[5 + i * indicesPerSquare + j * indicesPerSquareString] = 3 + i * vertexPerSquare + j * vertexPerSquareString;//i + (numberOfSteps + 1) + 1 + j * (numberOfSteps + 1);

			}
		}

		lnk.vbo.iboSize = indices.length;

		lnk.vbo.density = lnk.group.getDensity();
		
//		FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(vao.length);
//		vertexBuffer.put(vao);
//		vertexBuffer.rewind();

//		IntBuffer indexBuffer = BufferUtils.createIntBuffer(lnk.vbo.iboSize);
//		indexBuffer.put(indices);
//		indexBuffer.rewind();

//		lnk.vbo.vbo = vertexBuffer;
		lnk.vbo.vbo = VBO.createFloatBuffer(vertexes, colors, normals, texCoords);
//		lnk.vbo.ibo = indexBuffer;
		lnk.vbo.ibo = VBO.createIndexBuffer(newIndeces);
		//GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, lnk.vbo.vboId);
		
		lnk.vbo.loadBuffer2GPU();
	}

//	public int getLog2Density(int density) {
//		int log2 = 0;
//
//		if (density <= 1)
//			log2 = 0;
//		else if (density <= 2)
//			log2 = 1;
//		else if (density <= 4)
//			log2 = 2;
//		else if (density <= 8)
//			log2 = 3;
//		else if (density <= 16)
//			log2 = 4;
//		else if (density <= 32)
//			log2 = 5;
//		else if (density <= 64)
//			log2 = 6;
//
//		return log2;
//	}

	public int getDensity(float distance) { // плотность в зависимости от дальности и высоты
		int density = 1;

		if (distance <= 64)
			density = 2;
		else if (distance <= 128)
			density = 2;
		else if (distance <= 256)
			density = 2;
		else if (distance <= 512)
			density = 4;
		else if (distance <= 1024)
			density = 8;
		else if (distance <= 2048)
			density = 8; //8
		else if (distance <= 3072)
			density = 8; //16
		else if (distance <= 4096)
			density = 16; //16
		else {
			density = 64;
		}

		return density;
	}

	public boolean isGroupLoaded(MapGroup gr) {
		if (getLink(gr) == null) {
			return false;
		} else {
			return true;
		}
	}

	public Link getLink(MapGroup gr) {
		
		for (int i = 0; i < vboMapHD.size(); i++) {
			if (vboMapHD.get(i).group == gr) {
				return vboMapHD.get(i);
			}
		}
		return null;
	}

	public class Link {
		MapGroup group;
		VBO vbo;

		public int getDensity() {
			if (vbo != null)
				return vbo.density;
			else
				return 0;
		}
	}

	

}
