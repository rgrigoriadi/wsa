package WSA.map;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.unit.MapObject;
import WSA.unit.ObjectController;

public class MapGroup {

	/* 65 - удобно для работы с большими масштабами, средне, при зум-ине
	 * 
	 * 35 - супер при зум-ине, средне и ниже среднего при больших мастабах */
	public static final int GROUPSIZE = 65; // 2 ^ n + 1
	public static final int GROUP_STEP = GROUPSIZE - 1; // 2 ^ n
	public static final int MIN_DENSITY = 1;

	public static final int INDEX_PER_SQUARE = 6;
	public static final int VERTEX_PER_SQUARE = 4;

	public static final float HEIGHT_NON_GENERATED = -1234567.89f;

	public static final byte GENERATED = 1;
	public static final byte NON_GENERATED = 0;
	public static final byte SAVED = 3;

	public byte status = NON_GENERATED;

	public static final int SIDE_XM = 0;
	public static final int SIDE_ZM = 1;
	public static final int SIDE_XP = 2;
	public static final int SIDE_ZP = 3;

	public MapGroup[] neighborGroups;

	private float height[][];
	private byte material[][];
	private Vector3f normals[][];
	private float richness[][];

	public MapController map;

	private int startX;
	private int startZ;

	private int visibility;

	private int density = MIN_DENSITY;

	private float heightDeviation;

	public MapGroup(int stX, int stZ, MapController map) {
		startX = stX;
		startZ = stZ;
		this.map = map;
		visibility = 1;
		height = new float[GROUPSIZE][GROUPSIZE];
		material = new byte[GROUPSIZE - 1][GROUPSIZE - 1];
		richness = new float[GROUPSIZE - 1][GROUPSIZE - 1];

		status = NON_GENERATED;

		fillGroupWithZeros();

		float sumHeightDeviation = 0.0f;
		int numberOfNeighbors = 0;

		neighborGroups = new MapGroup[4];

		for (int side = 0; side < 4; side++) {
			Vector2f neighborCoord = getNeighborCoordinatesBySide_RelativeNormalised(side);
			neighborCoord.x = neighborCoord.x * GROUP_STEP + stX;
			neighborCoord.y = neighborCoord.y * GROUP_STEP + stZ;

			MapGroup neighborGroup = map.getGroup(neighborCoord);

			if (neighborGroup != null) {
				neighborGroup.neighborGroups[getOppositeSide(side)] = this;
				sumHeightDeviation += neighborGroup.heightDeviation;
				numberOfNeighbors += 1;
				neighborGroups[side] = neighborGroup;
			}
		}

		heightDeviation = MapGenerator.getNormalRandom(sumHeightDeviation / (numberOfNeighbors == 0 ? 1 : numberOfNeighbors), (4 - numberOfNeighbors) * 3, 0);

		generate();
	}

	public void generate() {

		// Если границы с данным блоком уже сгенерированы - мы должны не
		// перегенерировать их, а скопировать

		for (int i = 0; i <= GROUP_STEP; i++) {
			for (int side = 0; side < 4; side++) {
				if (neighborGroups[side] != null) {
					Vector2f dir = getNeighborCoordinatesBySide_RelativeNormalised(side);
					int xThis = dir.x == 0 ? i : dir.x == -1 ? 0 : GROUP_STEP;
					int zThis = dir.y == 0 ? i : dir.y == -1 ? 0 : GROUP_STEP;
					int xNeig = dir.x == 0 ? i : dir.x == -1 ? GROUP_STEP : 0;
					int zNeig = dir.y == 0 ? i : dir.y == -1 ? GROUP_STEP : 0;

					height[xThis][zThis] = neighborGroups[side].getHeight(xNeig, zNeig);
				}
			}
		}

		// То же самое по углам
		for (int dz = -1; dz < 2; dz += 2) {
			for (int dx = -1; dx < 2; dx += 2) {
				MapGroup angleGroup = map.getGroup(startX + dx * GROUP_STEP, startZ + dz * GROUP_STEP);

				if (angleGroup != null && neighborGroups[dx + 1] == null && neighborGroups[dz + 2] == null) {
					int targetX = (dx + 1) / 2 * GROUP_STEP;
					int targetZ = (dz + 1) / 2 * GROUP_STEP;
					this.height[targetX][targetZ] = angleGroup.height[GROUP_STEP - targetX][GROUP_STEP - targetZ];
				}
			}
		}

		// простой рекурсивый алгоритм v0.1

		float rand = (float) Math.random();
		if (rand < 0.5f) {
			heightDeviation = MapGenerator.CORNER_DEVIATION_PLAIN;
		} else if (rand < 0.8f) {
			heightDeviation = MapGenerator.CORNER_DEVIATION_HILLS;
		} else {
			heightDeviation = MapGenerator.CORNER_DEVIATION_MOUNT;
		}

		map.getMapGenerator().generate(this, heightDeviation);

		//		createNormalsCenter();
		generateNormals();
		for (int side = 0; side < 4; side++) {
			if (neighborGroups[side] != null) {
				//				createNormalsSide(side);
				neighborGroups[side].generateNormals();//createNormalsSide(getOppositeSide(side));
			}
		}

		//Game.print("Generated box " + startX + " " + startZ + " with sigmaHeights = " + sigmaHeights);

		status = GENERATED;

		//		objectsInGroup = new ArrayList<MapObject>();

	}

	public void fillGroupWithZeros() {
		for (int i = 0; i <= GROUP_STEP; i++) {
			for (int j = 0; j <= GROUP_STEP; j++) {
				height[i][j] = HEIGHT_NON_GENERATED;

				if (i < GROUP_STEP && j < GROUP_STEP) {
					material[i][j] = MapMaterial.NULL;
					richness[i][j] = 0.0f;
				}
			}
		}
	}

	public void fillMaterialsWith(byte theMaterial) {
		for (int i = 0; i < GROUP_STEP; i++) {
			for (int j = 0; j < GROUP_STEP; j++) {
				material[i][j] = theMaterial;
			}
		}
	}

	public int getDensity() {
		return density;
	}

	public void setDensity(int density) {
		if (this.density == density) {
			return;
		}
		if (density < MIN_DENSITY) {
			this.density = MIN_DENSITY;
		} else if (density > GROUP_STEP) {
			this.density = GROUP_STEP;
		} else {
			int i = GROUP_STEP;
			boolean isDensitySet = false;
			while (i > 1) {
				if (i <= density) { // i - всегда степень двойки, так мы ищем ближайшую меньшую искомого числа степень двойки, либо проверяем число на степенность
					this.density = i;
					isDensitySet = true;
					break;
				}
				i /= 2;
			}
			if (isDensitySet == false)
				this.density = 1;
		}

		//		createNormalsCenter();
		generateNormals();

	}

	public float getHeight(int i, int j) {
		if (i >= 0 && i <= GROUPSIZE - 1 && j >= 0 && j <= GROUPSIZE - 1) {
			return height[i][j];
		} else {
			return map.getHeight(startX + i, startZ + j);
		}
	}
	
//	public float getHeight(float x, float z) {
//		if (x > GROUP_STEP || x < 0 || z > GROUP_STEP || z < 0) {
////			return map.getHeight(gx, z);
//		}
//	}

	public byte getMaterial(int i, int j) {
		return material[i][j];
	}

	public float getRichness(int i, int j) {
		return richness[i][j];
	}

	public Vector3f getNormalForVertex(int xPositionInChunk, int zPositionInChunk) {
		float nearbieHeights[][] = new float[3][3];

		for (int dx = -1; dx <= 1; dx++) {
			for (int dz = -1; dz <= 1; dz++) {
				nearbieHeights[dx + 1][dz + 1] = getHeight((xPositionInChunk / density + dx) * density, (zPositionInChunk / density + dz) * density);
			}
		}

		return Normaliser.getNormalByNearbies(nearbieHeights, density);
	}

	public void generateNormals() {
		normals = null;
		normals = new Vector3f[GROUP_STEP / density + 1][GROUP_STEP / density + 1];

		clearNormals();

		for (int j = 0; j <= GROUP_STEP / density; j++) {
			for (int i = 0; i <= GROUP_STEP / density; i++) {
				normals[i][j] = getNormalForVertex(i * density, j * density);
			}
		}
	}

	//	public void createNormalsCenter() {
	//
	//		normals = null;
	//		normals = new Vector3f[GROUP_STEP / density + 1][GROUP_STEP / density + 1];
	//
	//		clearNormals();
	//
	//		for (int j = 1; j < GROUP_STEP / density; j++) {
	//			for (int i = 1; i < GROUP_STEP / density; i++) {
	//				normals[i][j] = getNormalForVertex(i * density, j * density);
	//			}
	//		}
	//	}
	//
	//	public void createNormalsSide(int side) {
	//		int stx, stz, fnx, fnz;
	//		int stDef = 0;
	//		int fnDef = GROUP_STEP / density;
	//		
	//		stx = side == SIDE_XP ? fnDef 
	//							  : stDef;
	//		stz = side == SIDE_ZP ? fnDef
	//							  : stDef;
	//		fnx = side == SIDE_XM ? stDef
	//							  : fnDef;
	//		fnz = side == SIDE_ZM ? stDef
	//				  			  : fnDef;
	//
	//		for (int j = stz; j <= fnz; j++) {
	//			for (int i = stx; i <= fnx; i++) {
	//				normals[i][j] = getNormalForVertex(i * density, j * density);
	//			}
	//		}
	//	}

	public Vector3f getCenterPoint() {
		return new Vector3f(startX + GROUP_STEP / 2, this.getHeight(GROUP_STEP / 2, GROUP_STEP / 2), startZ + GROUP_STEP / 2);
	}

	public boolean isVisible() {
		if (visibility > 0) {
			return true;
		} else {
			return false;
		}
	}

	public int getVisibility() {
		return visibility;
	}

	public void increaseVisibility() {
		if (visibility == 0) {
			visibility++;
		}
	}

	public void decreaseVisibility() {
		visibility--;
		if (visibility <= 0) {
			visibility = 0;
		}

	}

	public int getStartX() {
		return startX;
	}

	public int getStartZ() {
		return startZ;
	}

	public void clearNormals() {
		for (int i = 0; i <= GROUP_STEP / density; i++) {
			for (int j = 0; j <= GROUP_STEP / density; j++) {
				normals[i][j] = new Vector3f(0.0f, 1.0f, 0.0f);
			}
		}
	}

	public boolean isEqual(MapGroup gr) {
		if ((gr.startX == this.startX) && (gr.startZ == this.startZ)) {
			return true;
		} else {
			return false;
		}
	}

	public Vector2f getNeighborCoordinatesBySide_RelativeNormalised(int side) {
		float dx = side == SIDE_XM ? -1.0f : side == SIDE_XP ? 1.0f : 0.0f;
		float dz = side == SIDE_ZM ? -1.0f : side == SIDE_ZP ? 1.0f : 0.0f;
		return new Vector2f(dx, dz);
	}

	public int getOppositeSide(int side) {
		return side == SIDE_XM ? SIDE_XP : side == SIDE_XP ? SIDE_XM : side == SIDE_ZM ? SIDE_ZP : SIDE_ZM;
	}

	public float getHeightDeviation() {
		return heightDeviation;
	}

	public Vector3f getNormal(int i, int j) {
		return normals[i][j];
	}

	public float[][] getHeights() {
		return height;
	}

	public byte[][] getMaterials() {
		return material;
	}

	public float[][] getRichness() {
		return richness;
	}
	
	public Vector2f getCoordinatesWithinGroup (Vector2f coord) {
		return new Vector2f(coord.x - startX, coord.y - startZ);
	}
	
	public float getAngleOfLandscape(Vector2f coord) {
		return Math.abs(Vector3f.angle(new Vector3f(0.0f, 1.0f, 0.0f), Normaliser.getNormalInPoint(this, coord)) / (float) Math.PI * 180.0f);
	}
}
