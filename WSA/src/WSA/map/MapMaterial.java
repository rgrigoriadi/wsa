package WSA.map;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector4f;

import WSA.Game;
import WSA.RectangleFloat;
import WSA.graphics.render.Render;
import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class MapMaterial {
	
	
	public static final byte NULL = 0;
	public static final byte DIRT = 1;
	public static final byte GRASS = 2;
	public static final byte STONE = 3;
	public static final byte SAND = 4;
	
	public static final byte UNGENERATED = (byte) 666; // единственный без текстуры

	
	static ByteBuffer textureBuffer = null;
	static int textureId;
	
	static int TEXTURE_FILE_WIDTH = 0;
	static int TEXTURE_FILE_HEIGHT = 0;
	
	public static int textureSize = 128;
	public static int textureMapSize = 4;
	
	public static final byte NUMBER_OF_MATERIALS = 3;
	
	public static Vector4f getColor (byte arg) {
		
		Vector4f color = new Vector4f(1.0f, 1.0f, 1.0f, 0.0f);
		
		if (arg == UNGENERATED)
			color.set(0.5f, 0.5f, 0.75f, 1.0f);
		
		return color;		
	}
	
	public static int loadTexture () {
		
		try {
//			FileOutputStream fos = new FileOutputStream("./resources/out.txt");
			
			
			InputStream in = new FileInputStream(Render.FILEPATH_TEXTUREPACK_MAP);
			PNGDecoder decoder = new PNGDecoder(in);
			
			textureBuffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
			
			Game.print("w h " + decoder.getWidth() + " " + decoder.getHeight() + " tex buff capacity = " + textureBuffer.capacity());
			
			TEXTURE_FILE_HEIGHT = decoder.getHeight();
			TEXTURE_FILE_WIDTH = decoder.getWidth();
			
			textureMapSize = TEXTURE_FILE_WIDTH / textureSize;
			
			decoder.decode(textureBuffer, decoder.getWidth() * 4, Format.RGBA);
			textureBuffer.flip();
			
			// Create a new texture object in memory and bind it
			textureId = GL11.glGenTextures();
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
			GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
			
			// Upload the texture data and generate mip maps (for scaling)
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, TEXTURE_FILE_WIDTH, TEXTURE_FILE_HEIGHT, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, textureBuffer);
			GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
			
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return textureId; 
	}
	
	public static void deallocTexture () {
		if (textureBuffer != null)
			textureBuffer = ByteBuffer.allocateDirect(0);
	}
	
	public static RectangleFloat getTexCoord (byte material) {
		if (material == UNGENERATED) 
			return null;
		
		float inc = 0.5f;
		
		RectangleFloat ret = new RectangleFloat((material % textureMapSize  * (textureSize) + inc) / (float) TEXTURE_FILE_WIDTH, ((material % textureMapSize + 1) * (textureSize) - inc) / (float) TEXTURE_FILE_WIDTH, (material / textureMapSize  * (textureSize) + inc) / (float) TEXTURE_FILE_WIDTH, ((material / textureMapSize + 1)  * (textureSize) - inc) / (float) TEXTURE_FILE_WIDTH);
		return ret;
	}
	
	public static String getName(byte material) {
		switch (material) {
		case 0:
			return "NULL";
		case 1:
			return "dirt";
		case 2:
			return "grass";
		case 3:
			return "stone";
		case 4:
			return "sand";
		default:
			return "null";
		}
	}
	
}
