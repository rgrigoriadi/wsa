package WSA.map;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Normaliser {

	
	public static void normalize(Vector3f arg) {
		float len = (float) Math.sqrt((arg.x * arg.x) + (arg.y * arg.y) + (arg.z * arg.z));
		arg.x /= len;
		arg.y /= len;
		arg.z /= len;
	}
	
	public static Vector3f getNormal(Vector3f aa, Vector3f bb, Vector3f cc) { // получаем нормаль по 3-м точкам
		Vector3f ret = new Vector3f();
		Vector3f b = new Vector3f(), c = new Vector3f();
		Vector3f.sub(bb, aa, b);
		Vector3f.sub(cc, aa, c);

		ret.x = b.y * c.z - b.z * c.y;
		ret.y = b.z * c.x - b.x * c.z;
		ret.z = b.x * c.y - b.y * c.x;
		// нормаль направленна только вверх
		if (ret.y < 0) {
			ret.x = -ret.x;
			ret.y = -ret.y;
			ret.z = -ret.z;
		}
		normalize(ret);
		//Game.print(" " + a.y + " " + b.y + " " + c.y);
		//Game.print(" " + ret.x + " " + ret.y + " " + ret.z + " len = " + ret.length());
		return ret;
	}
	
	public static Vector3f getNormalByPrenormals(Vector3f nordWest, Vector3f nordNordEast, Vector3f eastNordEast, Vector3f southEast, Vector3f southSouthWest, Vector3f westSouthWest) {
		
		Vector3f n = new Vector3f();

		n.x = (nordWest.x + (nordNordEast.x + eastNordEast.x) / 2.0f + southEast.x + (southSouthWest.x + westSouthWest.x) / 2.0f) / 4.0f;
		n.y = (nordWest.y + (nordNordEast.y + eastNordEast.y) / 2.0f + southEast.y + (southSouthWest.y + westSouthWest.y) / 2.0f) / 4.0f;
		n.z = (nordWest.z + (nordNordEast.z + eastNordEast.z) / 2.0f + southEast.z + (southSouthWest.z + westSouthWest.z) / 2.0f) / 4.0f;
		
		normalize(n);
		return n;
	}
	
	public static Vector3f getNormalByNearbies(float neighborHeights[][], int density) {
		Vector3f[] preNormals = getPreNormals(neighborHeights, density);
	
		return getNormalByPrenormals(preNormals[0], preNormals[1], preNormals[2], preNormals[3], preNormals[4], preNormals[5]); 
	}
	
	public static Vector3f[] getPreNormals(float neighborHeights[][], float distance) {
		Vector3f[] preNormals = new Vector3f[6];
		
		preNormals[0] = getNormal(new Vector3f(-distance, neighborHeights[0][1], 0), new Vector3f(0, neighborHeights[1][1], 0), new Vector3f(0, neighborHeights[1][0], -distance));
		preNormals[1] = getNormal(new Vector3f(0, neighborHeights[1][1], 0), new Vector3f(distance, neighborHeights[2][0], -distance), new Vector3f(0, neighborHeights[1][0], -distance));
		preNormals[2] = getNormal(new Vector3f(0, neighborHeights[1][1], 0), new Vector3f(distance, neighborHeights[2][1], 0), new Vector3f(distance, neighborHeights[2][0], -distance));
		
		preNormals[3] = getNormal(new Vector3f(0, neighborHeights[1][2], distance), new Vector3f(distance, neighborHeights[2][1], 0), new Vector3f(0, neighborHeights[1][1], 0));
		preNormals[4] = getNormal(new Vector3f(0, neighborHeights[1][1], 0), new Vector3f(-distance, neighborHeights[0][2], distance), new Vector3f(0, neighborHeights[1][2], distance));
		preNormals[5] = getNormal(new Vector3f(0, neighborHeights[1][1], 0), new Vector3f(-distance, neighborHeights[0][1], 0), new Vector3f(-distance, neighborHeights[0][2], distance));
		return preNormals;
	}
	
	public static Vector3f getNormalInPoint (MapGroup gr, Vector2f pos) {
		int x = (int) pos.x;
		int z = (int) pos.y;
		Vector3f nminus = getNormal(new Vector3f(x, gr.getHeight(x, z), z), new Vector3f(x, gr.getHeight(x, z + 1), z + 1), new Vector3f(x + 1, gr.getHeight(x + 1, z), z));
		Vector3f nplus = getNormal(new Vector3f(x + 1, gr.getHeight(x + 1, z + 1), z + 1), new Vector3f(x + 1, gr.getHeight(x + 1, z), z), new Vector3f(x, gr.getHeight(x, z + 1), z + 1));
		Vector3f ret = Vector3f.add(nminus, nplus, null);
		ret.x /= 2.0f;
		ret.y /= 2.0f;
		ret.z /= 2.0f;
		return ret;
	}
}
