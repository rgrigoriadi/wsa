package WSA;

public class Date {

	int year;
	int month;
	int day;
	int part;
	float rest;
	
	public static final int MON_JANUARY = 0;
	public static final int MON_FEBRUARY = 1;
	public static final int MON_MARCH = 2;
	public static final int MON_APRIL = 3;
	public static final int MON_MAY = 4;
	public static final int MON_JUNE = 5;
	public static final int MON_JULY = 6;
	public static final int MON_AUGUST = 7;
	public static final int MON_SEPTEMBER = 8;
	public static final int MON_OCTOBER = 9;
	public static final int MON_NOVEMBER = 10;
	public static final int MON_DECEMBER = 11;
	
	public static final int DAY_NIGHT = 0;
	public static final int DAY_MORNING = 1;
	public static final int DAY_AFTERNOON = 2;
	public static final int DAY_EVENING = 3;
	
	public Date () {
		this.year = 0;
		this.month = 0;
		this.month = 0;
		this.part = 0;
		this.rest = 0;
	}
	
	public Date (int year, int month, int day, int part, float rest) {
		this.year = year;
		this.month = month % Game.MONTHS_PER_YEAR;
		this.day = day % Game.DAYS_PER_MONTH;
		this.part = part % Game.DAY_PARTS_PER_DAY;
		this.rest = rest % Game.DAY_PART_DURATION;
	}
	
	public Date (float secs) {
		Date dat = getDate (secs);
		this.year = dat.year;
		this.month = dat.month;
		this.day = dat.day;
		this.part = dat.part;
		this.rest = dat.rest;
	}
	
	public static Date getDate (float secSpent) {
		Date dat = new Date (0, 0, 0, 0, 0);
		dat.year = (int) (secSpent / (float) Game.YEAR_DURATION); 
		secSpent = secSpent % Game.YEAR_DURATION;
		
		dat.month = (int) (secSpent / (float) (Game.MONTH_DURATION)); 
		secSpent = secSpent % (Game.MONTH_DURATION);
		
		dat.day = (int) (secSpent / (float) (Game.DAY_DURATION)); 
		secSpent = secSpent % (Game.DAY_DURATION);
		
		dat.part = (int) (secSpent / (float) (Game.DAY_PART_DURATION)); 
		dat.rest = secSpent % (Game.DAY_PART_DURATION);
		return dat;
	}
	
	public void setDate (float secSpent) {
		year = (int) (secSpent / (float) Game.YEAR_DURATION); 
		secSpent = secSpent % Game.YEAR_DURATION;
		
		month = (int) (secSpent / (float) (Game.MONTH_DURATION)); 
		secSpent = secSpent % (Game.MONTH_DURATION);
		
		day = (int) (secSpent / (float) (Game.DAY_DURATION)); 
		secSpent = secSpent % (Game.DAY_DURATION);
		
		part = (int) (secSpent / (float) Game.DAY_PART_DURATION); 
		rest = secSpent % (Game.DAY_PART_DURATION);
	}
	
	public static float getDateInSecs (Date date) {
		return (date.year * Game.YEAR_DURATION + date.month * Game.MONTH_DURATION + date.day * Game.DAY_DURATION + date.part * Game.DAY_PART_DURATION);
	}
	
	public float setDateInSecs () {
		return (year * Game.YEAR_DURATION + month * Game.MONTH_DURATION + day * Game.DAY_DURATION + part * Game.DAY_PART_DURATION);
	}
	
	public Date sub (Date max, Date min) {
		Date dat = new Date(0, 0, 0, 0, 0);

		float maxf = getDateInSecs(max);
		float minf = getDateInSecs(min);
		
		dat = getDate(maxf - minf);
		
		if (minf > maxf) { // в случае непредвидки
			dat.year = 0;
			dat.month = 0;
			dat.day = 0;
			dat.part = 0;
			dat.rest = 0;
		}
		
		return dat;
	}
	
	public static String getDateString(Date date) {
		String ret = new String();
		
		String mon = new String();
		
		switch (date.month) {
		case MON_JANUARY:
			mon = "jan";
			break;
		case MON_FEBRUARY:
			mon = "feb";
			break;
		case MON_MARCH:
			mon = "mar";
			break;
		case MON_APRIL:
			mon = "apr";
			break;
		case MON_MAY:
			mon = "may";
			break;
		case MON_JUNE:
			mon = "jun";
			break;
		case MON_JULY:
			mon = "jul";
			break;
		case MON_AUGUST:
			mon = "aug";
			break;
		case MON_SEPTEMBER:
			mon = "sep";
			break;
		case MON_OCTOBER:
			mon = "oct";
			break;
		case MON_NOVEMBER:
			mon = "nov";
			break;
		case MON_DECEMBER:
			mon = "dec";
			break;
		default:
			mon = "XXX";
			break;
		}
		
		String day = new String();
		
		switch (date.day) {
		case 0:
			day = "*..";
			break;
		case 1:
			day = ".*.";
			break;
		case 2:
			day = "..*";
			break;
		default:
			break;
		}
		
		
		//ret = (day + " " + mon + " " + date.year);
		ret = ((date.day + 1) + "|" + Game.DAYS_PER_MONTH + " " + mon + " " + date.year);
		
		return ret;
	}
	
	public String getDateString() {
		return getDateString(this);
	}
	
	public static String getTimeString(Date date) {
		String ret = new String();
		int minutes = (int) ((date.part * Game.DAY_PART_DURATION + date.rest) * (24.0f / Game.DAY_DURATION) * 60.0f);
		//Game.print(" minutes = " + minutes + " gameTime = " + Game.getGameTimeInSeconds() + " Date : y,m,d,p = " + date.year + " " + date.month + " " + date.day + " " + date.part);
		ret = ("" + (int) (minutes / 60.0f) + ":" + (int) (minutes % 60.0f));
		return ret;
	}
	
	public String getTimeString() {
		return getTimeString(this);
	}
}
