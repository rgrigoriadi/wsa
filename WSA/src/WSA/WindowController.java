package WSA;

import java.io.IOException;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector2f;

import WSA.graphics.render.Render;
import WSA.map.MapGroup;
import WSA.map.MapMaterial;
import WSA.player.Player;
import WSA.player.ViewController;
import WSA.ui.gui.DebugWindow;
import WSA.ui.gui.MenuController;
import WSA.ui.gui.menu.FuncWindow;
import WSA.ui.gui.menu.InventoryMenu;
import WSA.ui.gui.menu.QuestionWindow;
import WSA.ui.gui.menu.StatusWindow;
import WSA.ui.input.InputController;
import de.matthiasmann.twl.Alignment;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.Dimension;
import de.matthiasmann.twl.Event;
import de.matthiasmann.twl.GUI;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.Widget;
import de.matthiasmann.twl.renderer.lwjgl.LWJGLRenderer;
import de.matthiasmann.twl.theme.ThemeManager;

public class WindowController extends Widget {

	CollisionsController collisionController;
	Render renderer;
	Player playerHandler;

	InputController input;

	private MenuController menuController;
	
	public WindowController(Player player, ViewController user, CollisionsController collisionController, Render render) {

		this.playerHandler = player;
		this.renderer = render;
		this.collisionController = collisionController;

		this.playerHandler.getActionController().setWindowResponder(this);
		input = new InputController(playerHandler, this);

		
		initGUI();
	}

	public void onCamModeFree() {
		disableMultiSelection();
	}

	public void onCamMove() {
		disableMultiSelection();
	}

	public void onStop() {
		Mouse.setGrabbed(false);
		Mouse.destroy();
		Keyboard.destroy();

		this.removeAllChildren();
		this.destroy();
	}

	public Dimension getWindowSize() {
		return new Dimension(getWidth(), getHeight());
	}

	Vector2f mapSize;
	Vector2f statSize;
	Vector2f statusBarSize;
	Vector2f sideStatTSize;
	Vector2f sideStatBSize;
	Label mapLabel, sideStatTLabel, sideStatBLabel, statusBarLabel;
	Button buttonButton;

	private GUI TWLGUI = null;
	private LWJGLRenderer TWLRenderer = null;

	boolean isGUIEnabled = true;
	public int buttonNumberY;
	public int buttonNumberX;
	public int buttonNumberXMin;
	public int buttonNumberYDefault;
	int buttonNumberXMax;
	public int buttonNumberYMax;
	public int buttonSizeD;
	public int buttonMargin;
	public static int TEXTHEIGHT;
	public int mainPannelHeight;

	QuestionWindow questionWindow = null;
	FuncWindow funcWindow = null;
	
	public InventoryMenu inventoryMenu;
	
	public StatusWindow statWindow = null;

	private boolean isCursorEnabled = false;
	private Label cursorLabel;

	Vector2f coord[] = new Vector2f[4];
	float minX = Float.NaN, maxX = Float.NaN, minY = Float.NaN, maxY = Float.NaN;

	private boolean isMultiSelectionEnabled = false;
	public int multiSelectionStartX;
	public int multiSelectionStartY;
	public int multiSelectionFinishX;
	public int multiSelectionFinishY;
	Label multiSelectionLabel = null;

	public Vector2f lmbSelectionStartOnScreen = null; // Начало выбора
	public Vector2f lmbSelectionStartOnMap = null;

	public Vector2f lmbSelectionFinishOnScreen = null; // Конец выбора
	public Vector2f lmbSelectionFinishOnMap = null;

	public void initGUI() {

		try {
			TWLRenderer = new LWJGLRenderer();
			TWLGUI = new GUI(this, TWLRenderer);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}

		ThemeManager theme;

		try {
			theme = ThemeManager.createThemeManager(WSA.class.getResource("/game0.xml"), TWLRenderer);
			TWLGUI.applyTheme(theme);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.setSize(Render.WIDTH, Render.HEIGHT);

		int mapSizeD = 250;

		TEXTHEIGHT = 8; // брать из фонта

		buttonNumberYDefault = 3;
		buttonNumberY = buttonNumberYDefault;
		//buttonNumberX = 2;
		buttonNumberXMin = 3;
		buttonNumberXMax = 8;
		buttonSizeD = 50;
		buttonMargin = buttonSizeD / 5; //10;

		mapSize = new Vector2f(mapSizeD, mapSizeD);
		sideStatTSize = new Vector2f(mapSizeD, 40.0f);
		sideStatBSize = new Vector2f(mapSizeD, mapSizeD);

		enableGUI();
	}

	public void enableGUI() {

		DebugWindow.init(this, this);

		menuController = new MenuController(this, this);
		
//		Button[] buttsForQ = new Button[3];
//
//		buttonButton = new Button("Butt");
//		buttonButton.setTheme("button0");
//		buttonButton.setSize(buttonSizeD, buttonSizeD);
//		//buttonButton.setPosition((int) statSize.x, Alignment.BOTTOM.computePositionY(height, buttonButton.getHeight()));
//
//		buttsForQ[0] = buttonButton;
//
//		buttonButton = new Button();
//		buttonButton.setTheme("button" + buttonSizeD + "px_move");
//		buttonButton.setSize(buttonSizeD, buttonSizeD);
//
//		buttsForQ[1] = buttonButton;
//
//		buttonButton = new Button();
//		buttonButton.setTheme("button0");
//		buttonButton.setSize(buttonSizeD, buttonSizeD);
//
//		buttsForQ[2] = buttonButton;
//
//		enableQuestionLabel("Would you like?", buttsForQ);

		isGUIEnabled = true; // открыть все гуи
	}

	@Override
	protected boolean handleEvent(Event evt) {
		boolean hitGUI = super.handleEvent(evt);
		if (!hitGUI) {
			boolean isPushed = false;
			int btn = -1;
			if (evt.getType() == Event.Type.MOUSE_BTNDOWN) {
				isPushed = true;
				switch (evt.getMouseButton()) {
				case 0:
					btn = 0;
					break;
				case 1:
					btn = 1;
					break;
				case 2:
					btn = 2;
					break;
				default:
					break;
				}

			}

			if (evt.getType() == Event.Type.MOUSE_BTNUP) {

				isPushed = false;
				switch (evt.getMouseButton()) {
				case 0:
					btn = 0;
					break;
				case 1:
					btn = 1;
					break;
				case 2:
					btn = 2;
					break;
				default:
					break;
				}
			}

			if (btn != -1) {
				input.pollMouseWorldClick(btn, isPushed, collisionController);
			}
		}
		return true;
	}

	public void getCloseButtonStyle(Button button, Runnable run) {
		button.setSize(70, 70);
		button.setTheme("closeButton");
		button.setText("Close");
		button.addCallback(run);
	}

	public void disableGUI() {
		isGUIEnabled = false; // убрать все
	}

	public void toggleFullScreen() {
		renderer.toggleFullScreen();
	}

	public void refresh(float time) {
		getMenuController().refresh(time);

		Vector2f curPos = collisionController.getCursorIntersectionWithMap(playerHandler.getCam());
		
		if (curPos != null) {
			MapGroup hovered = collisionController.getMapGroup(curPos);
			if (hovered != null) {
				Game.print("SelectedMapGroup SigmaHeights = " + hovered.getHeightDeviation() + " density = " + hovered.getDensity());
				
				Vector2f grPos = hovered.getCoordinatesWithinGroup(curPos);
				
				Game.print("Matrial = " + MapMaterial.getName(hovered.getMaterial((int) grPos.x, (int) grPos.y)));
				
				Game.print("Landscape angle = " + hovered.getAngleOfLandscape(grPos) + " richness = " + hovered.getRichness((int) grPos.x, (int) grPos.y));
			}
		}
		
		if (statWindow != null) {
			statWindow.refreshStats();
		}

		refreshCursor();

		TWLGUI.update();

		DebugWindow.clear();
	}

	public void enableMultiSelection(int x, int y, int maxY) {
		this.multiSelectionStartX = x;
		this.multiSelectionStartY = y;

		this.multiSelectionFinishX = x;
		this.multiSelectionFinishY = y;

		isMultiSelectionEnabled = true;

		multiSelectionLabel = new Label();
		refreshMultiSelectionRectangle(maxY);
		multiSelectionLabel.setTheme("multiSelection0");
		this.add(multiSelectionLabel);
	}

	public void setMultiSelectionOnScreenPos(int x, int y) {
		if (isMultiSelectionEnabled == false) {
			enableMultiSelection(x, y, playerHandler.getCam().getCursor().getMaxY());
		}

		this.multiSelectionFinishX = x;
		this.multiSelectionFinishY = y;
		refreshMultiSelectionRectangle(playerHandler.getCam().getCursor().getMaxY());

		coord[0] = collisionController.getVectorIntersectionWithMap(playerHandler.getCam().getPosition(), playerHandler.getCam().getCursorDirection(multiSelectionStartX, multiSelectionStartY));
		coord[1] = collisionController.getVectorIntersectionWithMap(playerHandler.getCam().getPosition(), playerHandler.getCam().getCursorDirection(multiSelectionStartX, multiSelectionFinishY));
		coord[2] = collisionController.getVectorIntersectionWithMap(playerHandler.getCam().getPosition(), playerHandler.getCam().getCursorDirection(multiSelectionFinishX, multiSelectionFinishY));
		coord[3] = collisionController.getVectorIntersectionWithMap(playerHandler.getCam().getPosition(), playerHandler.getCam().getCursorDirection(multiSelectionFinishX, multiSelectionStartY));

		minX = Float.NaN;
		maxX = Float.NaN;
		minY = Float.NaN;
		maxY = Float.NaN;

		for (int i = 0; i < 4; i++) {
			if (coord[i].x >= maxX || Float.isNaN(maxX))
				maxX = coord[i].x;
			if (coord[i].y >= maxY || Float.isNaN(maxY))
				maxY = coord[i].y;
			if (coord[i].x <= minX || Float.isNaN(minX))
				minX = coord[i].x;
			if (coord[i].y <= minY || Float.isNaN(minY))
				minY = coord[i].y;
		}

		//TODO Убрать всю эту дрянь, т.к все, что нужно у проверщика есть

	}

	public void disableMultiSelection() {
		this.removeChild(multiSelectionLabel);
		if (multiSelectionLabel != null) {
			multiSelectionLabel.destroy();
		}
		isMultiSelectionEnabled = false;

		minX = Float.NaN;
		maxX = Float.NaN;
		minY = Float.NaN;
		maxY = Float.NaN; // TODO убрать это после взуализационного теста 

	}

	private void refreshMultiSelectionRectangle(int maxY) { // TODO убрать это после взуализационного теста
		int x, y, w, h;

		int sty = maxY - multiSelectionStartY, fny = maxY - multiSelectionFinishY;

		if (multiSelectionStartX > multiSelectionFinishX) {
			x = multiSelectionFinishX;
			w = multiSelectionStartX - multiSelectionFinishX;
		} else {
			x = multiSelectionStartX;
			w = multiSelectionFinishX - multiSelectionStartX;
		}

		if (sty > fny) {
			y = fny;
			h = sty - fny;
		} else {
			y = sty;
			h = fny - sty;
		}

		multiSelectionLabel.setPosition(x, y);
		multiSelectionLabel.setSize(w, h);

		//Game.print("y h  "+ y + ", " + h + "  || yst yfn  =  " + multiSelectionStartY + " " + multiSelectionFinishY);
	}

	public void enableCursor() {
		cursorLabel = new Label();
		cursorLabel.setTheme("cursor0");

		int curSize = playerHandler.getCam().getCursor().getCursorSize();
		cursorLabel.setSize(curSize, curSize);
		isCursorEnabled = true;
		this.add(cursorLabel);
	}

	public void disableCursor() {
		this.removeChild(cursorLabel);
		this.removeChild(cursorLabel);
		if (cursorLabel != null) {
			cursorLabel.destroy();
		}
		isCursorEnabled = false;
	}

	public void refreshCursor() {

		playerHandler.getCam().getCursor().refreshCursor();
		if (isCursorEnabled) {
			if (playerHandler.getCam().getCursor().isActive() == false) {
				disableCursor();
			} else {
				setCursorPosition(playerHandler.getCam().getCursor().getCursorWindowPosition());
			}
		} else {
			if (playerHandler.getCam().getCursor().isActive()) {
				enableCursor();
				setCursorPosition(playerHandler.getCam().getCursor().getCursorWindowPosition());
			}
		}
	}

	public void processSpaceButtonState(boolean isDown) {
		if (isDown) {
			if (playerHandler.getCam().isGlobalFree == false) {
				Mouse.getDX();
				Mouse.getDY();
				Mouse.setGrabbed(true);
				playerHandler.getCam().getCursor().setActive(false);
				playerHandler.getCam().getCursor().resetCursor();
				onCamModeFree();
			}
			playerHandler.getCam().isGlobalFree = true;
		} else {
			if (playerHandler.getCam().isGlobalFree == true) {
				Mouse.setGrabbed(true); //Mouse.setGrabbed(false);
				playerHandler.getCam().getCursor().setActive(true);
				playerHandler.getCam().getCursor().resetCursor();
				Mouse.setCursorPosition(playerHandler.getCam().getCursor().getX(), playerHandler.getCam().getCursor().getY());
			}

			playerHandler.getCam().isGlobalFree = false;
		}
	}

	public void setCursorPosition(int x, int y) {
		cursorLabel.setPosition(x, y);
	}

	public void setCursorPosition(Dimension pos) {
		cursorLabel.setPosition(pos.getX(), pos.getY());
	}

	public Label getCursorLabel() {
		return cursorLabel;
	}

	public void setCursorLabel(Label cursorLabel) {
		this.cursorLabel = cursorLabel;
	}

	public boolean isMultiSelectionEnabled() {
		return isMultiSelectionEnabled;
	}

	public void setMultiSelectionEnabled(boolean isMultiSelectionEnabled) {
		this.isMultiSelectionEnabled = isMultiSelectionEnabled;
	}
//	
//	public void enableStatMenu() {
//		disableStatMenu();
//		statWindow = new StatusWindow(this, this, getPannelSize(4), mainPannelHeight);
//	}
//
//	public void disableStatMenu() {
//		if (statWindow != null) {
//			statWindow.destroy();
//			statWindow = null;
//		}
//	}
//
//	public void enableFuncMenu() {
//		disableFuncMenu();
//		funcWindow = new FuncWindow(this, this);
//	}
//
//	public void disableFuncMenu() {
//		if (funcWindow != null) {
//			funcWindow.destroy();
//			funcWindow = null;
//		}
//	}
//
//	public void enableQuestionLabel(String text, Button[] butts) {
//		disableAdditionalMenu();
//		disableQuestionLabel();
//		if (funcWindow == null) {
//			enableFuncMenu();
//		}
//		questionWindow = new QuestionWindow(text, butts, funcWindow.getLabel(), this, this);
//	}
//
//	public void disableQuestionLabel() {
//		if (questionWindow != null) {
//			questionWindow.destroy();
//		}
//		questionWindow = null;
//	}
//
//	public void enableAdditionalMenu() {
//		disableAdditionalMenu();
//		disableQuestionLabel();
//
//	}
//
//	public void disableAdditionalMenu() {
//
//	}

	public Cursor getCursor() {
		return playerHandler.getCam().getCursor();
	}

	public ViewController getCam() {
		return playerHandler.getCam();
	}

	public InputController getInput() {
		return input;
	}
	
	public MenuController getMenuController () {
		return menuController;
	}

}
