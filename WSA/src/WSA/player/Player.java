package WSA.player;

import WSA.ObjectSelection;

public class Player {

	private int playerId;
	
	private ViewController playerView;
	private ObjectSelection selectedObjects;
	private PlayerActionController actionController;

	private int userActionMode = ACTION_MODE_SELECT_OBJECT;

	public static final int ACTION_MODE_NULL = -1;
	public static final int ACTION_MODE_SELECT_OBJECT = 0;
	public static final int ACTION_MODE_SELECT_ARGUMENT_POSITION = 1;
	public static final int ACTION_MODE_SELECT_ARGUMENT_OBJECT = 2;

	public Player(int id, ViewController viewController) {
		this.playerId = id;
		this.actionController = new PlayerActionController(this);
		this.playerView = viewController;
	}

	public void select(ObjectSelection selection, boolean isAddingToSelected) {
		if (selectedObjects == null) {
			selectedObjects = new ObjectSelection();
		}

		if (isAddingToSelected == false) {
			selectedObjects.clearSelection();
		}

		selectedObjects.add(selection);
	}

	public void unSelectAll() {
		selectedObjects.clearSelection();
	}

	public boolean hasSelection() {
		if (selectedObjects == null || selectedObjects.getSize() <= 0) {
			return false;
		} else {
			return true;
		}
	}

	public ObjectSelection getSelection() {
		return selectedObjects;
	}

	public PlayerActionController getActionController() {
		return actionController;
	}
	
	public ViewController getCam () {
		return playerView;
	}
	
	public int getId() {
		return playerId;
	}
}
