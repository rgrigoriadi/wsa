package WSA.player;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.ObjectSelection;
import WSA.WindowController;
import WSA.ui.input.SelectionController;
import WSA.unit.MapObject;
import WSA.unit.action.Action;
import WSA.unit.action.ActionHit;
import WSA.unit.action.ActionMove;

public class PlayerActionController {
	
	Player player;
	WindowController windowController;
	
	
	public PlayerActionController(Player player) {
		this.player = player;
	}

	public void setWindowResponder(WindowController window) {
		this.windowController = window;
	}
	
	public void onLMBClick(CollisionsController collisionsController, ViewController cam, SelectionController selectionController, boolean isShift) {
		if (!isShift && player.hasSelection()) {
			player.unSelectAll();
		}	

		if (selectionController.isSelectionEnabled()) {
			ObjectSelection selection = null;
			
			if (selectionController.isMultiSelectionEnabled()) {
				selection = selectionController.getSelectedObjects(player.getId());	
			}
			
			if (selection == null) {
				selection = new ObjectSelection(collisionsController.getObjectByCursor(cam));
			}
			
			if (selection != null && selection.getSize() > 0) {
				player.select(selection, isShift);
				if (selection.getSize() == 1) {
					onSelect(player);
				} else {
					onDeselect();
				}
			} else {
				onDeselect();
			}
		}
	}
	
	public void onSelect (Player  player) {
		if (player.getSelection().isOneObjectSelection()) {
			if (isShowingOnWindow()) {
				windowController.getMenuController().showStats(player.getSelection().getObject(0));
			}
		}
	}
	
	public void onDeselect() {
		if (isShowingOnWindow()) {
			windowController.getMenuController().closeStats();
		}
	}

	public void onRMBClick(CollisionsController collisionsController, ViewController cam, boolean isShift) {

		if (player.getSelection() != null && player.getSelection().getSize() > 0) {
			if (player.getSelection().getSelectionPlayerId() == player.getId()) {
				MapObject targetObject = collisionsController.getObjectByCursor(cam);

				if (targetObject != null) {
					if (targetObject.getPlayerId() != player.getId()) {
						atack(player.getSelection(), targetObject, collisionsController, isShift);
					} else {
						move(player.getSelection(), targetObject, collisionsController, isShift);
					}
				} else {
					move(player.getSelection(), collisionsController.getCursorIntersectionWithMap(cam), collisionsController, isShift);
				}
			}
		}
	}

	public void move(ObjectSelection objectsToMove, Vector2f position, CollisionsController collisionsController, boolean addActionToStack) {
		for (int i = 0; i < objectsToMove.getSize(); i++) {

			MapObject object = objectsToMove.getObject(i);

			ActionMove action = new ActionMove(new Vector3f(position.x, collisionsController.getMapHeight(position.x, position.y), position.y), collisionsController);

			if (action.isActionAppliableForObject(object)) {
				applyAction(object, action, addActionToStack);
			}
		}

	}

	public void move(ObjectSelection objectsToMove, MapObject targetObject, CollisionsController collisionsController, boolean addActionToStack) {
		for (int i = 0; i < objectsToMove.getSize(); i++) {

			MapObject object = objectsToMove.getObject(i);

			ActionMove action = new ActionMove(targetObject, 1.0f, collisionsController);

			if (action.isActionAppliableForObject(object)) {
				applyAction(object, action, addActionToStack);
			}
		}
	}
	
	
	public void atack(ObjectSelection atackingObjects, MapObject targetObject, CollisionsController collisionsController, boolean addActionToStack) {
		for (int i = 0; i < atackingObjects.getSize(); i++) {

			MapObject object = atackingObjects.getObject(i);

			ActionHit action = new ActionHit(true, targetObject, collisionsController);

			if (action.isActionAppliableForObject(object)) {
				applyAction(object, action, addActionToStack);
			}
		}
	}
	
	public void applyAction(MapObject object, Action action, boolean addActionToStack) {

		//action.getRequiredObjectClass() 
		if (!addActionToStack) {
			action.getAppliableObjectInstance(object).clearActionStack();
		}

		action.getAppliableObjectInstance(object).addAction(action);

	}
	
	public boolean isShowingOnWindow () {
		if (windowController != null) {
			return true;
		} else {
			return false;
		}
	}
}
