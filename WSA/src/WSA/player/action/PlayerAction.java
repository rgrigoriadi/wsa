package WSA.player.action;

public abstract class PlayerAction {
	protected Runnable command;
	protected String name;
	
	public void act() {
		command.run();
	}
	
	public String getName() {
		return name;
	}
}
