package WSA.player;


import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.Cursor;
import WSA.Game;
import WSA.graphics.render.Render;
import WSA.map.Normaliser;

/**
 * 
 * @author Roman
 */
public class ViewController {

	public boolean isGlobalFree;
	private Cursor cursor;

	private Vector3f position;

	private float camYAngle; // угол м/д 0, 0, -1 и направлением взгляда
	private float camXAngle; 
	
	private float CAM_Y_SPEED;
	private float CAM_XZ_SPEED;
	private float CAM_Y_ROTATION_SPEED;
	private float CAM_XZ_ROTATION_SPEED;
	private float CAM_INIT_Y_ANG; // угол м/д 0, 0, -1 и направлением взглядa
	private float CAM_INIT_X_ANG;
	private float SHIFT_SPEED_COEFICIENT = 10.0f;
	
	CollisionsController collisionController;
	
	public enum camMode {
		GLOBAL, FIRST_PERSON, THIRD_PERSON, NO_GAME
	}

	public ViewController(float x, float y, float z, CollisionsController collision) { // Положение центра экрана относительно нулевой координаты карты
		
		this.collisionController = collision;
		
		CAM_INIT_X_ANG = 0.0f; // z -z
		CAM_INIT_Y_ANG = -45.0f; // z -z

		camXAngle = CAM_INIT_X_ANG;
		camYAngle = CAM_INIT_Y_ANG;

		CAM_Y_SPEED = 5.0f;
		CAM_XZ_SPEED = 40.0f;
		CAM_Y_ROTATION_SPEED = 0.1f;
		CAM_XZ_ROTATION_SPEED = 0.1f;

		isGlobalFree = false;

		position = new Vector3f(x, y, z);

		cursor = new Cursor(Render.WIDTH - 1, Render.HEIGHT - 1, 0, 0);
		setCameraPosition(x, y, z);
	}
	
	public void setCameraPosition(float x, float y, float z) {
		position = new Vector3f(x, y, z);
	}
	
	public void setCameraRotation (float xAng, float yAng) {
//		yAng = Unit.correctAngle(yAng, false);

		if (yAng > 89.9f) {
			yAng = 89.9f;
		} else if (yAng < -89.9f) {
			yAng = -89.9f;
		}
		
		
		camXAngle = xAng;
		camYAngle = yAng;
		Game.print("CAm rotation x = " + camXAngle + " y = " + camYAngle);
		
	}
	
	public void resetCameraRotation() {
		camXAngle = CAM_INIT_X_ANG;
		camYAngle = CAM_INIT_Y_ANG;
	}

	public void moveCamera(int dirX, int dirZ, int dY, boolean isShift) {// Глобальное перемещение камеры, от пользователя

		// TODO строим скорость перемещения в зависимости от высоты и дальности до фокусной точки

		float h = position.y - collisionController.getMapHeight (position.x, position.z);
		
		float normalDistance = 50.0f;
		float distance = collisionController.getDistanceCursorToMap(this);
		
		Game.print(" height = " + h + " dist = " + distance);
		Game.print(" position = " + position.x + " " + position.z);
		
		float k = 1.0f;//distance / normalDistance;
		
		float xsSpeed = CAM_XZ_SPEED * k;
		float ySpeed = CAM_Y_SPEED * k;
		
//		if (xsSpeed > freeXSSpeed) 
//			xsSpeed = freeXSSpeed;
//		
//		if (ySpeed > freeYSpeed) 
//			ySpeed = freeYSpeed;
		
		float coef;

		if (isShift == true)
			coef = SHIFT_SPEED_COEFICIENT;
		else
			coef = 1.0f;

		if (dirX != 0) {
			position.x += dirX * Game.getDeltaInSeconds() * coef * xsSpeed * Math.cos(camXAngle * Math.PI / 180.0f);
			position.z += dirX * Game.getDeltaInSeconds() * coef * xsSpeed * Math.sin(camXAngle * Math.PI / 180.0f);
		}

		if (dirZ != 0) {
			position.x += dirZ * Game.getDeltaInSeconds() * coef * xsSpeed * -1.0f * Math.sin(camXAngle * Math.PI / 180.0f);
			position.z += dirZ * Game.getDeltaInSeconds() * coef * xsSpeed * Math.cos(camXAngle * Math.PI / 180.0f);
		}

		if (dY != 0) {
			position.y += dY * Game.getDeltaInSeconds() * coef * ySpeed * Math.sin(camYAngle * Math.PI / 180.0f);
			position.x += dY * Game.getDeltaInSeconds() * coef * ySpeed * Math.cos(camYAngle * Math.PI / 180.0f) * Math.cos(camXAngle * Math.PI / 180.0f);
			position.z += dY * Game.getDeltaInSeconds() * coef * ySpeed * Math.cos(camYAngle * Math.PI / 180.0f) * Math.sin(camXAngle * Math.PI / 180.0f);

		}
		
		if (h < CollisionsController.MIN_CAM_HEIGHT_ON_MAP) {
			position.y = collisionController.getMapHeight (position.x, position.z) + CollisionsController.MIN_CAM_HEIGHT_ON_MAP;
		}
		
		if (h > CollisionsController.MAX_CAM_HEIGHT_ON_MAP) {
			position.y = collisionController.getMapHeight (position.x, position.z) + CollisionsController.MAX_CAM_HEIGHT_ON_MAP;
		}
	}

	public void rotateCamera(float dAngX, float dAngY) { // поворот камеры от пользователя
		camXAngle += dAngX * CAM_XZ_ROTATION_SPEED;
		camYAngle += dAngY * CAM_Y_ROTATION_SPEED;

		setCameraRotation(camXAngle, camYAngle);
	}

	public float getFreeCamYAng() {
		return camYAngle;
	}

	public Vector3f getViewVector(Vector3f pos) { // Возвращает направление взгляда для вектора пос
		float xAngle = camXAngle;
		float yAngle = camYAngle;

		float length = 10.0f;

		Vector3f ret = new Vector3f();

		ret.x = (float) (length * Math.cos(xAngle / 180.0f * Math.PI));
		ret.z = (float) (length * Math.sin(xAngle / 180.0f * Math.PI));

		ret.y = (float) (length * Math.sin(yAngle * Math.PI / 180.0f));
		ret.z *= (float) (Math.cos(yAngle * Math.PI / 180.0f));
		ret.x *= (float) (Math.cos(yAngle * Math.PI / 180.0f));

		//Game.print("xAngle = " + xAngle + " yAngle = " + yAngle);

		ret.x += pos.x;
		ret.y += pos.y;
		ret.z += pos.z;
		return ret;
	}
	
	public Vector3f getViewVector() { // Возвращает направление взгляда для данного в UserView положения камеры
		return getViewVector(position);
	}

	public Vector3f getCursorDirection(Vector3f pos, Vector3f dir, float perspectiveAngle, Vector2f curPos) { // Возвращает вектор, являющийся направлением полета курсора (абсолютный)
		float dx = dir.x - pos.x;
		float dy = dir.x - pos.y;
		float dz = dir.x - pos.z;

		float len = (float) ((cursor.getMaxY() / 2.0f) / Math.tan(perspectiveAngle * Math.PI / 180.0f / 2.0f));

		float coefX = (curPos.x - this.cursor.getMaxX() / 2.0f);
		float coefY = (curPos.y - this.cursor.getMaxY() / 2.0f);
				
		Vector3f addX = new Vector3f((float) (1 * Math.cos((camXAngle + 90.0f) / 180.0f * Math.PI)), 0.0f, (float) (1 * Math.sin((camXAngle + 90.0f) / 180.0f * Math.PI)));
		Vector3f addY = new Vector3f((float) (1 * Math.cos((camXAngle) / 180.0f * Math.PI)) * (float) (1 * Math.cos((camYAngle + 90.0f) / 180.0f * Math.PI)), (float) (1 * Math.sin((camYAngle + 90.0f) / 180.0f * Math.PI)), (float) (1 * Math.sin((camXAngle) / 180.0f * Math.PI))* (float) (1 * Math.cos((camYAngle + 90.0f) / 180.0f * Math.PI))); //
		Normaliser.normalize(addY); //
		
		addX.scale(coefX);
		addY.scale(coefY); //
		
		float length = len;

		Vector3f ret = new Vector3f(dir);

		ret.x = (float) (length * Math.cos(camXAngle / 180.0f * Math.PI));
		ret.z = (float) (length * Math.sin(camXAngle / 180.0f * Math.PI));

		ret.y = (float) (length * Math.sin(camYAngle * Math.PI / 180.0f));
		ret.z *= (float) (Math.cos(camYAngle * Math.PI / 180.0f));
		ret.x *= (float) (Math.cos(camYAngle * Math.PI / 180.0f));

		Vector3f.add(ret, addX, ret);
		Vector3f.add(ret, addY, ret);

		ret.x += pos.x;
		ret.y += pos.y;
		ret.z += pos.z;
		return ret;

	}
	
	public Vector3f getCursorDirection() { // Возвращает вектор, являющийся направлением полета курсора (абсолютный), для данного объекта userView
		return getCursorDirection(position, getViewVector(position), Render.perspectiveAngle, new Vector2f(this.cursor.getX(), this.cursor.getY()));
		
	}
	
	public Vector3f getCursorDirection(int x, int y) { // Возвращает вектор, являющийся направлением полета курсора (абсолютный), для данного объекта userView, при конкретных значениях положения курсора на экране
		return getCursorDirection(position, getViewVector(position), Render.perspectiveAngle, new Vector2f(x, y));		
	}
	
	public Vector3f getCamAbsoluteDirection () {
		Vector3f direction = new Vector3f();
		direction.x = (float) (Math.cos(-camXAngle * Math.PI / 180.0f));
		direction.z = (float) (Math.sin(-camXAngle * Math.PI / 180.0f));

		direction.y = (float) (Math.sin(camYAngle * Math.PI / 180.0f));
		direction.z *= (float) (-1.0f * Math.cos(camYAngle * Math.PI / 180.0f));
		direction.x *= (float) (Math.cos(camYAngle * Math.PI / 180.0f));

		Vector3f.add(direction, position, direction);
		return direction;

	}
	
	public Vector3f getCamAbsoluteYVector() {
		return new Vector3f(0.0f, 1.0f, 0.0f);
	}
		
	public Cursor getCursor() {
		return cursor;
	}
	
	public Vector3f getPosition () {
		return position;
	}
	
	public Vector2f getRotation () {
		return new Vector2f(camXAngle, camYAngle);
	}

}

