package WSA.ai;

public abstract class AI {

	public abstract void refresh();
	
	public String getAIName() {
		return this.getClass().getName();
	}
}
