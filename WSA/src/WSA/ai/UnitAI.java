package WSA.ai;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import WSA.CollisionsController;
import WSA.Game;
import WSA.map.MapController;
import WSA.unit.ObjectController;
import WSA.unit.action.ActionMove;
import WSA.unit.action.ActionRotate;
import WSA.unit.objects.Unit;

public class UnitAI extends AI {

	Unit unit;
	
	MapController map;
	ObjectController objectController;
	CollisionsController collisionsController;
	
	float lastBusyTime = Game.getGameTime();
	
	
	public static final float MAX_BORING_TIME = 7.0f;
	public float BORING_TIME = (float) (Math.random() * MAX_BORING_TIME);
	
	public static final float MAX_WALK_DISTANCE = 5.0f;
	public static final float MAX_ROTATE_ANG = 15.0f;
	
	int tacktic;
	
	public UnitAI(Unit unit, MapController map, ObjectController objectController, CollisionsController collisionsController) {
		this.unit = unit;
		tacktic = BehaviorTactics.BEHAVIOR_FREE;
		this.map = map;
		this.objectController = objectController;
		this.collisionsController = collisionsController;
	}
	
	@Override
	public void refresh() {
		if (!unit.isBusy()) {
			if (Game.getGameTime() - lastBusyTime > BORING_TIME) {
				objectDoSomething();
			}
		} else {
			lastBusyTime = Game.getGameTime();
		}
	}

	public void objectDoSomething () {
		if (tacktic == BehaviorTactics.BEHAVIOR_FREE) {
			if (Math.random() < 0.1f) {
				Vector3f newPos = new Vector3f(unit.getPosition());
				newPos.x += -MAX_WALK_DISTANCE + 2 * Math.random() * MAX_WALK_DISTANCE;
				newPos.z += -MAX_WALK_DISTANCE + 2 * Math.random() * MAX_WALK_DISTANCE;
				newPos.y = collisionsController.getMapHeight(newPos.x, newPos.z);
				
				unit.addAction(new ActionMove(newPos, collisionsController));
			} else {
				float dAng = (float) (-MAX_ROTATE_ANG + 2 * Math.random() * MAX_ROTATE_ANG);
				unit.addAction(new ActionRotate(new Vector2f(unit.getRotation().x + dAng, unit.getRotation().y)));
			}
			BORING_TIME = (float) (Math.random() * MAX_BORING_TIME);
		} else if (tacktic == BehaviorTactics.BEHAVIOR_HOLD_POSITION) {
			
		}
	}
	
	
}
