package WSA.enviroment;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Vector3f;

import WSA.Game;


public class Enviroment {
	private Vector3f sunPosition;
	private Vector3f sunColor;

	public static final float SUN_MAX_HEIGHT = 1.0f;

	public static final Vector3f colorZenith = new Vector3f(1.0f, 1.0f, 212.0f / 255.0f);
	public static final Vector3f colorMiddle = new Vector3f(1.0f, 1.0f, 147.0f / 255.0f);
	public static final Vector3f colorBottom = new Vector3f(1.0f, 85.0f / 255.0f, 28.0f / 255.0f);

	public static final Vector3f FOG_NIGHT_COLOR = new Vector3f(0.0f, 0.0f, 0.0f);
	public static final Vector3f FOG_DAY_COLOR = new Vector3f(0.8f, 0.8f, 0.8f);

	public static final Vector3f SKY_DAY_COLOR = new Vector3f(0.51f, 0.66f, 0.99f);
	public static final Vector3f SKY_NIGHT_COLOR = new Vector3f(0.0078f, 0.1406f, 0.3007f);

	public static float ZENITH_LEVEL = 0.6f;
	public static float MIDDLE_LEVEL = 0.3f;
	public static float BOTTOM_LEVEL = 0.1f;

	// Север в направлении X+


	private Game game;

	public Enviroment(Game game) {
		this.game = game;
		sunPosition = new Vector3f();
		sunPosition.set(1.0f, 1.0f, 1.0f);
	}

	public void refresh(float time) {
		float ang = 360.0f * ((time % Game.DAY_DURATION) / Game.DAY_DURATION);

		float distanceToSun = 1.0f;

		sunPosition.x = distanceToSun * (float) Math.sin(ang * Math.PI / 180.0f);
		sunPosition.y = distanceToSun * (float) -Math.cos(ang * Math.PI / 180.0f);
		sunPosition.z = distanceToSun * (float) Math.sin(ang * Math.PI / 180.0f);
	}

	public Vector3f getSunPositionV3f() {
		return sunPosition;
	}

	public FloatBuffer getSunPositionFB() {

		FloatBuffer ret = ByteBuffer.allocateDirect(4 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();//)BufferUtils.createFloatBuffer(4);
		ret.put(sunPosition.x);
		ret.put(sunPosition.y);
		ret.put(sunPosition.z);
		ret.put(0.0f);
		ret.flip();
		return ret;
	}

	public FloatBuffer getLightDiffuse() {

		float colorIntencityCoefficient = 1.0f;

		if (sunPosition.y < 0.0f) {
			colorIntencityCoefficient = 1.0f - 10.0f * Math.abs(sunPosition.y) / SUN_MAX_HEIGHT;
			if (colorIntencityCoefficient < 0.0f) {
				colorIntencityCoefficient = 0.0f;
			}
		}

		Vector3f color = getSunLightColor();

		FloatBuffer ret = BufferUtils.createFloatBuffer(4);
		ret.put(color.x * colorIntencityCoefficient);
		ret.put(color.y * colorIntencityCoefficient);
		ret.put(color.z * colorIntencityCoefficient);
		ret.put(1.0f);
		ret.flip();
		return ret;
	}

	public FloatBuffer getLightAmbient() {
		FloatBuffer ret = BufferUtils.createFloatBuffer(4);

		ret.put(0.0f);
		ret.put(0.0f);
		ret.put(0.0f);
		ret.put(1.0f);
		ret.flip();
		return ret;
	}

	public Vector3f getSunCenterColor() {
		return new Vector3f(1.0f, 1.0f, 1.0f);
	}
	public Vector3f getSunLightColor() {
		Vector3f color = colorZenith;

		if (sunPosition.y / SUN_MAX_HEIGHT < BOTTOM_LEVEL) {
			color = colorBottom;
		} else if (sunPosition.y / SUN_MAX_HEIGHT > ZENITH_LEVEL) {
			color = colorZenith;
		} else if (sunPosition.y / SUN_MAX_HEIGHT > MIDDLE_LEVEL){		
			Vector3f.add(colorMiddle, (Vector3f) Vector3f.sub(colorZenith, colorMiddle, null).scale((sunPosition.y / SUN_MAX_HEIGHT - MIDDLE_LEVEL) / (ZENITH_LEVEL - MIDDLE_LEVEL)), color);
		} else {
			Vector3f.add(colorBottom, (Vector3f) Vector3f.sub(colorMiddle, colorBottom, null).scale((sunPosition.y / SUN_MAX_HEIGHT - BOTTOM_LEVEL) / (MIDDLE_LEVEL - BOTTOM_LEVEL)), color);
		}

		return color;
	}
	
	public Vector3f getFogColor(float time) {
		Vector3f ret = new Vector3f();

		float period = Game.DAY_DURATION * 2.0f;

		float ang = ((360.0f * ((time % period) / period))/* + */) % 360.0f;

		float nightCoef = 0.5f;

		float ratioSin = (1.0f + nightCoef) * (float) Math.abs(Math.sin(ang * Math.PI / 180.0f)) - (nightCoef); // длительность полной ночи 

		//Game.print(" time = " + Date.getTimeString(Game.getDate()) + " ang = " + ang);

		if (ratioSin < 0) {
			ratioSin = 0;
		}
		
		

		ret.x = FOG_NIGHT_COLOR.x + (FOG_DAY_COLOR.x - FOG_NIGHT_COLOR.x) * ratioSin;
		ret.y = FOG_NIGHT_COLOR.y + (FOG_DAY_COLOR.y - FOG_NIGHT_COLOR.y) * ratioSin;
		ret.z = FOG_NIGHT_COLOR.z + (FOG_DAY_COLOR.z - FOG_NIGHT_COLOR.z) * ratioSin;

		//ret = color4Day;
		return ret;		
	}

	public Vector3f getSkyColor(float time) {
		Vector3f ret = new Vector3f();

		float period = Game.DAY_DURATION * 2.0f;

		float ang = ((360.0f * ((time % period) / period))/* + */) % 360.0f;

		float nightCoef = 0.5f;

		float ratioSin = (1.0f + nightCoef) * (float) Math.abs(Math.sin(ang * Math.PI / 180.0f)) - (nightCoef); // длительность полной ночи 

		//Game.print(" time = " + Date.getTimeString(Game.getDate()) + " ang = " + ang);

		if (ratioSin < 0) {
			ratioSin = 0;
		}

		ret.x = SKY_NIGHT_COLOR.x + (SKY_DAY_COLOR.x - SKY_NIGHT_COLOR.x) * ratioSin;
		ret.y = SKY_NIGHT_COLOR.y + (SKY_DAY_COLOR.y - SKY_NIGHT_COLOR.y) * ratioSin;
		ret.z = SKY_NIGHT_COLOR.z + (SKY_DAY_COLOR.z - SKY_NIGHT_COLOR.z) * ratioSin;

		//ret = color4Day;
		return ret;
	}

}
