package WSA.enviroment;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import org.lwjgl.util.vector.Vector3f;

import WSA.Game;


public class Orb {

	private Vector3f position;
	
	
	public void refresh(float time) {
		float ang = 360.0f * ((time % Game.DAY_DURATION) / Game.DAY_DURATION);

		float distanceToSun = 1.0f;

		position.x = distanceToSun * (float) Math.sin(ang * Math.PI / 180.0f);
		position.y = distanceToSun * (float) -Math.cos(ang * Math.PI / 180.0f);
		position.z = distanceToSun * (float) Math.sin(ang * Math.PI / 180.0f);
	}
	
	public Vector3f getSunPositionV3f() {
		return position;
	}

	public FloatBuffer getSunPositionFB() {
		FloatBuffer ret = ByteBuffer.allocateDirect(4 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();//)BufferUtils.createFloatBuffer(4);
		ret.put(position.x);
		ret.put(position.y);
		ret.put(position.z);
		ret.put(0.0f);
		ret.flip();
		return ret;
	}
	
}
